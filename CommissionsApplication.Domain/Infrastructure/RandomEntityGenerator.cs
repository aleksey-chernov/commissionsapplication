﻿using System;
using System.Linq;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.Domain.Infrastructure
{
    public class RandomEntityGenerator
    {
        private const string LatinChars = "abcdefghijklmnopqrstuvwxyz";
        private const string CyrillicChars = "абвгдеёжзийклмнопстуфхцчшщъыьэюя";

        private readonly Random _random;

        public RandomEntityGenerator(Random random)
        {
            _random = random;
        }

        public DateTime RandomDate(DateTime from, DateTime to)
        {
            var range = to - from;
            var randomTimeSpan =
                TimeSpan.FromSeconds((long) (range.TotalSeconds - _random.Next(0, (int) range.TotalSeconds)));
            return from + randomTimeSpan;
        }

        public string RandomString(int size, string chars, bool title = false)
        {
            var buffer = new char[size];

            for (var i = 0; i < size; i++)
            {
                var nextChar = chars[_random.Next(chars.Length)];
                buffer[i] = title && i == 0 ? Char.ToUpper(nextChar) : nextChar;
            }

            return new string(buffer);
        }

        public string RandomLatinString(int size, bool title = false)
        {
            return RandomString(size, LatinChars, title);
        }

        public string RandomCyrillicString(int size, bool title = false)
        {
            return RandomString(size, CyrillicChars, title);
        }

        public Commission RandomCommission()
        {
            return new Commission
            {
                CommissionStatus = (Commission.Status) (_random.Next()%7),
                OutcomeDocument =
                    string.Format("{0} от {1}", _random.Next(), RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now)),
                IncomeDocument =
                    string.Format("{0} от {1}", _random.Next(), RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now)),
                IncomeDate = RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now),
                Summary =
                    string.Join(" ", Enumerable.Range(1, 5).Select(x => RandomCyrillicString(_random.Next(5, 10), x == 1))),
                Organization = RandomCyrillicString(_random.Next(5, 10), true),
                Signer =
                    string.Format("{0} {1}.{2}.", RandomCyrillicString(_random.Next(7, 13), true), RandomCyrillicString(1, true),
                        RandomCyrillicString(1, true)),
                Resolution =
                    string.Join(" ",
                        Enumerable.Range(1, _random.Next(3, 12)).Select(x => RandomCyrillicString(_random.Next(5, 10), x == 1))),
                Commentary =
                    string.Join(" ",
                        Enumerable.Range(1, _random.Next(3, 12)).Select(x => RandomCyrillicString(_random.Next(5, 10), x == 1))),
                ExecutionMark =
                    string.Format("{0} от {1}", _random.Next(), RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now)),
                ExecutionDate =  _random.Next()%2 == 1 ? (DateTime?) null : RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now),
                RegNumber = _random.Next().ToString(),
                CommissionNumber = _random.Next().ToString(),
                ImportanceFactor = _random.Next(1, 4),
                DelayFactor =
                    _random.Next()%2 == 1 ? 0.02M : _random.Next()%2 == 1 ? 0.04M : (decimal) _random.NextDouble(),
                DelayInfo = _random.Next(1, 3) + "-" + _random.Next(4, 7),
                Reason =
                    string.Join(" ",
                        Enumerable.Range(1, _random.Next(3, 12)).Select(x => RandomCyrillicString(_random.Next(5, 10), x == 1)))
            };
        }

        public Responsible RandomResponsible()
        {
            return new Responsible
            {
                Name = string.Format("{0} {1}.{2}.", RandomCyrillicString(_random.Next(7, 13), true), RandomCyrillicString(1, true),
                    RandomCyrillicString(1, true)),
                Email = string.Format("{0}@givnipiet.ru", RandomLatinString(_random.Next(7, 13)))
            };
        }

        public User RandomUser()
        {
            return new User
            {
                Name = RandomCyrillicString(_random.Next(7, 13), true),
                Email = string.Format("{0}@givnipiet.ru", RandomLatinString(_random.Next(7, 13))),
                UserRole = (User.Role) (_random.Next()%2)
            };
        }

        public Attachment RandomAttachment()
        {
            return new Attachment
            {
                Name = string.Format("{0}.{1}", RandomCyrillicString(_random.Next(5, 15), true), RandomCyrillicString(3)),
                AttachmentType = (Attachment.Type) (_random.Next()%4)
            };
        }
    }
}