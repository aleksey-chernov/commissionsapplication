﻿using System;

namespace CommissionsApplication.Domain.Entities
{
    public class Charge
    {
        public int ChargeId { get; set; }

        public int ResponsibleId { get; set; }

        public int CommissionId { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public Responsible Responsible { get; set; }

        public Commission Commission { get; set; }
    }
}