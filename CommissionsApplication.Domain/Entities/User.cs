﻿using System;
using System.Collections.Generic;

namespace CommissionsApplication.Domain.Entities
{
    public class User
    {
        public enum Role { Administrator, Assistant }

        public User()
        {
            Responsibles = new List<Responsible>();
        }

        public int UserId { get; set; }

        public byte[] RowVersion { get; set; }

        public DateTime? LastModified { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public Role UserRole { get; set; }

        public ICollection<Responsible> Responsibles { get; private set; }
    }
}