﻿using System;
using System.Collections.Generic;

namespace CommissionsApplication.Domain.Entities
{
    public class Commission
    {
        public enum Status { None, NotExecuted, Executed, ThisWeek, NextWeek, Question, Transfer }

        public static readonly List<Status> DelayedStatusList = new List<Status>
        {
            Status.NotExecuted,
            Status.NextWeek,
            Status.Question,
            Status.ThisWeek
        };

        public Commission()
        {
            IncomeDate = DateTime.Now;
            Charges = new List<Charge>();
            ControlDates = new List<ControlDate>();
            Attachments = new List<Attachment>();
            ImportanceFactor = 1;
        }

        public byte[] RowVersion { get; set; }

        public DateTime? LastModified { get; set; }

        public int CommissionId { get; set; }

        public int CommissionRowNumber { get; set; }

        public Status CommissionStatus { get; set; }

        public string OutcomeDocument { get; set; }

        public string IncomeDocument { get; set; }

        public DateTime IncomeDate { get; set; }

        public string Summary { get; set; }

        public string Organization { get; set; }

        public string Signer { get; set; }

        public string Resolution { get; set; }

        public string Commentary { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string ExecutionMark { get; set; }

        public string RegNumber { get; set; }

        public string CommissionNumber { get; set; }

        public int ImportanceFactor { get; set; }

        public decimal? DelayFactor { get; set; }

        public string DelayInfo { get; set; }

        public string Reason { get; set; }

        public ICollection<Charge> Charges { get; private set; }

        public ICollection<ControlDate> ControlDates { get; private set; }

        public ICollection<Attachment> Attachments { get; private set; }

        public bool IsDelayed => DelayedStatusList.Contains(CommissionStatus);
    }
}