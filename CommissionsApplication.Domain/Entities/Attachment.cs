﻿using System.IO;

namespace CommissionsApplication.Domain.Entities
{
    public class Attachment
    {
        public enum Type { Income, Outcome }

        public int AttachmentId { get; set; }

        public int CommissionId { get; set; }

        public Type AttachmentType { get; set; }

        public string Name { get; set; }

        public Stream Data { get; set; }
        
        public string ContentType { get; set; }
    }
}