﻿using System;

namespace CommissionsApplication.Domain.Entities
{
    public class ControlDate
    {
        public int ControlDateId { get; set; }

        public int CommissionId { get; set; }

        public DateTime Date { get; set; }
    }
}