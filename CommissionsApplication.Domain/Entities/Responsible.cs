﻿using System;
using System.Collections.Generic;

namespace CommissionsApplication.Domain.Entities
{
    public class Responsible
    {
        public Responsible()
        {
            Charges = new List<Charge>();
            Users = new List<User>();
        }

        public int ResponsibleId { get; set; }

        public byte[] RowVersion { get; set; }

        public DateTime? LastModified { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public ICollection<Charge> Charges { get; private set; }

        public ICollection<User> Users { get; private set; }
    }
}