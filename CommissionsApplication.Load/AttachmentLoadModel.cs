﻿using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.Load
{
    /// <summary>
    /// Вспомогательный класс для загрузки приложений
    /// </summary>
    internal class AttachmentLoadModel
    {
        public int CommissionId { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public Attachment.Type AttachmentType { get; set; }
    }
}