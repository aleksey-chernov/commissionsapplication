﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.Domain.Entities;
using Excel;
using log4net;

namespace CommissionsApplication.Load
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger("CommissionsLoaderLog");
        private static readonly string CommissionsPath = ConfigurationManager.AppSettings["CommissionsXlsPath"];
        private static readonly string ResponsiblesPath = ConfigurationManager.AppSettings["ResponsiblesXlsPath"];
        private static readonly string AttachmentsPath = ConfigurationManager.AppSettings["AttachmentsXlsPath"];
        private static readonly string AttachmentsFilePrefix = ConfigurationManager.AppSettings["AttachmentsPrefix"];
        private static readonly string AttachmentsFilePath = ConfigurationManager.AppSettings["AttachmentsPath"];

        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Console.WriteLine("Начало импорта...");
            Logger.Info("Начало импорта");

            using (var context = new CommissionsContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    //Очищаем таблицы, сбрасываем identity
                    Console.WriteLine("Очистка таблиц...");
                    context.Database.ExecuteSqlCommand("DELETE FROM dbo.Users");
                    context.Database.ExecuteSqlCommand("DELETE FROM dbo.Responsibles");
                    context.Database.ExecuteSqlCommand("DELETE FROM dbo.Commissions");
                    context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('dbo.Users', RESEED, 0)");
                    context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('dbo.Responsibles', RESEED, 0)");
                    context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('dbo.Commissions', RESEED, 0)");

                    //Добавляем пользователей по умолчанию
                    Console.WriteLine("Добавляются пользователи...");
                    context.Users.AddRange(new List<User>
                    {
                        new User
                        {
                            Name = "Администратор",
                            Login = "admin",
                            Password = PasswordHash.GetMD5Hash("123456"),
                            Email = "admin@spbaep.ru",
                            UserRole = User.Role.Administrator
                        },
                        new User
                        {
                            Name = "Помощник",
                            Login = "assist",
                            Password = PasswordHash.GetMD5Hash("123456"),
                            Email = "assist@spbaep.ru",
                            UserRole = User.Role.Assistant
                        }
                    });
                    context.SaveChanges();

                    //Заполняем таблицу подчиненных, считанных из соответствующего xlsx файла
                    Console.WriteLine("Добавляются ответственные...");
                    var responsibles = Readers.ResponsiblesReader().ToList();
                    context.Responsibles.AddRange(responsibles);
                    context.SaveChanges();

                    //Заполняем таблицу поручений, считанных из соответствующего xlsx файла
                    Console.WriteLine("Добавляются поручения с входящими приложениями...");
                    foreach (var model in Readers.CommissionsReader())
                    {
                        var commission = new Commission
                        {
                            CommissionStatus = model.CommissionStatus,
                            OutcomeDocument = model.OutcomeDocument,
                            IncomeDocument = model.IncomeDocument,
                            IncomeDate = model.IncomeDate,
                            Summary = model.Summary,
                            Organization = model.Organization,
                            Signer = model.Signer,
                            Resolution = model.Resolution,
                            Commentary = model.Commentary,
                            ExecutionDate = model.ExecutionDate,
                            ExecutionMark = model.ExecutionMark,
                            RegNumber = model.RegNumber,
                            CommissionNumber = model.CommissionNumber,
                            ImportanceFactor = model.ImportanceFactor,
                            DelayFactor = model.DelayFactor,
                            DelayInfo = model.DelayInfo,
                            Reason = model.Reason
                        };
                        foreach (var controlDate in model.ControlDates)
                        {
                            commission.ControlDates.Add(controlDate);
                        }

                        //Добавляем ответственных по поручению, ищем по совпадению фамилии в ранее созданной коллекции ответственных
                        foreach (
                            var responsbile in
                                responsibles.Where(r => model.Responsibles.Any(name => name.StartsWith(r.Name))))
                        {
                            commission.Charges.Add(new Charge
                            {
                                Responsible = responsbile,
                                Commission = commission,
                                ExecutionDate = null
                            });
                        }
                        context.Commissions.Add(commission);
                        context.SaveChanges();

                        //Для каждого поручения добавляем в таблицу приложений входящий документ
                        AddAttachment(context, new AttachmentLoadModel
                        {
                            CommissionId = commission.CommissionId,
                            Name = commission.IncomeDocument,
                            Path = model.IncomeDocumentPath
                        });

                        Console.WriteLine("Добавлено поручение {0}...", commission.CommissionId);
                    }

                    //Заполняем таблицу приложений, считанных из соответствующего xlsx файла
                    Console.WriteLine("Добавляются исходящие приложения...");
                    var attachments = Readers.AttachmentsReader().ToList();
                    foreach (var model in attachments)
                    {
                        AddAttachment(context, model);
                        Console.WriteLine("Добавлено приложение {0} к поручению {1}...", model.Name, model.CommissionId);
                    }

                    //Заполняем пустые поля Отметка о выполнении для поручений имеющих исходящие приложения
                    Console.WriteLine("Заполняем пустые поля 'Отметка о выполнении'...");
                    var commissionsWithAttachmentsIds = attachments.Select(a => a.CommissionId).ToList();
                    foreach (
                        var commission in
                            context.Commissions.Where(
                                c => commissionsWithAttachmentsIds.Contains(c.CommissionId) && c.ExecutionMark == null))
                    {
                        commission.ExecutionMark = "Приложения";
                    }
                    context.SaveChanges();

                    transaction.Commit();
                }
            }

            Console.WriteLine("Импорт завершен. Ошибки импорта см. в файле log.txt");
            Logger.Info("Окончание импорта");
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("Критическая ошибка импорта! см. файл log.txt");
            Logger.Error($"Критическая ошибка выполнения! {e.ExceptionObject}");
            Environment.Exit(1);
        }

        /// <summary>
        ///     Вспомогательный метод для загрузки приложений
        /// </summary>
        /// <param name="context"></param>
        /// <param name="attachmentModel"></param>
        private static void AddAttachment(CommissionsContext context, AttachmentLoadModel attachmentModel)
        {
            if (!attachmentModel.Path.StartsWith(AttachmentsFilePrefix))
            {
                Logger.Error($"Неправильный префикс адреса файла {attachmentModel.Path}");
                return;
            }

            var attachmentFilePath = Path.Combine(AttachmentsFilePath,
                attachmentModel.Path.Substring(AttachmentsFilePrefix.Length + 1));
            var attachmentFile = new FileInfo(attachmentFilePath);
            var attachment = new Attachment
            {
                AttachmentType = attachmentModel.AttachmentType,
                CommissionId = attachmentModel.CommissionId,
                Name = attachmentFile.Name,
                ContentType = MIMEAssistant.GetMIMEType(attachmentFile.Extension)
            };
            context.Attachments.Add(attachment);
            context.SaveChanges();

            //Открываем файл на чтение и загружаем его поток, используя Filestream
            //Для корректной работы приложение должно быть запущено под учетной записью авторизованной на SQL сервере
            try
            {
                using (var dataStream = attachmentFile.OpenRead())
                {
                    var rowData = context.Database.SqlQuery<FileStreamRowData>(
                        "SELECT Data.PathName() AS 'Path', GET_FILESTREAM_TRANSACTION_CONTEXT() AS 'Transaction' FROM dbo.Attachments WHERE AttachmentId = @id",
                        new SqlParameter("id", attachment.AttachmentId)).SingleOrDefault();

                    if (rowData == null)
                        return;

                    using (var stream = new SqlFileStream(rowData.Path, rowData.Transaction, FileAccess.Write))
                    {
                        dataStream.CopyTo(stream);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Logger.Error($"Файл с таким именем не найден {attachment.Name}");
                return;
            }
            context.SaveChanges();
        }

        /// <summary>
        ///     Вспомогательные методы для выгрузки данных из xlsx документов
        /// </summary>
        private static class Readers
        {
            public static IEnumerable<Responsible> ResponsiblesReader()
            {
                using (var stream = File.Open(ResponsiblesPath, FileMode.Open, FileAccess.Read))
                using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    reader.Read();

                    Responsible responsible = null;
                    while (reader.Read())
                    {
                        try
                        {
                            responsible = new Responsible
                            {
                                Name = reader.GetString(0),
                                Email = reader.GetString(1)
                            };
                        }
                        catch
                        {
                            Logger.Error(
                                $"Ошибка при чтении файла {ResponsiblesPath}. Последняя прочитанная запись: {responsible?.Name ?? "-"}.");

                            throw;
                        }

                        yield return responsible;
                    }
                }
            }

            public static IEnumerable<CommissionLoadModel> CommissionsReader()
            {
                using (var stream = File.Open(CommissionsPath, FileMode.Open, FileAccess.Read))
                using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    reader.Read();

                    CommissionLoadModel commission = null;
                    while (reader.Read())
                    {
                        try
                        {
                            commission = new CommissionLoadModel
                            {
                                CommissionRowNumber = reader.GetInt32(0),
                                CommissionStatus = (Commission.Status)reader.GetInt32(2),
                                OutcomeDocument = reader.GetString(3),
                                IncomeDocument = reader.GetString(5),
                                IncomeDocumentPath = reader.GetString(6),
                                IncomeDate = DateTime.ParseExact(reader.GetString(4), @"dd.MM.yyyy", null),
                                Summary = reader.GetString(7),
                                Organization = reader.GetString(8),
                                Signer = reader.GetString(9),
                                Resolution = reader.GetString(10),
                                Commentary = reader.GetString(12),
                                ExecutionDate = string.IsNullOrEmpty(reader.GetString(14))
                                                    ? (DateTime?)null
                                                    : DateTime.ParseExact(reader.GetString(14), @"dd.MM.yyyy", null),
                                ExecutionMark = reader.GetString(13),
                                RegNumber = reader.GetString(15),
                                CommissionNumber = reader.GetString(16),
                                ImportanceFactor = reader.GetInt32(17),
                                DelayFactor = string.IsNullOrEmpty(reader.GetString(18))
                                                    ? (decimal?)null
                                                    : reader.GetDecimal(18),
                                DelayInfo = reader.GetString(19),
                                Reason = reader.GetString(20)
                            };

                            var controlDatesString = reader.GetString(1);
                            if (!string.IsNullOrEmpty(controlDatesString))
                            {
                                foreach (var controlDate in controlDatesString
                                    .Split(';')
                                    .Select(d => new ControlDate { Date = DateTime.ParseExact(d, @"dd.MM.yyyy", null) })
                                    .OrderBy(d => d.Date))
                                {
                                    commission.ControlDates.Add(controlDate);
                                }
                            }

                            var responsiblesString = reader.GetString(11);
                            if (!string.IsNullOrEmpty(responsiblesString))
                            {
                                commission.Responsibles.AddRange(responsiblesString.Split(';'));
                            }
                        }
                        catch
                        {
                            Logger.Error(
                                $"Ошибка при чтении файла {CommissionsPath}. Последняя прочитанная запись: {commission?.CommissionRowNumber.ToString() ?? "-"}.");

                            throw;
                        }

                        yield return commission;
                    }
                }
            }

            public static IEnumerable<AttachmentLoadModel> AttachmentsReader()
            {
                using (var stream = File.Open(AttachmentsPath, FileMode.Open, FileAccess.Read))
                using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    reader.Read();

                    AttachmentLoadModel attachment = null;
                    while (reader.Read())
                    {
                        try
                        {
                            attachment = new AttachmentLoadModel
                            {
                                CommissionId = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Path = reader.GetString(2),
                                AttachmentType = Attachment.Type.Outcome
                            };
                        }
                        catch
                        {
                            Logger.Error(
                                $"Ошибка при чтении файла {AttachmentsPath}. Последняя прочитанная запись: {attachment?.Name ?? "-"}.");

                            throw;
                        }

                        yield return attachment;
                    }
                }
            }
        }
    }
}