﻿using System;
using System.Collections.Generic;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.Load
{
    /// <summary>
    /// Вспомогательный класс для загрузки поручений
    /// </summary>
    internal class CommissionLoadModel
    {
        public CommissionLoadModel()
        {
            IncomeDate = DateTime.Now;
            Responsibles = new List<string>();
            ControlDates = new List<ControlDate>();
            Attachments = new List<Attachment>();
            ImportanceFactor = 1;
        }

        public int CommissionRowNumber { get; set; }

        public Commission.Status CommissionStatus { get; set; }

        public string OutcomeDocument { get; set; }

        public string IncomeDocument { get; set; }

        public string IncomeDocumentPath { get; set; }

        public DateTime IncomeDate { get; set; }

        public string Summary { get; set; }

        public string Organization { get; set; }

        public string Signer { get; set; }

        public string Resolution { get; set; }

        public string Commentary { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string ExecutionMark { get; set; }

        public string RegNumber { get; set; }

        public string CommissionNumber { get; set; }

        public int ImportanceFactor { get; set; }

        public decimal? DelayFactor { get; set; }

        public string DelayInfo { get; set; }

        public string Reason { get; set; }

        public List<string> Responsibles { get; set; }

        public ICollection<ControlDate> ControlDates { get; private set; }

        public ICollection<Attachment> Attachments { get; private set; }
    }
}