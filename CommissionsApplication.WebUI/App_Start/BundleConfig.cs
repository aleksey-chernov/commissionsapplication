﻿using System;
using System.Web.Optimization;

namespace CommissionsApplication.WebUI
{
    public class BundleConfig
    {
        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
                throw new ArgumentNullException("ignoreList");
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            ignoreList.Ignore("*.min.js", OptimizationMode.WhenEnabled);
            ignoreList.Ignore("*.min.css", OptimizationMode.WhenEnabled);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);
            BundleTable.EnableOptimizations = false;

#if DEBUG
            bundles.Add(new ScriptBundle("~/bundles/libs")
                .Include("~/Scripts/libs/libs.js"));

            bundles.Add(new StyleBundle("~/bundles/style")
                .Include("~/Content/style.css")
                .Include("~/Content/site.css"));
#else
            bundles.Add(new ScriptBundle("~/bundles/libs")
                .Include("~/Scripts/libs/libs.min.js"));

            bundles.Add(new StyleBundle("~/bundles/style")
                .Include("~/Content/style.min.css")
                .Include("~/Content/site.css"));
#endif

            bundles.Add(new ScriptBundle("~/bundles/app")
                .Include("~/Scripts/app/application.js"));
        }
    }
}