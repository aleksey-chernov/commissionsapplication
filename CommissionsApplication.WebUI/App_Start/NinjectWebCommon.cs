using System;
using System.Web;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.DAL.Repositories.Concrete;
using CommissionsApplication.WebUI;
using CommissionsApplication.WebUI.Infrastructure.Auth.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Auth.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Export.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Export.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Mail.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Mail.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete;
using CommissionsApplication.WebUI.Models.Concrete;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace CommissionsApplication.WebUI
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<EFUnitOfWork>().InRequestScope()
                .WithConstructorArgument("userName", ctx => HttpContext.Current.User.Identity.Name);

            kernel.Bind<IMembershipUserConverter>().To<CustomMembershipUserConverter>().InRequestScope();
            kernel.Bind<IMapper>().To<CustomMapper>().InRequestScope();
            kernel.Bind<IExportService<CommissionViewModel>>().To<CommissionsExportService>().InSingletonScope();
            kernel.Bind<IMailManager>().To<DefaultMailManager>();
        }
    }
}