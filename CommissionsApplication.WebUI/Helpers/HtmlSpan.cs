﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace CommissionsApplication.WebUI.Helpers
{
    public static class HtmlSpan
    {
        public static MvcHtmlString SpanFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            return SpanFor(html, expression, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString SpanFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder span = new TagBuilder("span");
            span.SetInnerText(labelText);

            return MvcHtmlString.Create(span.ToString(TagRenderMode.Normal));
        }
    }
}