﻿using System.Linq;
using System.Web.Mvc;

namespace CommissionsApplication.WebUI.Helpers
{
    public static class HtmlRouteData
    {
        public static string ValueForRouteData(this HtmlHelper html, string value, string actions, string controllers)
        {
            var viewContext = html.ViewContext.Controller.ControllerContext.IsChildAction
                ? html.ViewContext.ParentActionViewContext
                : html.ViewContext;
            
            var routeValues = viewContext.RouteData.Values;
            var currentAction = routeValues["action"].ToString();
            var currentController = routeValues["controller"].ToString();

            var acceptedActions = actions.Trim().Split(',').Distinct();
            var acceptedControllers = controllers.Trim().Split(',').Distinct();

            return acceptedActions.Contains(currentAction) && acceptedControllers.Contains(currentController) 
                ? value 
                : string.Empty;
        }
    }
}