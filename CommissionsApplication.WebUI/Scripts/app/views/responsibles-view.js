﻿define("ResponsiblesView", function(require) {
  var AjaxView = require("AjaxView"),
    objectFix = require("objectFix");

  objectFix.enable();

  function ResponsiblesView(locales) {
    AjaxView.call(this);

    this.locales = locales;
  };

  ResponsiblesView.prototype = Object.create(AjaxView.prototype);

  ResponsiblesView.prototype.render = function() {
    var datagrid = require("datagrid"),
      multiselect = require("multiselect");

    var urls = {
          responsibles: $("#responsibles-url").val(),
          responsibleAdd: $("#responsible-add-url").val(),
          responsibleEdit: $("#responsible-edit-url").val(),
          responsibleDelete: $("#responsible-delete-url").val()
        },
      heightFunc = function() {
        return $(window).height() - 160;
      };

    datagrid.init("#responsibles-grid", {
      url: urls.responsibles,
      columns: [
        { field: "ResponsibleId", visible: false },
        { field: "Name", title: this.locales["Responsible_Name"], width: 100 },
        { field: "Email", title: this.locales["Responsible_Email"], width: 100 }
      ],
      filterable: true,
      pageable: false,
      filters: [
        { field: "Name", title: this.locales["Responsible_Name"], visible: true },
        { field: "Email", title: this.locales["Responsible_Email"] }
      ],
      height: heightFunc(),
      width: "100%",
      tableStyle: "table table-hover table-bordered table-condensed"
    });

    datagrid.setResizeHeight("#responsibles-grid", heightFunc);

    datagrid.makeEditable("#responsibles-grid", {
      dialogId: "dialog-responsible",
      dialogConfirmId: "dialog-responsible-confirm-delete",
      dialogConfirmMessage:  this.locales["ResponsibleDelete_Title"],
      keyField: "ResponsibleId",
      addUrl: urls.responsibleAdd,
      editUrl: urls.responsibleEdit,
      deleteUrl: urls.responsibleDelete,
      editFormSelector: "#dialog-responsible form",
      initDialogControls: function() {
        multiselect.init("#dialog-responsible .multiselect");
      },
      validate: true
    });
  };

  return ResponsiblesView;
});