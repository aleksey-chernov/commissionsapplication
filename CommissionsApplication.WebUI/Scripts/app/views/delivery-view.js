﻿define("DeliveryView", function(require) {
  var AjaxView = require("AjaxView"),
    objectFix = require("objectFix");

  objectFix.enable();

  function DeliveryView() {
    AjaxView.call(this);
  };

  DeliveryView.prototype = Object.create(AjaxView.prototype);

  DeliveryView.prototype.render = function() {
    var btn = $("#delivery-btn"),
      resultDiv = $("#delivery-result"),
      url = $("#delivery-url").val(),
      alert = $("<div class='alert alert-error'>");

    btn.data("loading-text", "Загрузка...").click(function () {
      btn.button("loading");
      resultDiv.empty();

      $.get(url, function (result) {
        btn.button("reset");
        resultDiv.append(alert.html(result));
      });
    });
  };

  return DeliveryView;
});