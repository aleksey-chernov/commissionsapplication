﻿define("UsersView", function(require) {
  var AjaxView = require("AjaxView"),
    objectFix = require("objectFix");

  objectFix.enable();

  function UsersView(locales) {
    AjaxView.call(this);

    this.locales = locales;
  };

  UsersView.prototype = Object.create(AjaxView.prototype);

  UsersView.prototype.render = function() {
    var datagrid = require("datagrid"),
      multiselect = require("multiselect");

    var urls = {
          users: $("#users-url").val(),
          userAdd: $("#user-add-url").val(),
          userEdit: $("#user-edit-url").val(),
          userDelete: $("#user-delete-url").val(),
          userRoles: $("#user-roles-url").val()
        },
      heightFunc = function() {
        return $(window).height() - 160;
      };

    datagrid.init("#users-grid", {
      url: urls.users,
      columns: [
        { field: "UserId", visible: false },
        { field: "Name", title: this.locales["User_Name"], width: 100 },
        { field: "Login", title: this.locales["User_Login"], width: 100 },
        { field: "Email", title: this.locales["User_Email"], width: 100 },
        { field: "UserRole", title: this.locales["User_UserRole"], width: 100 }
      ],
      filterable: true,
      pageable: false,
      filters: [
        { field: "Name", title: this.locales["User_Name"], visible: true },
        { field: "Login", title: this.locales["User_Login"] },
        { field: "Email", title: this.locales["User_Email"] },
        {
          field: "UserRoles",
          title: this.locales["User_UserRole"],
          type: "multiselect",
          url: urls.userRoles,
          visible: true
        }
      ],
      height: heightFunc(),
      width: "100%",
      tableStyle: "table table-hover table-bordered table-condensed"
    });

    datagrid.setResizeHeight("#users-grid", heightFunc);

    datagrid.makeEditable("#users-grid", {
      dialogId: "dialog-user",
      dialogConfirmId: "dialog-user-confirm-delete",
      dialogConfirmMessage: this.locales["UserDelete_Title"],
      keyField: "UserId",
      addUrl: urls.userAdd,
      editUrl: urls.userEdit,
      deleteUrl: urls.userDelete,
      editFormSelector: "#dialog-user form",
      initDialogControls: function() {
        multiselect.init("#dialog-user .multiselect");
      },
      validate: true
    });
  };

  return UsersView;
});