﻿define("AjaxView", function () {
  function AjaxView() {
    $.ajaxSetup({ cache: false });

    $(document).ajaxError(function (e, xhr, settings, thrownError) {
      if (xhr.status === 403) {
        var response = $.parseJSON(xhr.responseText);
        window.location = response.LoginUrl;
      } else if (xhr.status !== 200 && xhr.status !== 0) {
        document.write(xhr.responseText);
      }
    });
  };

  return AjaxView;
});