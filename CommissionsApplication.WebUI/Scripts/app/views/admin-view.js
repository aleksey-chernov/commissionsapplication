﻿define("AdminView", function(require) {
  var AjaxView = require("AjaxView"),
    objectFix = require("objectFix"),
    datagrid = require("datagrid"),
    datepicker = require("datepicker"),
    editableselect = require("editableselect");

  objectFix.enable();

  function AdminView(locales) {
    AjaxView.call(this);

    this.locales = locales;
    this.urls = {
      commissions: $("#commissions-url").val(),
      commissionAdd: $("#commission-add-url").val(),
      commissionEdit: $("#commission-edit-url").val(),
      commissionDelete: $("#commission-delete-url").val(),
      commissionsExport: $("#commissions-export-url").val(),
      attachments: $("#attachments-url").val(),
      attachment: $("#attachment-url").val(),
      attachmentAdd: $("#attachment-add-url").val(),
      responsibles: $("#responsibles-url").val(),
      statuses: $("#statuses-url").val(),
      adminProps: $("#admin-props-url").val()
    };
    this.attachments = [];
  };

  AdminView.statusColors = [
    null,
    "lightcoral",
    "lightgreen",
    "lightyellow",
    "lightsalmon",
    "lightgray",
    "lightsteelblue"
  ];

  AdminView.prototype = Object.create(AjaxView.prototype);

  AdminView.prototype.render = function() {
    var that = this,
      heightFunc = function() {
        return $(window).height() - 160;
      },
      dialogAttach = $("#dialog-attachments");

    datagrid.init("#grid", {
      url: this.urls.commissions,
      columns: [
        { field: "CommissionId", hidden: true },
        { field: "CommissionStatusIndex", hidden: true },
        { field: "CommissionRowNumber", title: this.locales["Commission_CommissionRowNumber"], width: 60 },
        { field: "ControlDates", title: this.locales["Commission_ControlDates"], width: 90, sortable: false },
        { field: "CommissionStatus", title: this.locales["Commission_CommissionStatus"], width: 90 },
        { field: "OutcomeDocument", title: this.locales["Commission_OutcomeDocument"], width: 100 },
        { field: "IncomeDate", title: this.locales["Commission_IncomeDate"], width: 90 },
        {
          field: "IncomeDocument",
          title: this.locales["Commission_IncomeDocument"],
          width: 110,
          formatter: function(value, item) {
            return "<a href='#' class='attachments-show-ref' data-id='" + item["CommissionId"] + "' data-type='0'>" + value + "</a>";
          }
        },
        { field: "Summary", title: this.locales["Commission_Summary"], width: 290 },
        { field: "Organization", title: this.locales["Commission_Organization"], width: 120 },
        { field: "Signer", title: this.locales["Commission_Signer"], width: 120 },
        { field: "Resolution", title: this.locales["Commission_Resolution"], width: 310 },
        { field: "Responsibles", title: this.locales["Commission_Responsible"], width: 110, sortable: false },
        {
          field: "Commentary",
          title: this.locales["Commission_Commentary"],
          width: 450
          //formatter: function(value) {
          //  return value.length > 100
          //    ? value.substring(0, 100) + "..."
          //    : value;
          //}
        },
        {
          field: "ExecutionMark",
          title: this.locales["Commission_ExecutionMark"],
          width: 90,
          formatter: function(value, item) {
            return "<a href='#' class='attachments-show-ref' data-id='" + item["CommissionId"] + "' data-type='1'>" + value + "</a>";
          }
        },
        { field: "ExecutionDate", title: this.locales["Commission_ExecutionDate"], width: 100 },
        { field: "RegNumber", title: this.locales["Commission_RegNumber"], width: 75 },
        { field: "CommissionNumber", title: this.locales["Commission_CommissionNumber"], width: 80 },
        {
          field: "ImportanceFactor",
          title: this.locales["Commission_ImportanceFactor"],
          width: 100,
          formatter: function(value, item) {
            return "<div><a href='#' class='importance-tooltip' data-placement='bottom'" +
              " data-title='" + item["ImportanceFactorString"] + "'>" + value + "</a></div>";
          }
        },
        { field: "ImportanceFactorString", hidden: true },
        {
          field: "DelayFactor",
          title: this.locales["Commission_DelayFactor"],
          width: 100,
          formatter: function(value, item) {
            return "<div><a href='#' class='delay-tooltip' data-placement='bottom'" +
              " data-title='" + item["DelayFactorString"] + "'>" + value + "</a></div>";
          }
        },
        { field: "DelayInfo", title: this.locales["Commission_DelayInfo"], width: 100 },
        { field: "DelayFactorString", hidden: true },
        { field: "Reason", title: this.locales["Commission_Reason"], width: 100 }
      ],
      columnsSelect: true,
      refreshable: true,
      filterable: true,
      pageList: [
        {
          name: 100,
          size: 100
        },
        {
          name: 500,
          size: 500
        },
        {
          name: 1000,
          size: 1000
        }
      ],
      filters: [
        {
          field: "IncomeDate",
          title: this.locales["Commission_IncomeDate"],
          type: "daterange",
          visible: true,
          minDate: "01.01.2010",
          maxDate: "31.12.2020",
          format: "DD.MM.YYYY"
        },
        {
          field: "ControlDate",
          title: this.locales["Commission_ControlDates"],
          type: "daterange",
          visible: true,
          minDate: "01.01.2010",
          maxDate: "31.12.2020",
          format: "DD.MM.YYYY"
        },
        {
          field: "Responsibles",
          title: this.locales["Commission_Responsible"],
          type: "multiselect",
          url: this.urls.responsibles,
          visible: true,
          selectAll: true,
          multiselectFiltering: true
        },
        {
          field: "CommissionStatuses",
          title: this.locales["Commission_CommissionStatus"],
          type: "multiselect",
          url: this.urls.statuses,
          visible: true,
          selectAll: true
        },
        { field: "CommissionId", title: this.locales["Commission_CommissionRowNumber"] },
        { field: "OutcomeDocument", title: this.locales["Commission_OutcomeDocument"] },
        { field: "OutcomeDocument", title: this.locales["Commission_OutcomeDocument"] },
        { field: "IncomeDocument", title: this.locales["Commission_IncomeDocument"] },
        { field: "Summary", title: this.locales["Commission_Summary"] },
        { field: "Organization", title: this.locales["Commission_Organization"] },
        { field: "Signer", title: this.locales["Commission_Signer"] },
        { field: "Resolution", title: this.locales["Commission_Resolution"] },
        { field: "Commentary", title: this.locales["Commission_Commentary"] },
        { field: "ExecutionMark", title: this.locales["Commission_ExecutionMark"] },
        { field: "ExecutionDate", title: this.locales["Commission_ExecutionDate"], type: "date" },
        { field: "RegNumber", title: this.locales["Commission_RegNumber"] },
        //{ field: "ImportanceFactor", title: this.locales["Commission_ImportanceFactor"] },
        //{ field: "DelayFactor", title: this.locales["Commission_DelayFactor"] },
        { field: "Reason", title: this.locales["Commission_Reason"] }
      ],
      height: heightFunc(),
      width: "100%",
      tableStyle: "table table-hover table-bordered table-condensed",
      exportable: true,
      exportList: [
        {
          name: "Excel",
          type: "xls"
        }
      ],
      onExport: function(format, orderBy, filter) {
        var url = that.urls.commissionsExport + "?format=" + format;
        if (orderBy) {
          url += "&orderby=" + orderBy;
        }
        if (filter) {
          url += "&filter=" + filter;
        }
        window.open(url, "_blank");
      },
      onRowDisplay: function(row, item) {
        var color = AdminView.statusColors[item["CommissionStatusIndex"]];
        if (color) {
          row.css("background", color);
        }
      },
      onBodyShown: function() {
        $(".importance-tooltip, .delay-tooltip").tooltip();

        $(".attachments-show-ref").click(function() {
          var data = $(this).data();
          var attachmentsUrl = that.urls.attachments + "?id=" + data["id"] + "&type=" + data["type"];

          $.get(attachmentsUrl, function(result) {
            dialogAttach.html(result);
            dialogAttach.modal({ height: 300 });

            $(".attachments-ref").click(function(event) {
              event.preventDefault();

              var url = that.urls.attachment + "?id=" + $(this).data("id");
              window.open(url, "_blank");
            });
          });
        });
      }
    });
    datagrid.setResizeHeight("#grid", heightFunc);

    datagrid.makeEditable("#grid", {
      dialogId: "dialog-commission",
      dialogConfirmId: "dialog-confirm-delete",
      dialogConfirmMessage: that.locales["CommissionDelete_Title"],
      keyField: "CommissionId",
      addUrl: that.urls.commissionAdd,
      editUrl: that.urls.commissionEdit,
      deleteUrl: that.urls.commissionDelete,
      copyable: true,
      editFormSelector: "#dialog-commission form",
      initDialogControls: function() {
        editableselect.init("#dialog-commission .editable-select");
        datepicker.init("#dialog-commission .datepicker");

        that._initAttachmentModals();
        that._initChargesModal();
        that._initControlDatesModal();
      },
      validate: true,
      beforeSubmit: function() {
        $(".editable-select option").attr("selected", "selected");
      },
      successPromise: function(id, callback) {
        that._submitAttachmentModals(id, callback);
      }
    });
  };

  AdminView.prototype._initChargesModal = function() {
    var chargesDialogBtn = $("#charges-dialog-btn"),
      addBtn = $("#charges-add-btn"),
      deleteBtn = $(".charges-delete-btn"),
      chargesList = $("#charges-list"),
      responsiblesList = $("#charges-responsibles");

    var chargesBtnText = function() {
      var names = [];
      chargesList.find(".charges-name-span span").each(function() {
        names.push($(this).text());
      });
      return names.length === 0 ? "Не выбрано" : names.join(", ");
    };
    var updateInputNames = function() {
      chargesList.find(".row-fluid").each(function(index) {
        var row = $(this);

        row.find(".charges-chargeId").attr("name", "ChargeModels[" + index + "].ChargeId");
        row.find(".charges-responsibleId").attr("name", "ChargeModels[" + index + "].ResponsibleId");
        row.find(".charges-executionDate").attr("name", "ChargeModels[" + index + "].ExecutionDate");
      });
    };
    var responsibleIdExists = function(id) {
      var exists = false;
      chargesList.find(".row-fluid").each(function() {
        if ($(this).find(".charges-responsibleId").val() === id) {
          exists = true;
          return false;
        }
      });
      return exists;
    };
    var onDeleteRow = function() {
      $(this).parents(".row-fluid").remove();
      addBtn.prop("disabled", responsibleIdExists(responsiblesList.find("option:selected").val()));
      updateInputNames();
    };

    chargesDialogBtn.text(chargesBtnText());
    $("#dialog-charges").on("hidden", function() { chargesDialogBtn.text(chargesBtnText()) });

    deleteBtn.click(onDeleteRow);

    responsiblesList.change(function() {
      addBtn.prop("disabled", responsibleIdExists(responsiblesList.find("option:selected").val()));
    });
    addBtn.prop("disabled", responsibleIdExists(responsiblesList.find("option:selected").val()));
    addBtn.click(function() {
      var charge = {
        chargeId: 0,
        responsibleId: responsiblesList.find("option:selected").val(),
        responsibleName: responsiblesList.find("option:selected").text()
      };
      var newRow = $(
        "<div class='row-fluid'>" +
        "<div class='span6 charges-name-span'>" +
        "<input class='charges-chargeId' type='hidden' value='0'>" +
        "<input class='charges-responsibleId' type='hidden' value='" + charge.responsibleId + "'>" +
        "<span>" + charge.responsibleName + "</span>" +
        "</div>" +
        "<div class='span4'>" +
        "<input class='input-block-level datepicker charges-executionDate' type='text' value=''>" +
        "</div>" +
        "<div class='span2'>" +
        "<button class='btn btn-danger charges-btn'>Удалить</button>" +
        "</div>" +
        "</div>");

      chargesList.append(newRow);
      newRow.find(".charges-btn").click(onDeleteRow);

      datepicker.init("#charges-list .datepicker");
      addBtn.prop("disabled", true);
      updateInputNames();
    });
  };

  AdminView.prototype._initAttachmentModals = function() {
    var that = this;

    $(".attachments-ref").click(function() {
      var url = that.urls.attachment + "?id=" + $(this).data("id");
      window.open(url, "_blank");
    });

    $(".attachments-upload").each(function() {
      var $this = $(this),
        error = $(".attachments-error", $this.closest(".modal")),
        body = $(".modal-body", $this.closest(".modal"));

      $this.fileupload({
        url: that.urls.attachmentAdd,
        dataType: "json",
        autoUpload: false,
        add: function(e, data) {
          var file = data.files[0];

          if (file.size > (1024 * 1024 * 100)) {
            error.html("<div class='alert alert-error'>Слишком большой файл. Максимальный размер файла 100 Мб.</div>");
            return;
          }

          var newRow = $(
            "<div class='row-fluid'>" +
            "<div class='span10'>" +
            "<span>" + file.name + "</span>" +
            "</div>" +
            "<div class='span2'>" +
            "<button class='btn btn-danger attachments-delete-btn'>Удалить</button>" +
            "<button class='btn attachments-add-btn' style='display: none'>Добавить</button>" +
            "</div>" +
            "</div>");
          body.append(newRow);

          newRow.find(".attachments-add-btn").click(function(event) {
            event.preventDefault();

            that.attachments.push(data.submit());
          });
          newRow.find(".attachments-delete-btn").click(function(event) {
            event.preventDefault();

            newRow.remove();
          });
        }
      });
    });

    $(".attachments-delete-btn").click(function(event) {
      event.preventDefault();

      var $this = $(this);
      $this.siblings(".attachments-delete-checkbox")
        .prop("checked", true);
      $this.closest(".row-fluid")
        .css("display", "none");
    });
  };

  AdminView.prototype._submitAttachmentModals = function(id, callback) {
    $(".attachments-upload").each(function() {
      var $this = $(this);

      $this.fileupload(
        "option",
        "formData",
        [
          {
            name: "id",
            value: id
          },
          {
            name: "type",
            value: $this.data("type")
          }
        ]);
    });

    $(".attachments-add-btn").click();

    $.when.apply($, $.map(this.attachments, function(upload) {
      var deferred = $.Deferred();
      upload.always(function() { deferred.resolve() });
      return deferred.promise();
    })).done(callback);
  };

  AdminView.prototype._initControlDatesModal = function() {
    var dialogBtn = $("#control-dates-dialog-btn"),
      addBtn = $("#control-dates-add-btn"),
      deleteBtn = $(".control-dates-delete-btn"),
      controlDatesList = $("#control-dates-list"),
      controlDatesPicker = $("#control-dates-datepicker");

    var dialogBtnText = function() {
      return controlDatesList.find(".control-dates-date").last().val() || "Не выбрано";
    };
    var updateInputNames = function() {
      controlDatesList.find(".row-fluid").each(function(index) {
        var row = $(this);

        row.find(".control-dates-controlDateId").attr("name", "ControlDates[" + index + "].ControlDateId");
        row.find(".control-dates-date").attr("name", "ControlDates[" + index + "].Date");
      });
    };
    var onDeleteRow = function() {
      $(this).parents(".row-fluid").remove();
      updateInputNames();
    };

    datepicker.init("#control-dates-datepicker");

    dialogBtn.text(dialogBtnText());
    $("#dialog-control-dates").on("hidden", function() { dialogBtn.text(dialogBtnText()) });

    deleteBtn.click(onDeleteRow);

    controlDatesPicker.datepicker().on("changeDate", function() {
      addBtn.prop("disabled", controlDatesPicker.datepicker().val() === "");
    });
    addBtn.prop("disabled", true);
    addBtn.click(function() {
      var controlDate = {
        controlDateId: 0,
        date: controlDatesPicker.val()
      };
      var newRow = $(
        "<div class='row-fluid'>" +
        "<div class='span10 control-dates-span'>" +
        "<input class='control-dates-controlDateId' type='hidden' value='0'>" +
        "<input class='control-dates-date' type='hidden' value='" + controlDate.date + "'>" +
        "<span>" + controlDate.date + "</span>" +
        "</div>" +
        "<div class='span2'>" +
        "<button class='btn btn-danger control-dates-btn control-dates-delete-btn'>Удалить</button>" +
        "</div>" +
        "</div>");

      controlDatesList.append(newRow);
      newRow.find(".control-dates-delete-btn").click(onDeleteRow);

      updateInputNames();
    });
  };

  return AdminView;
});