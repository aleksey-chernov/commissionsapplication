﻿define("AssistView", function(require) {
  var AjaxView = require("AjaxView"),
    AdminView = require("AdminView"),
    objectFix = require("objectFix"),
    datagrid = require("datagrid");

  objectFix.enable();

  function AssistView(locales) {
    AjaxView.call(this);

    this.locales = locales;
    this.urls = {
      commissions: $("#commissions-url").val(),
      commissionsExport: $("#commissions-export-url").val(),
      attachments: $("#attachments-url").val(),
      attachment: $("#attachment-url").val(),
      responsibles: $("#responsibles-url").val(),
      statuses: $("#statuses-url").val()
    };
  };

  AssistView.prototype = Object.create(AjaxView.prototype);

  AssistView.prototype.render = function() {
    var that = this,
      heightFunc = function() {
        return $(window).height() - 120;
      },
      dialogAttach = $("#dialog-attachments");

    datagrid.init("#grid", {
      url: this.urls.commissions,
      columns: [
        { field: "CommissionId", hidden: true },
        { field: "CommissionStatusIndex", hidden: true },
        { field: "CommissionRowNumber", title: this.locales["Commission_CommissionRowNumber"], width: 60 },
        { field: "ControlDates", title: this.locales["Commission_ControlDates"], width: 90, sortable: false },
        { field: "CommissionStatus", title: this.locales["Commission_CommissionStatus"], width: 90 },
        { field: "OutcomeDocument", title: this.locales["Commission_OutcomeDocument"], width: 100 },
        { field: "IncomeDate", title: this.locales["Commission_IncomeDate"], width: 90 },
        {
          field: "IncomeDocument",
          title: this.locales["Commission_IncomeDocument"],
          width: 110,
          formatter: function(value, item) {
            return "<a href='#' class='attachments-show-ref' data-id='" + item["CommissionId"] + "' data-type='0'>" + value + "</a>";
          }
        },
        { field: "Summary", title: this.locales["Commission_Summary"], width: 290 },
        { field: "Organization", title: this.locales["Commission_Organization"], width: 120 },
        { field: "Signer", title: this.locales["Commission_Signer"], width: 120 },
        { field: "Resolution", title: this.locales["Commission_Resolution"], width: 310 },
        { field: "Responsibles", title: this.locales["Commission_Responsible"], width: 110, sortable: false },
        {
          field: "Commentary",
          title: this.locales["Commission_Commentary"],
          width: 450
          //formatter: function(value) {
          //  return value.length > 100
          //    ? value.substring(0, 100) + "..."
          //    : value;
          //}
        },
        {
          field: "ExecutionMark",
          title: this.locales["Commission_ExecutionMark"],
          width: 90,
          formatter: function(value, item) {
            return "<a href='#' class='attachments-show-ref' data-id='" + item["CommissionId"] + "' data-type='1'>" + value + "</a>";
          }
        },
        { field: "RegNumber", title: this.locales["Commission_RegNumber"], width: 75 }
      ],
      columnsSelect: true,
      refreshable: true,
      filterable: true,
      pageList: [
        {
          name: 100,
          size: 100
        },
        {
          name: 500,
          size: 500
        },
        {
          name: 1000,
          size: 1000
        }
      ],
      filters: [
        {
          field: "IncomeDate",
          title: this.locales["Commission_IncomeDate"],
          type: "daterange",
          visible: true,
          minDate: "01.01.2010",
          maxDate: "31.12.2020",
          format: "DD.MM.YYYY"
        },
        {
          field: "ControlDate",
          title: this.locales["Commission_ControlDates"],
          type: "daterange",
          visible: true,
          minDate: "01.01.2010",
          maxDate: "31.12.2020",
          format: "DD.MM.YYYY"
        },
        {
          field: "Responsibles",
          title: this.locales["Commission_Responsible"],
          type: "multiselect",
          url: this.urls.responsibles,
          visible: true,
          selectAll: true,
          multiselectFiltering: true
        },
        {
          field: "CommissionStatuses",
          title: this.locales["Commission_CommissionStatus"],
          type: "multiselect",
          url: this.urls.statuses,
          visible: true,
          selectAll: true
        },
        { field: "CommissionId", title: this.locales["Commission_CommissionRowNumber"] },
        { field: "OutcomeDocument", title: this.locales["Commission_OutcomeDocument"] },
        { field: "IncomeDocument", title: this.locales["Commission_IncomeDocument"] },
        { field: "Summary", title: this.locales["Commission_Summary"] },
        { field: "Organization", title: this.locales["Commission_Organization"] },
        { field: "Signer", title: this.locales["Commission_Signer"] },
        { field: "Resolution", title: this.locales["Commission_Resolution"] },
        { field: "Commentary", title: this.locales["Commission_Commentary"] },
        { field: "ExecutionMark", title: this.locales["Commission_ExecutionMark"] },
        { field: "RegNumber", title: this.locales["Commission_RegNumber"] }
      ],
      height: heightFunc(),
      width: "100%",
      tableStyle: "table table-hover table-bordered table-condensed",
      exportable: true,
      exportList: [
        {
          name: "Excel",
          type: "xls"
        }
      ],
      onExport: function(format, orderBy, filter) {
        var url = that.urls.commissionsExport + "?format=" + format;
        if (orderBy) {
          url += "&orderby=" + orderBy;
        }
        if (filter) {
          url += "&filter=" + filter;
        }
        window.open(url, "_blank");
      },
      onRowDisplay: function(row, item) {
        var color = AdminView.statusColors[item["CommissionStatusIndex"]];
        if (color) {
          row.css("background", color);
        }
      },
      onBodyShown: function() {
        $(".attachments-show-ref").click(function() {
          var data = $(this).data();
          var attachmentsUrl = that.urls.attachments + "?id=" + data["id"] + "&type=" + data["type"];

          $.get(attachmentsUrl, function(result) {
            dialogAttach.html(result);
            dialogAttach.modal({ height: 300 });

            $(".attachments-ref").click(function (event) {
              event.preventDefault();

              var url = that.urls.attachment + "?id=" + $(this).data("id");
              window.open(url, "_blank");
            });
          });
        });
      }
    });
    datagrid.setResizeHeight("#grid", heightFunc);
  };

  return AssistView;
});