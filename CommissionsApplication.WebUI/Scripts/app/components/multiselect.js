﻿define("multiselect", function () {
  function init(selector) {
    $(selector).multiselect({
      maxHeight: 200,
      buttonContainer: "<div class='btn-group' style='margin-bottom: 10px'></div>",
      buttonWidth: "100%",
      buttonText: function (options) {
        if (options.length === 0) {
          return "Не выбрано";
        } else {
          var labels = [];
          options.each(function () {
            if ($(this).attr("label") !== undefined) {
              labels.push($(this).attr("label"));
            } else {
              labels.push($(this).html());
            }
          });
          return labels.join(", ") + "";
        }
      }
    });
  };

  return {
    init: init
  };
});