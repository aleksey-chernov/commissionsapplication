﻿define("editableselect", function () {
  function init(selector) {
    $(selector).editableSelect("init", {
      containerStyle: { display: "block" },
      btnGroupClass: "btn-group",
      addBtnClass: "btn",
      addBtnHtml: "<i class='icon-plus'></i>",
      removeBtnClass: "btn",
      removeBtnHtml: "<i class='icon-minus'></i>",
      inputClass: "editable-select-input datepicker"
    });
  };

  return {
    init: init
  };
});