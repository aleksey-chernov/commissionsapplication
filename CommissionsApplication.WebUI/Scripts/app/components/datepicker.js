﻿define("datepicker", function() {
  function init(selector) {
    $(selector).datepicker({
      format: "dd.mm.yyyy",
      language: "ru",
      autoclose: true,
      todayHighlight: true,
      clearBtn: true
    });
  };

  return {
    init: init
  };
});