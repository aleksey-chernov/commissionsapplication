﻿define("formValidation", function () {
  function init(selector) {
    $(selector).removeData("validator")
      .removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse(selector);

    $.validator.methods.date = function (value, element) {
      Globalize.culture("ru-RU");
      return this.optional(element) || Globalize.parseDate(value) !== null;
    }

    $.validator.methods.number = function (value, element) {
      return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
    };

    $(selector).unbind("submit");
  };

  return {
    init: init
  };
});