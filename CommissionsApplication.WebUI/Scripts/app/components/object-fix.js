﻿define("objectFix", function() {
  function enable() {
    if (!Object.create) {
      Object.create = function(proto, props) {
        if (typeof props !== "undefined") {
          throw "The multiple-argument version of Object.create is not provided by this browser and cannot be shimmed.";
        }

        function ctor() {}

        ctor.prototype = proto;
        return new ctor();
      };
    }
  };

  return {
    enable: enable
  };
});