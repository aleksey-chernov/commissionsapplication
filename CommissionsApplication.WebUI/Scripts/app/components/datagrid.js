﻿define("datagrid", function(require) {
  var formValidation = require("formValidation");

  function init(selector, options) {
    var grid = $(selector);

    grid.bootstrapGrid("init", options);
  };

  function makeEditable(selector, options) {
    var grid = $(selector);

    var dialog = $("<div class='modal fade datagrid-modal' id='" + options.dialogId + "' style='display: none' tabindex='-1' data-keyboard='false' data-backdrop='static'></div>");
    var dialogConfirm = $("<div class='modal fade datagrid-modal' id='" + options.dialogConfirmId + "' style='display: none' tabindex='-1'>" +
      " <div class='modal-header'>" +
      "   <h4>" + options.dialogConfirmMessage + "</h4>" +
      " </div>" +
      " <div class='modal-footer'>" +
      "   <button class='btn' data-dismiss='modal'>Отмена</button>" +
      "   <a class='btn btn-danger'>Удалить</a>" +
      " </div>" +
      "</div>");

    dialog.insertAfter(grid);
    dialogConfirm.insertAfter(grid);

    if (grid.data("bootstrap-grid")) {
      grid.bootstrapGrid("setOption", "editableContext", true)
        .bootstrapGrid("setOption", "onRowContextAdd", function() {
          $.get(options.addUrl, function(result) {
            dialog.html(result)
              .data("type", "add");

            $(".modal-body", dialog).css({ opacity: 0.0, visibility: "hidden" });

            dialog.modal();
          });
        })
        .bootstrapGrid("setOption", "onRowContextEdit", function(item) {
          $.get(options.editUrl + "/" + item[options.keyField], function(result) {
            dialog.html(result)
              .data("type", "edit");

            $(".modal-body", dialog).css({ opacity: 0.0, visibility: "hidden" });

            dialog.modal();
          });
        })
        .bootstrapGrid("setOption", "onRowContextRemove", function(item) {
          dialogConfirm.modal();
          $(".btn-danger", dialogConfirm).off("click").on("click", function (event) {
            event.preventDefault();

            $.post(options.deleteUrl + "/" + item[options.keyField], function() {
              dialogConfirm.modal("hide");
              grid.bootstrapGrid("refresh");
            });
          });
        });

      if (options.copyable) {
        grid.bootstrapGrid("setOption", "onRowContextCopy", function(item) {
          $.get(options.addUrl + "/" + item[options.keyField], function (result) {
            dialog.html(result)
              .data("type", "add");

            $(".modal-body", dialog).css({ opacity: 0.0, visibility: "hidden" });

            dialog.modal();
          });
        });
      }

      grid.data("bootstrap-grid").display.initContextMenu();
    }

    var initDialogControls = function() {
      if (options.initDialogControls) {
        options.initDialogControls();
      }

      if (options.validate) {
        formValidation.init(options.editFormSelector);
      }

      $(options.editFormSelector)
        .submit(function(event) {
          event.preventDefault();

          if (options.validate && !$(this).valid())
            return;

          if (options.beforeSubmit) {
            options.beforeSubmit();
          }

          var url = dialog.data("type") === "add"
            ? options.addUrl : options.editUrl;

          dialog.modal("loading");

          $.ajax({
            type: "post",
            url: url,
            data: $(options.editFormSelector).serialize(),
            success: function(result) {
              if (result.id) {
                if (options.successPromise) {
                  options.successPromise(result.id, function() {
                    dialog.modal("hide");
                  });
                  return;
                }

                if (options.successSubmit) {
                  options.successSubmit(result.id);
                }

                dialog.modal("hide");
              } else {
                dialog.html(result)
                  .modal("loading");

                initDialogControls();
              }
            }
          });
        });
    };

    dialog.on("hidden", function() {
        dialog.empty();
        grid.bootstrapGrid("refresh");
      })
      .on("shown", function() {
        initDialogControls();

        $(".modal-body", this.dialog)
          .css({ opacity: 0.0, visibility: "visible" })
          .animate({ opacity: 1.0 }, 250);
      });
  };

  function setResizeHeight(selector, heightFunc) {
    var grid = $(selector);

    $(window).resize(function() {
      grid.bootstrapGrid("setOption", "height", heightFunc())
        .bootstrapGrid("redraw");
    });
  }

  return {
    init: init,
    makeEditable: makeEditable,
    setResizeHeight: setResizeHeight
  };
});