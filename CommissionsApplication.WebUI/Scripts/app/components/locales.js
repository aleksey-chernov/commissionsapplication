﻿define("locales", function() {
  function load(url) {
    var deferred = $.Deferred();

    $.get(url, function(result) {
      deferred.resolve(result.locales);
    });

    return deferred.promise();
  };

  return {
    load: load
  }
});