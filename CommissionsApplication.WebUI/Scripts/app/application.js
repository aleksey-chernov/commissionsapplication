﻿var application = function(baseUrl) {
  require.config({
    baseUrl: baseUrl + "/Scripts",
    paths: {
      //Application
      "application": "app/application",

      //Components
      "datagrid": "app/components/datagrid",
      "datepicker": "app/components/datepicker",
      "editableselect": "app/components/editableselect",
      "multiselect": "app/components/multiselect",
      "formValidation": "app/components/form-validation",
      "objectFix": "app/components/object-fix",
      "locales": "app/components/locales",

      //Views
      "AjaxView": "app/views/ajax-view",
      "AdminView": "app/views/admin-view",
      "AssistView": "app/views/assist-view",
      "UsersView": "app/views/users-view",
      "ResponsiblesView": "app/views/responsibles-view",
      "DeliveryView": "app/views/delivery-view"

      //Models
      //"User": "app/models/user"
    }
  });
}