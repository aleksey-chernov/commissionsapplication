﻿define("User", function(require) {
  function User(name, roles) {
    this.name = name;
    this.roles = roles;
  };

  User.prototype.inRole = function (role) {
    var that = this;

    return $.inArray(role, that.roles) !== -1;
  }

  return User;
});