﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Models.Abstract
{
    public interface IUserModel
    {
        int UserId { get; set; }

        byte[] RowVersion { get; set; }

        [Display(ResourceType = typeof(Global), Name = "User_Name")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        string Name { get; set; }

        [Display(ResourceType = typeof (Global), Name = "User_Login")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        [StringLength(99, ErrorMessageResourceName = "FieldIsTooLong", ErrorMessageResourceType = typeof(Validation))]
        [Remote("ValidateUserLoginExists", "Users", AdditionalFields = "UserId")]
        string Login { get; set; }

        [Display(ResourceType = typeof (Global), Name = "User_Email")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        [StringLength(99, ErrorMessageResourceName = "FieldIsTooLong", ErrorMessageResourceType = typeof(Validation))]
        [Email(ErrorMessageResourceName = "EmailFormatInvalid", ErrorMessageResourceType = typeof(Validation))]
        [Remote("ValidateUserEmailExists", "Users", AdditionalFields = "UserId")]
        string Email { get; set; }

        [Display(ResourceType = typeof(Global), Name = "User_Password")]
        string Password { get; set; }

        [Display(ResourceType = typeof(Global), Name = "User_RepeatPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof (Validation), ErrorMessageResourceName = "UserRepeatPasswordInvalid")]
        string RepeatPassword { get; set; }

        [Display(ResourceType = typeof (Validation), Name = "User_UserRole")]
        UserRoleModel.Role UserRole { get; set; }

        [Display(ResourceType = typeof(Validation), Name = "User_Responsibles")]
        ICollection<int> SelectedResponsibleIds { get; set; }
        
        IEnumerable<SelectListItem> ResponsiblesList { get; set; }
    }
}