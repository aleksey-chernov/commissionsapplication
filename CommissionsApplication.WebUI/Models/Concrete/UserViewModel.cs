﻿namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string UserRole { get; set; }
    }
}