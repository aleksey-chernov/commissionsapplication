﻿using System.ComponentModel.DataAnnotations;
using CommissionsApplication.WebUI.App_GlobalResources;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class LoginViewModel
    {
        [Display(ResourceType = typeof(Global), Name = "Login_Login")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        public string Login { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Login_Password")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        public string Password { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Login_IsPersistent")]
        public bool IsPersistent { get; set; }
    }
}