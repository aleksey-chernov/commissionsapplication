﻿namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class ControlDateModel
    {
        public int ControlDateId { get; set; }

        public string Date { get; set; }
    }
}