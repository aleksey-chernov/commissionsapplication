﻿using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class UserRoleModel
    {
        public enum Role
        {
            [DropDownName(typeof(Global), ("UserRole_Administrator"))]
            Administrator,

            [DropDownName(typeof(Global), ("UserRole_Assistant"))]
            Assistant
        }
    }
}