﻿using System;
using System.Collections.Generic;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class CommissionFilterModel
    {
        public IEnumerable<Commission.Status> CommissionStatuses { get; set; }

        public string OutcomeDocument { get; set; }

        public string IncomeDocument { get; set; }

        public DateRange IncomeDate { get; set; }

        public string Summary { get; set; }

        public string Organization { get; set; }

        public string Signer { get; set; }

        public string Resolution { get; set; }

        public DateTime? CommissionDate { get; set; }

        public string Commentary { get; set; }

        public string ExecutionMark { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string RegNumber { get; set; }

        public string CommissionNumber { get; set; }

        public string ImportanceFactor { get; set; }

        public string DelayFactor { get; set; }

        public string Reason { get; set; }

        public IEnumerable<int> Responsibles { get; set; }

        public DateRange ControlDate { get; set; }
    }
}