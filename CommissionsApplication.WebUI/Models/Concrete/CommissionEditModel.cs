﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.App_GlobalResources;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class CommissionEditModel
    {
        public CommissionEditModel()
        {
            ControlDates = new List<ControlDateModel>();
            ChargeModels = new List<ChargeModel>();
            AttachmentsIncome = new List<AttachmentModel>();
            AttachmentsOutcome = new List<AttachmentModel>();
        }

        public int CommissionId { get; set; }

        public byte[] RowVersion { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_CommissionStatus")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof (Validation))]
        public CommissionStatusModel.Status CommissionStatus { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_OutcomeDocument")]
        public string OutcomeDocument { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_IncomeDocument")]
        public string IncomeDocument { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_IncomeDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof (Validation))]
        public DateTime IncomeDate { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Summary")]
        public string Summary { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Organization")]
        public string Organization { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Signer")]
        public string Signer { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Resolution")]
        public string Resolution { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Commentary")]
        public string Commentary { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_ExecutionMark")]
        public string ExecutionMark { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_ExecutionDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? ExecutionDate { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_RegNumber")]
        public string RegNumber { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_CommissionNumber")]
        public string CommissionNumber { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_ImportanceFactor")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof (Validation))]
        [Range(1, 3, ErrorMessageResourceName = "RangeValueInvalid", ErrorMessageResourceType = typeof (Validation))]
        public int ImportanceFactor { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_DelayFactor")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal? DelayFactor { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_DelayInfo")]
        public string DelayInfo { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Reason")]
        public string Reason { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_ControlDates")]
        public IList<ControlDateModel> ControlDates { get; set; }

        [Display(ResourceType = typeof (Global), Name = "Commission_Responsible")]
        public IList<ChargeModel> ChargeModels { get; set; }

        public IEnumerable<SelectListItem> ResponsiblesList { get; set; }

        public IList<AttachmentModel> AttachmentsIncome { get; set; }

        public IList<AttachmentModel> AttachmentsOutcome { get; set; }
    }
}