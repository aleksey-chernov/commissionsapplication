﻿namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class ResponsibleViewModel
    {
        public int ResponsibleId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}