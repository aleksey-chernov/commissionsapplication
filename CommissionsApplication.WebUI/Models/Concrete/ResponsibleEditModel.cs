﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class ResponsibleEditModel
    {
        public ResponsibleEditModel()
        {
            SelectedUserIds = new List<int>();
        }

        public int ResponsibleId { get; set; }

        public byte[] RowVersion { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Responsible_Name")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        public string Name { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Responsible_Email")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        [StringLength(99, ErrorMessageResourceName = "FieldIsTooLong", ErrorMessageResourceType = typeof(Validation))]
        [Email(ErrorMessageResourceName = "EmailFormatInvalid", ErrorMessageResourceType = typeof(Validation))]
        [Remote("ValidateResponsibleEmailExists", "Users", AdditionalFields = "ResponsibleId")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Responsible_Users")]
        public ICollection<int> SelectedUserIds { get; set; }

        public IEnumerable<SelectListItem> UsersList { get; set; }
    }
}