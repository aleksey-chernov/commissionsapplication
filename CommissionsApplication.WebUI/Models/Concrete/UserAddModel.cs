﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Models.Abstract;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class UserAddModel : IUserModel
    {
        public UserAddModel()
        {
            SelectedResponsibleIds = new List<int>();
        }

        public int UserId { get; set; }

        public byte[] RowVersion { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        [Display(ResourceType = typeof (Global), Name = "User_Password")]
        [Required(ErrorMessageResourceName = "PropertyValueRequired", ErrorMessageResourceType = typeof(Validation))]
        public string Password { get; set; }

        public string RepeatPassword { get; set; }

        public UserRoleModel.Role UserRole { get; set; }

        public ICollection<int> SelectedResponsibleIds { get; set; }
        
        public IEnumerable<SelectListItem> ResponsiblesList { get; set; }
    }
}