﻿using System.Collections.Generic;
using CommissionsApplication.WebUI.App_GlobalResources;
using SharpExport.Attributes;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    [ExportTable(typeof(Global), NameResource = "Commission_CommissionsName")]
    public class CommissionViewModel
    {
        public static readonly List<string> AdminProperties = new List<string>
        {
            "ExecutionDate", "CommissionNumber", "ImportanceFactor",
            "DelayFactor", "DelayInfo", "Reason", "Responsibles"
        };
        
        public int CommissionId { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_CommissionRowNumber", Width = 60, Index = 1)]
        public int CommissionRowNumber { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_ControlDates", Width = 90, Index = 2)]
        public string ControlDates { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_CommissionStatus", Width = 90, Index = 3)]
        public string CommissionStatus { get; set; }

        public int CommissionStatusIndex { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_OutcomeDocument", Width = 100, Index = 4)]
        public string OutcomeDocument { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_IncomeDate", Width = 90, Index = 5)]
        public string IncomeDate { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_IncomeDocument", Width = 110, Index = 6)]
        public string IncomeDocument { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Summary", Width = 290, Index = 7)]
        public string Summary { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Organization", Width = 120, Index = 8)]
        public string Organization { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Signer", Width = 120, Index = 9)]
        public string Signer { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Resolution", Width = 310, Index = 10)]
        public string Resolution { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Responsible", Width = 110, Index = 11)]
        public string Responsibles { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Commentary", Width = 450, Index = 12)]
        public string Commentary { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_ExecutionMark", Width = 90, Index = 13)]
        public string ExecutionMark { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_ExecutionDate", Width = 100, Index = 14)]
        public string ExecutionDate { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_RegNumber", Width = 75, Index = 15)]
        public string RegNumber { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_CommissionNumber", Width = 80, Index = 16)]
        public string CommissionNumber { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_ImportanceFactor", Width = 100, Index = 17)]
        public int ImportanceFactor { get; set; }

        public string ImportanceFactorString { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_DelayFactor", Width = 100, Index = 18)]
        public string DelayFactor { get; set; }

        public string DelayFactorString { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_DelayInfo", Width = 100, Index = 19)]
        public string DelayInfo { get; set; }

        [ExportColumn(typeof(Global), NameResource = "Commission_Reason", Width = 100, Index = 20)]
        public string Reason { get; set; }
    }
}