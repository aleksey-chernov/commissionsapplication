﻿using System;
using System.ComponentModel.DataAnnotations;
using CommissionsApplication.WebUI.App_GlobalResources;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class ChargeModel
    {
        public int ChargeId { get; set; }

        public int ResponsibleId { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Charge_ResponsibleName")]
        public string ResponsibleName { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Charge_ExecutionDate")]
        public DateTime? ExecutionDate { get; set; }
    }
}