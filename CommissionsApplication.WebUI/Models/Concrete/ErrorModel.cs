﻿namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class ErrorModel
    {
        public string Message { get; set; }
    }
}