﻿using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class CommissionStatusModel
    {
        public enum Status
        {
            [DropDownName(typeof(Global), "CommissionStatus_None")]
            None,

            [DropDownName(typeof(Global), "CommissionStatus_NotExecuted")]
            NotExecuted,

            [DropDownName(typeof(Global), "CommissionStatus_Executed")]
            Executed,

            [DropDownName(typeof(Global), "CommissionStatus_ThisWeek")]
            ThisWeek,

            [DropDownName(typeof(Global), "CommissionStatus_NextWeek")]
            NextWeek,

            [DropDownName(typeof(Global), "CommissionStatus_Question")]
            Question,

            [DropDownName(typeof(Global), "CommissionStatus_Transfer")]
            Transfer
        }
    }
}