﻿using System.Collections.Generic;
using System.Web.Mvc;
using CommissionsApplication.WebUI.Models.Abstract;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class UserEditModel : IUserModel
    {
        public UserEditModel()
        {
            SelectedResponsibleIds = new List<int>();
        }

        public int UserId { get; set; }

        public byte[] RowVersion { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string RepeatPassword { get; set; }

        public UserRoleModel.Role UserRole { get; set; }
        
        public ICollection<int> SelectedResponsibleIds { get; set; }

        public IEnumerable<SelectListItem> ResponsiblesList { get; set; }
    }
}