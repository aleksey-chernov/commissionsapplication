﻿namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class CommissionAssistViewModel
    {
        public int CommissionId { get; set; }

        public int CommissionRowNumber { get; set; }

        public string ControlDates { get; set; }

        public string CommissionStatus { get; set; }

        public int CommissionStatusIndex { get; set; }

        public string OutcomeDocument { get; set; }

        public string IncomeDate { get; set; }

        public string IncomeDocument { get; set; }

        public string Summary { get; set; }

        public string Organization { get; set; }

        public string Signer { get; set; }

        public string Resolution { get; set; }

        public string Responsibles { get; set; }

        public string Commentary { get; set; }

        public string ExecutionMark { get; set; }

        public string RegNumber { get; set; }
    }
}