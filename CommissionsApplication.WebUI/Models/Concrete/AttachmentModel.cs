﻿using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.WebUI.Models.Concrete
{
    public class AttachmentModel
    {
        public int AttachmentId { get; set; }

        public int CommissionId { get; set; }

        public Attachment.Type AttachmentType { get; set; }
        
        public string Name { get; set; }

        public string ContentType { get; set; }

        public bool IsDeleted { get; set; }
    }
}