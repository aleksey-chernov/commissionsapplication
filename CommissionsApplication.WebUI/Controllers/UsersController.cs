﻿using System;
using System.Linq;
using System.Web.Mvc;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Infrastructure.Filters;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Controllers
{
    [AjaxAuthorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UsersController(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [AjaxOnly]
        public ActionResult Users(string orderBy = null, [JsonBinder] UserFilter filter = null)
        {
            var userModels = _unitOfWork.UserRepository.GetAll(orderBy, filter)
                .Select(u => _mapper.UserViewModelMapper.Map(u))
                .ToList();

            return Json(new
            {
                totalItems = userModels.Count,
                items = userModels
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult UserEdit(int id)
        {
            var user = _unitOfWork.UserRepository.GetByKey(id);
            if (user == null)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_UserRemoved});

            var userModel = _mapper.UserEditModelMapper.Map(user);

            ViewBag.Title = Global.UserEdit_Title;

            return PartialView(userModel);
        }

        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit(UserEditModel userModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_UserValidation});
            if (!_unitOfWork.UserRepository.Exists(userModel.UserId))
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_UserRemoved});

            var user = _mapper.UserMapper.Map(userModel);
            if (string.IsNullOrWhiteSpace(userModel.Password))
            {
                user.Password = _unitOfWork.UserRepository.GetPasswordByKey(userModel.UserId);
            }
            try
            {
                _unitOfWork.UserRepository.Update(user);
                _unitOfWork.Commit();

                return Json(new
                {
                    id = user.UserId
                });
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_UserConcurrency});
            }
            catch (RepositoryDuplicateException)
            {
                //TODO: Not user friendly
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_UserValidation });
            }
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult UserAdd()
        {
            var userModel = _mapper.UserEditModelMapper.Map(new User());

            ViewBag.Title = Global.UserAdd_Title;

            return PartialView("UserEdit", userModel);
        }

        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult UserAdd(UserAddModel userModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_UserValidation});

            var user = _mapper.UserMapper.Map(userModel);
            try
            {
                user = _unitOfWork.UserRepository.Insert(user);
                _unitOfWork.Commit();

                return Json(new
                {
                    id = user.UserId
                });
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_UserConcurrency });
            }
            catch (RepositoryDuplicateException)
            {
                //TODO: Not user friendly
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_UserValidation });
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult UserDelete(int id)
        {
            _unitOfWork.UserRepository.DeleteByKey(id);
            _unitOfWork.Commit();

            return new EmptyResult();
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult UserRoles()
        {
            var usersList =
                Enum.GetValues(typeof (UserRoleModel.Role))
                    .Cast<UserRoleModel.Role>()
                    .Select(s => new
                    {
                        Text =
                            typeof (UserRoleModel.Role).GetField(s.ToString())
                                .GetCustomAttributes(typeof (DropDownNameAttribute), false)
                                .OfType<DropDownNameAttribute>()
                                .First()
                                .Name,
                        Value = s
                    });

            return Json(new
            {
                List = usersList
            }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public ActionResult Responsibles(string orderBy = null, [JsonBinder] ResponsibleFilter filter = null)
        {
            var responsibleModels = _unitOfWork.ResponsibleRepository.GetAll(orderBy, filter)
                .Select(r => _mapper.ResponsibleViewModelMapper.Map(r))
                .ToList();

            return Json(new
            {
                totalItems = responsibleModels.Count,
                items = responsibleModels
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult ResponsibleEdit(int id)
        {
            var responsible = _unitOfWork.ResponsibleRepository.GetByKey(id);
            if (responsible == null)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_ResponsibleRemoved});

            var responsibleModel = _mapper.ResponsibleEditModelMapper.Map(responsible);

            ViewBag.Title = Global.ResponsibleEdit_Title;

            return PartialView(responsibleModel);
        }

        [AjaxOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResponsibleEdit(ResponsibleEditModel responsibleModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_ResponsibleValidation});
            if (!_unitOfWork.ResponsibleRepository.Exists(responsibleModel.ResponsibleId))
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_ResponsibleRemoved});

            var responsible = _mapper.ResponsibleMapper.Map(responsibleModel);
            try
            {
                _unitOfWork.ResponsibleRepository.Update(responsible);
                _unitOfWork.Commit();

                return Json(new
                {
                    id = responsible.ResponsibleId
                });
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_ResponsibleConcurrency});
            }
            catch (RepositoryDuplicateException)
            {
                //TODO: Not user friendly
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_ResponsibleValidation });
            }
        }

        [HttpGet]
        [AjaxOnly]
        public ActionResult ResponsibleAdd()
        {
            var responsibleModel = new ResponsibleEditModel
            {
                UsersList = _unitOfWork.UserRepository.GetAll()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.UserId.ToString()
                    })
            };

            ViewBag.Title = Global.ResponsibleAdd_Title;

            return PartialView("ResponsibleEdit", responsibleModel);
        }

        [HttpPost]
        [AjaxOnly]
        [ValidateAntiForgeryToken]
        public ActionResult ResponsibleAdd(ResponsibleEditModel responsibleModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_ResponsibleValidation});

            var responsible = _mapper.ResponsibleMapper.Map(responsibleModel);
            try
            {
                responsible = _unitOfWork.ResponsibleRepository.Insert(responsible);
                _unitOfWork.Commit();

                return Json(new
                {
                    id = responsible.ResponsibleId
                });
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_ResponsibleConcurrency });
            }
            catch (RepositoryDuplicateException)
            {
                //TODO: Not user friendly
                return PartialView("ModalError", new ErrorModel { Message = Global.Error_ResponsibleValidation });
            }
        }

        [AjaxOnly]
        [HttpPost]
        public ActionResult ResponsibleDelete(int id)
        {
            _unitOfWork.ResponsibleRepository.DeleteByKey(id);
            _unitOfWork.Commit();

            return new EmptyResult();
        }

        [AjaxOnly]
        [HttpGet]
        public JsonResult ValidateUserEmailExists(string email, int userId)
        {
            if (_unitOfWork.UserRepository.UserEmailUniqueForKey(email, userId))
            {
                return Json(Validation.User_EmailIsNotUnique, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [HttpGet]
        public JsonResult ValidateUserLoginExists(string login, int userId)
        {
            if (_unitOfWork.UserRepository.UserLoginUniqueForKey(login, userId))
            {
                return Json(Validation.User_LoginIsNotUnique, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [HttpGet]
        public JsonResult ValidateResponsibleEmailExists(string email, int responsibleId)
        {
            if (_unitOfWork.ResponsibleRepository.ResponsibleEmailUniqueForKey(email, responsibleId))
            {
                return Json(Validation.Responsible_EmailIsNotUnique, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}