﻿using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Filters;

namespace CommissionsApplication.WebUI.Controllers
{
    public class GlobalController : Controller
    {
        [AjaxOnly]
        public ActionResult Locales()
        {
            var locales = typeof (Global).GetProperties(BindingFlags.Public | BindingFlags.Static)
                .Where(prop => prop.PropertyType == typeof (string))
                .ToDictionary(prop => prop.Name, prop => (string) prop.GetValue(null, null));

            return Json(new
            {
                locales
            }, JsonRequestBehavior.AllowGet);
        }
    }
}