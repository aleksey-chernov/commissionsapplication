﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Infrastructure.Export.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Export.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Filters;
using CommissionsApplication.WebUI.Infrastructure.Mail.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Results;
using CommissionsApplication.WebUI.Models.Concrete;
using Elmah;
using MessageAttachment = System.Net.Mail.Attachment;

namespace CommissionsApplication.WebUI.Controllers
{
    public class ManagementController : Controller
    {
        private readonly IExportService<CommissionViewModel> _exportService;
        private readonly IMailManager _mailService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ManagementController(IUnitOfWork unitOfWork,
            IExportService<CommissionViewModel> exportService,
            IMapper mapper, IMailManager mailService)
        {
            _unitOfWork = unitOfWork;
            _exportService = exportService;
            _mapper = mapper;
            _mailService = mailService;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View(User.IsInRole("Administrator") ? "Admin" : "Assist");
        }

        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult Commissions(int pageIndex = 1, int? pageSize = null, string orderBy = null,
            [JsonBinder] CommissionFilter filter = null)
        {
            if (User.IsInRole("Assistant"))
            {
                filter = AssistCommissionFilter(filter);
                if (!filter.Responsibles.Any())
                {
                    return Json(new
                    {
                        totalItems = 0
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            int count;

            IEnumerable<Commission> commissions;
            if (pageSize.HasValue)
            {
                commissions = _unitOfWork.CommissionRepository.GetAllPaginated(out count, new PageCriteria
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize.Value
                }, orderBy, filter);
            }
            else
            {
                commissions = _unitOfWork.CommissionRepository.GetAll(orderBy, filter);
                count = ((IList<Commission>) commissions).Count;
            }

            object commissionModels = commissions.Select(c => _mapper.CommissionViewModelMapper.Map(c));
            if (User.IsInRole("Assistant"))
                commissionModels =
                    ((IEnumerable<CommissionViewModel>) commissionModels).Select(
                        cm => _mapper.CommissionAssistViewModelMapper.Map(cm));

            return Json(new
            {
                totalItems = count,
                items = commissionModels
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult CommissionsExport(string format, string orderBy = null,
            [JsonBinder] CommissionFilter filter = null)
        {
            ExportOptions exportOptions = null;

            if (User.IsInRole("Assistant"))
            {
                filter = AssistCommissionFilter(filter);
                exportOptions = new ExportOptions
                {
                    IgnoreColumns = typeof (CommissionViewModel).GetProperties().Select(prop => prop.Name)
                        .Except(typeof (CommissionAssistViewModel).GetProperties().Select(prop => prop.Name)).ToArray()
                };
            }

            var commissions = _unitOfWork.CommissionRepository.GetAll(orderBy, filter)
                .Select(c => _mapper.CommissionViewModelMapper.Map(c));

            switch (format)
            {
                default:
                    return File(_exportService.ToExcel(commissions, exportOptions),
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Таблица поручений.xlsx");
            }
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        public ActionResult CommissionEdit(int id)
        {
            var commission = _unitOfWork.CommissionRepository.GetByKey(id);
            if (commission == null)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionRemoved});

            var commissionModel = _mapper.CommissionEditModelMapper.Map(commission);

            ViewBag.Title = Global.CommissionEdit_Title;

            return PartialView(commissionModel);
        }

        [HttpPost]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult CommissionEdit(CommissionEditModel commissionModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionValidation});
            if (!_unitOfWork.CommissionRepository.Exists(commissionModel.CommissionId))
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionRemoved});

            var commission = _mapper.CommissionMapper.Map(commissionModel);
            try
            {
                _unitOfWork.CommissionRepository.Update(commission);
                _unitOfWork.Commit();
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionConcurrency});
            }

            return Json(new
            {
                id = commission.CommissionId
            });
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        public ActionResult CommissionAdd(int? id = null)
        {
            Commission commission;
            if (id.HasValue)
            {
                commission = _unitOfWork.CommissionRepository.GetByKey(id.Value);
                if (commission == null)
                    return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionRemoved});

                commission.CommissionId = 0;
                commission.Attachments.Clear();
                commission.Charges.Clear();
            }
            else
            {
                commission = new Commission();
            }

            var commissionModel = _mapper.CommissionEditModelMapper.Map(commission);

            ViewBag.Title = Global.CommissionAdd_Title;

            return PartialView("CommissionEdit", commissionModel);
        }

        [HttpPost]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult CommissionAdd(CommissionEditModel commissionModel)
        {
            if (!ModelState.IsValid)
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionValidation});

            var commission = _mapper.CommissionMapper.Map(commissionModel);
            try
            {
                commission = _unitOfWork.CommissionRepository.Insert(commission);
                _unitOfWork.Commit();
            }
            catch (RepositoryConcurrencyException)
            {
                return PartialView("ModalError", new ErrorModel {Message = Global.Error_CommissionConcurrency});
            }

            return Json(new
            {
                id = commission.CommissionId
            });
        }

        [HttpPost]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        public ActionResult CommissionDelete(int id)
        {
            _unitOfWork.CommissionRepository.DeleteByKey(id);
            _unitOfWork.Commit();

            return new EmptyResult();
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult CommissionAttachments(int id, Attachment.Type type)
        {
            if (User.IsInRole("Assistant"))
            {
                var user = Membership.GetUser();

                if (user != null && !_unitOfWork.CommissionRepository.RelatesToUser(id, user.UserName))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }
            }

            return PartialView("CommissionAttachments", _unitOfWork.AttachmentRepository
                .GetByCommissionAndType(id, type)
                .Select(a => _mapper.AttachmentModelMapper.Map(a)));
        }

        [HttpGet]
        [Authorize]
        public ActionResult Attachment(int id)
        {
            if (User.IsInRole("Assistant"))
            {
                var user = Membership.GetUser();

                if (user != null && !_unitOfWork.AttachmentRepository.RelatesToUser(id, user.UserName))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }
            }

            var attachment = _unitOfWork.AttachmentRepository.GetByKey(id);

            if (attachment == null)
                return Content(Global.Error_AttachmentRemoved);

            return new AttachmentResult(attachment, _unitOfWork.AttachmentRepository.GetAttachmentData);
        }

        [HttpPost]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        public ActionResult AttachmentAdd(int id, Attachment.Type type)
        {
            foreach (var file in Request.Files.Cast<string>().Select(fileName => Request.Files[fileName])
                )
            {
                if (file.ContentLength > 1024*1024*100)
                    continue;

                var attachment = new Attachment
                {
                    CommissionId = id,
                    AttachmentType = type,
                    Name = file.FileName,
                    ContentType = file.ContentType,
                    Data = file.InputStream
                };

                _unitOfWork.AttachmentRepository.Insert(attachment);

                try
                {
                    _unitOfWork.Commit();
                }
                catch (RepositoryException ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                    return new EmptyResult();
                }
            }

            return new EmptyResult();
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult ResponsibleNames()
        {
            var responsiblesList = _unitOfWork.ResponsibleRepository.GetAll()
                .Select(x => new
                {
                    Text = x.Name,
                    Value = x.ResponsibleId.ToString()
                });

            return Json(new
            {
                List = responsiblesList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult AssistResponsibleNames()
        {
            var user = Membership.GetUser();
            var responsiblesList = _unitOfWork.ResponsibleRepository
                .GetByUserLogin(user != null ? user.UserName : null)
                .Select(x => new
                {
                    Text = x.Name,
                    Value = x.ResponsibleId.ToString()
                });

            return Json(new
            {
                List = responsiblesList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult CommissionStatuses()
        {
            var statusList =
                Enum.GetValues(typeof (CommissionStatusModel.Status))
                    .Cast<CommissionStatusModel.Status>()
                    .Select(s => new
                    {
                        Text =
                            typeof (CommissionStatusModel.Status).GetField(s.ToString())
                                .GetCustomAttributes(typeof (DropDownNameAttribute), false)
                                .OfType<DropDownNameAttribute>()
                                .First()
                                .Name,
                        Value = s
                    });

            return Json(new
            {
                List = statusList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize]
        public ActionResult AdminProperties()
        {
            return Json(CommissionViewModel.AdminProperties, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Delivery()
        {
            return View();
        }

        [HttpGet]
        [AjaxOnly]
        [AjaxAuthorize(Roles = "Administrator")]
        public ActionResult DeliverDelayReport()
        {
            try
            {
                _mailService.DeliverDelayReport();

                return Content("Письма отправлены");
            }
            catch
            {
                return Content("Ошибка отправки");
            }
        }

        private CommissionFilter AssistCommissionFilter(CommissionFilter filterModel)
        {
            if (filterModel == null)
            {
                filterModel = new CommissionFilter();
            }

            var user = Membership.GetUser();
            if (filterModel.Responsibles == null || !filterModel.Responsibles.Any())
            {
                filterModel.Responsibles = _unitOfWork.ResponsibleRepository
                    .GetByUserLogin(user?.UserName)
                    .Select(r => r.ResponsibleId);
            }
            else
            {
                filterModel.Responsibles = filterModel.Responsibles
                    .Intersect(_unitOfWork.ResponsibleRepository
                        .GetByUserLogin(user?.UserName)
                        .Select(r => r.ResponsibleId));
            }
            
            return filterModel;
        }
    }
}