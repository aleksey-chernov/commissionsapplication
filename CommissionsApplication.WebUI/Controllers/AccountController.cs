﻿using System.Web.Mvc;
using System.Web.Security;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Management");
            }

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Login, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.Login, model.IsPersistent);

                    return string.IsNullOrEmpty(returnUrl)
                        ? Redirect(Url.Action("Login"))
                        : Redirect(returnUrl);
                }

                ModelState.AddModelError("InvalidCredentials", Validation.Login_InvalidCredentials);
            }

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        public ActionResult Logout(string returnUrl)
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", new {ReturnUrl = returnUrl});
        }
    }
}