﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace CommissionsApplication.WebUI.Binders
{
    public class DecimalModelBinder : IModelBinder
    {
        public virtual object BindModel(ControllerContext controllerContext,
            ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider
                .GetValue(bindingContext.ModelName);
            var modelState = new ModelState { Value = valueResult };
            
            object actualValue = null;
            try
            {
                var isNullableAndNull = (bindingContext.ModelMetadata.IsNullableValueType &&
                                         string.IsNullOrWhiteSpace(valueResult.AttemptedValue));

                if (!isNullableAndNull)
                {
                    actualValue = Convert.ToDecimal(valueResult.AttemptedValue, valueResult.AttemptedValue.Contains(".")
                        ? CultureInfo.InvariantCulture
                        : CultureInfo.CurrentCulture);
                }
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }
}