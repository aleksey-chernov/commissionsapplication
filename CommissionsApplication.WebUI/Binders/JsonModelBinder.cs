﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CommissionsApplication.WebUI.Binders
{
    public class JsonModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var json = controllerContext.HttpContext.Request.Params[bindingContext.ModelName];            

            if (string.IsNullOrEmpty(json))
                return null;

            try
            {
                return new JavaScriptSerializer().Deserialize(json, bindingContext.ModelType);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}