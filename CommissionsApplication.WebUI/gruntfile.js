/// <vs />
module.exports = function(grunt) {
  grunt.initConfig({
    bower_concat: {
      build: {
        options: {
          separator: ";"
        },
        dest: "Scripts/libs/libs.js",
        cssDest: "Content/style.css",
        mainFiles: {
          "jquery-validation": ["jquery.validate.js"],
          "jquery-validation-unobtrusive": ["jquery.validate.unobtrusive.js"],
          "jquery-ajax-unobtrusive": ["jquery.unobtrusive-ajax.js"],
          "jQuery-File-Upload": ["js/vendor/jquery.ui.widget.js", "js/jquery.iframe-transport.js", "js/jquery.fileupload.js", "css/jquery.fileupload.css"],
          "bootstrap-datepicker": ["dist/js/bootstrap-datepicker.js", "dist/locales/bootstrap-datepicker.ru.min.js", "dist/css/bootstrap-datepicker.css"],
          "bootstrap-daterangepicker": ["daterangepicker.js", "daterangepicker-bs2.css"],
          "bootstrap-grid": [
            "js/bootstrap-grid.js", "js/bootstrap-grid-filter.js", "js/bootstrap-grid-contextmenu.js",
            "css/bootstrap-grid.css", "css/bootstrap-grid-filter.css", "css/bootstrap-grid-contextmenu.css"
          ],
          "editable-select": ["js/editable-select.js", "css/editable-select.css"],
          "globalize": ["lib/globalize.js", "lib/cultures/globalize.culture.ru-RU.js"]
        }
      }
    },
    uglify: {
      build: {
        files: {
          "Scripts/libs/libs.min.js": ["Scripts/libs/libs.js"]
        }
      }
    },
    cssmin: {
      build: {
        files: {
          "Content/style.min.css": ["Content/style.css"]
        }
      }
    },
    copy: {
      build: {
        expand: true,
        src: "bower_components/bootstrap/img/*.png",
        dest: "img/"
      }
    }
  });

  grunt.loadNpmTasks("grunt-bower-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-copy");

  grunt.registerTask("default", ["bower_concat", "uglify", "cssmin", "copy"]);
};