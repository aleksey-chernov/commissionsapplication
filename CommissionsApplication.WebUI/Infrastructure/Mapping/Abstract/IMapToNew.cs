﻿namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract
{
    public interface IMapToNew<in TSource, out TTarget>
    {
        TTarget Map(TSource source);
    }
}