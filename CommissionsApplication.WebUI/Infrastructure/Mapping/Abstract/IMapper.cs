﻿using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Models.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract
{
    public interface IMapper
    {
        IMapToNew<Attachment, AttachmentModel> AttachmentModelMapper { get; }

        IMapToNew<AttachmentModel, Attachment> AttachmentMapper { get; }

        IMapToNew<Commission, CommissionViewModel> CommissionViewModelMapper { get; }

        IMapToNew<CommissionViewModel, CommissionAssistViewModel> CommissionAssistViewModelMapper { get; }

        IMapToNew<Commission, CommissionEditModel> CommissionEditModelMapper { get; }

        IMapToNew<CommissionEditModel, Commission> CommissionMapper { get; }

        IMapToNew<User, UserViewModel> UserViewModelMapper { get; }

        IMapToNew<User, UserEditModel> UserEditModelMapper { get; }

        IMapToNew<IUserModel, User> UserMapper { get; }

        IMapToNew<Responsible, ResponsibleViewModel> ResponsibleViewModelMapper { get; }

        IMapToNew<Responsible, ResponsibleEditModel> ResponsibleEditModelMapper { get; }

        IMapToNew<ResponsibleEditModel, Responsible> ResponsibleMapper { get; }
    }
}