﻿using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Abstract;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class UserMapper : IMapToNew<IUserModel, User>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserMapper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public User Map(IUserModel source)
        {
            var target = new User
            {
                UserId = source.UserId,
                RowVersion = source.RowVersion,
                Name = source.Name,
                Login = source.Login,
                Email = source.Email,
                Password = !string.IsNullOrEmpty(source.Password)
                    ? PasswordHash.GetMD5Hash(source.Password)
                    : string.Empty,
                UserRole = (User.Role) source.UserRole
            };

            foreach (var responsible in _unitOfWork.ResponsibleRepository
                .GetByKeys(source.SelectedResponsibleIds))
            {
                target.Responsibles.Add(responsible);
            }

            return target;
        }
    }
}