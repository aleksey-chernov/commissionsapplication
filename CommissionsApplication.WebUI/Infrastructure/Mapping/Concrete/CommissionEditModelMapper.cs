﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommissionEditModelMapper : IMapToNew<Commission, CommissionEditModel>
    {
        private readonly IUnitOfWork _unitOfWork; 
        private readonly IMapper _mapper;
        
        public CommissionEditModelMapper(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public CommissionEditModel Map(Commission source)
        {
            var incomeAttachments = new List<AttachmentModel>();
            var outcomeAttachments = new List<AttachmentModel>();
            foreach (var attachment in source.Attachments)
            {
                switch (attachment.AttachmentType)
                {
                    case Attachment.Type.Income:
                        incomeAttachments.Add(_mapper.AttachmentModelMapper.Map(attachment));
                        break;
                    case Attachment.Type.Outcome:
                        outcomeAttachments.Add(_mapper.AttachmentModelMapper.Map(attachment));
                        break;
                }
            }

            var controlDateModels = source.ControlDates
                .Select(x => new ControlDateModel
                {
                    ControlDateId = x.ControlDateId,
                    Date = x.Date.ToShortDateString()
                })
                .ToList();

            var chargeModels = source.Charges
                .Select(x => new ChargeModel
                {
                    ChargeId = x.ChargeId,
                    ResponsibleId = x.ResponsibleId,
                    ResponsibleName = x.Responsible.Name,
                    ExecutionDate = x.ExecutionDate
                })
                .ToList();

            var responsiblesList = _unitOfWork.ResponsibleRepository.GetAll()
                .Select(r => new SelectListItem
                {
                    Text = r.Name,
                    Value = r.ResponsibleId.ToString()
                });

            return new CommissionEditModel
            {
                CommissionId = source.CommissionId,
                RowVersion = source.RowVersion,
                OutcomeDocument = source.OutcomeDocument,
                IncomeDocument = source.IncomeDocument,
                IncomeDate = source.IncomeDate,
                Summary = source.Summary,
                Organization = source.Organization,
                Signer = source.Signer,
                Resolution = source.Resolution,
                Commentary = source.Commentary,
                ExecutionMark = source.ExecutionMark,
                ExecutionDate = source.ExecutionDate,
                RegNumber = source.RegNumber,
                CommissionNumber = source.CommissionNumber,
                ImportanceFactor = source.ImportanceFactor,
                DelayFactor = source.DelayFactor,
                DelayInfo = source.DelayInfo,
                Reason = source.Reason,
                ControlDates = controlDateModels,
                AttachmentsIncome = incomeAttachments,
                AttachmentsOutcome = outcomeAttachments,
                ChargeModels = chargeModels,
                ResponsiblesList = responsiblesList,
                CommissionStatus = (CommissionStatusModel.Status) source.CommissionStatus
            };
        }
    }
}