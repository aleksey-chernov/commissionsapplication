﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommissionEditMapper : IMapToNew<Commission, CommissionEditModel>
    {
        private readonly IMapToNew<Attachment, AttachmentModel> _attachmentModelMapper;
        private readonly IUnitOfWork _unitOfWork;

        public CommissionEditMapper(IUnitOfWork unitOfWork, IMapToNew<Attachment, AttachmentModel> attachmentModelMapper)
        {
            _unitOfWork = unitOfWork;
            _attachmentModelMapper = attachmentModelMapper;
        }

        public CommissionEditModel Map(Commission source)
        {
            var selectedControlDates = source.ControlDates.Select(s => s.Date.ToShortDateString()).ToList();
            var controlDatesList = selectedControlDates.Select(d => new SelectListItem
            {
                Value = d,
                Text = d
            });

            var incomeAttachments = new List<AttachmentModel>();
            var outcomeAttachments = new List<AttachmentModel>();
            foreach (var attachment in source.Attachments)
            {
                switch (attachment.AttachmentType)
                {
                    case Attachment.Type.Income:
                        incomeAttachments.Add(_attachmentModelMapper.Map(attachment));
                        break;
                    case Attachment.Type.Outcome:
                        outcomeAttachments.Add(_attachmentModelMapper.Map(attachment));
                        break;
                }
            }

            var chargeModels = source.Charges
                .Select(x => new ChargeModel
                {
                    ChargeId = x.ChargeId,
                    ResponsibleId = x.ResponsibleId,
                    ResponsibleName = x.Responsible.Name,
                    ExecutionDate = x.ExecutionDate,
                    IsSelected = true
                })
                .Concat(_unitOfWork.ResponsibleRepository.GetAll().Select(x => new ChargeModel
                {
                    ResponsibleId = x.ResponsibleId,
                    ResponsibleName = x.Name
                }))
                .GroupBy(c => c.ResponsibleId)
                .Select(group => group.Aggregate((charge, resp) => charge))
                .OrderBy(ch => ch.ResponsibleId)
                .ToList();

            return new CommissionEditModel
            {
                CommissionId = source.CommissionId,
                RowVersion = source.RowVersion,
                OutcomeDocument = source.OutcomeDocument,
                IncomeDocument = source.IncomeDocument,
                IncomeDate = source.IncomeDate,
                Summary = source.Summary,
                Organization = source.Organization,
                Signer = source.Signer,
                Resolution = source.Resolution,
                Commentary = source.Commentary,
                ExecutionMark = source.ExecutionMark,
                ExecutionDate = source.ExecutionDate,
                RegNumber = source.RegNumber,
                CommissionNumber = source.CommissionNumber,
                ImportanceFactor = source.ImportanceFactor,
                DelayFactor = source.DelayFactor,
                DelayInfo = source.DelayInfo,
                Reason = source.Reason,
                SelectedControlDates = selectedControlDates,
                ControlDatesList = controlDatesList,
                AttachmentsIncome = incomeAttachments,
                AttachmentsOutcome = outcomeAttachments,
                ChargeModels = chargeModels
            };
        }
    }
}