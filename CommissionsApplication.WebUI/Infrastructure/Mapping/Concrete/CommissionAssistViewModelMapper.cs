﻿using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommissionAssistViewModelMapper : IMapToNew<CommissionViewModel, CommissionAssistViewModel>
    {
        public CommissionAssistViewModel Map(CommissionViewModel source)
        {
            return new CommissionAssistViewModel
            {
                CommissionId = source.CommissionId,
                CommissionRowNumber = source.CommissionRowNumber,
                ControlDates = source.ControlDates,
                CommissionStatus = source.CommissionStatus,
                CommissionStatusIndex = source.CommissionStatusIndex,
                OutcomeDocument = source.OutcomeDocument,
                IncomeDate = source.IncomeDate,
                IncomeDocument = source.IncomeDocument,
                Summary = source.Summary,
                Organization = source.Organization,
                Signer = source.Signer,
                Resolution = source.Resolution,
                Responsibles = source.Responsibles,
                Commentary = source.Commentary,
                ExecutionMark = source.ExecutionMark,
                RegNumber = source.RegNumber
            };
        }
    }
}