﻿using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class ResponsibleMapper : IMapToNew<ResponsibleEditModel, Responsible>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ResponsibleMapper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Responsible Map(ResponsibleEditModel source)
        {
            var target = new Responsible
            {
                ResponsibleId = source.ResponsibleId,
                RowVersion = source.RowVersion,
                Name = source.Name,
                Email = source.Email
            };

            foreach (var user in _unitOfWork.UserRepository
                .GetByKeys(source.SelectedUserIds))
            {
                target.Users.Add(user);
            }

            return target;
        }
    }
}