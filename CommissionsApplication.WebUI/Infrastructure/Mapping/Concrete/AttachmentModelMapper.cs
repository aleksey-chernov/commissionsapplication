﻿using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class AttachmentModelMapper : IMapToNew<Attachment, AttachmentModel>
    {
        public AttachmentModel Map(Attachment source)
        {
            return new AttachmentModel
            {
                AttachmentId = source.AttachmentId,
                CommissionId = source.CommissionId,
                AttachmentType = source.AttachmentType,
                ContentType = source.ContentType,
                Name = source.Name
            };
        }
    }
}