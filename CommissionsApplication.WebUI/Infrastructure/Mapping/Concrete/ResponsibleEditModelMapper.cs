﻿using System.Linq;
using System.Web.Mvc;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class ResponsibleEditModelMapper : IMapToNew<Responsible, ResponsibleEditModel>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ResponsibleEditModelMapper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ResponsibleEditModel Map(Responsible source)
        {
            var userList = _unitOfWork.UserRepository.GetAll()
                .Select(u => new SelectListItem
                {
                    Text = u.Name,
                    Value = u.UserId.ToString()
                });

            return new ResponsibleEditModel
            {
                ResponsibleId = source.ResponsibleId,
                RowVersion = source.RowVersion,
                Name = source.Name,
                Email = source.Email,
                SelectedUserIds = source.Users.Select(u => u.UserId).ToList(),
                UsersList = userList
            };
        }
    }
}