﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.App_GlobalResources;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommissionViewModelMapper : IMapToNew<Commission, CommissionViewModel>
    {
        private static readonly Dictionary<CommissionStatusModel.Status, string> StatusLookup =
            Enum.GetValues(typeof (CommissionStatusModel.Status))
                .Cast<CommissionStatusModel.Status>()
                .ToDictionary(s => s,
                    s => typeof (CommissionStatusModel.Status).GetField(s.ToString())
                        .GetCustomAttributes(typeof (DropDownNameAttribute), false)
                        .OfType<DropDownNameAttribute>()
                        .First()
                        .Name);

        private static readonly List<string> DelayFactorsLookup = new List<string>
        {
            0.02M.ToString(CultureInfo.CurrentCulture),
            0.04M.ToString(CultureInfo.CurrentCulture),
            0.06M.ToString(CultureInfo.CurrentCulture)
        };

        public CommissionViewModel Map(Commission source)
        {
            return new CommissionViewModel
            {
                CommissionId = source.CommissionId,
                CommissionRowNumber = source.CommissionRowNumber,
                ControlDates = string.Join(Environment.NewLine, source.ControlDates
                    .OrderByDescending(x => x.Date)
                    .Select(x => x.Date.ToShortDateString())),
                CommissionStatus = StatusLookup[(CommissionStatusModel.Status) source.CommissionStatus],
                CommissionStatusIndex = (int) source.CommissionStatus,
                OutcomeDocument = source.OutcomeDocument ?? string.Empty,
                IncomeDate = source.IncomeDate.ToShortDateString(),
                IncomeDocument = source.IncomeDocument ?? string.Empty,
                Summary = source.Summary ?? string.Empty,
                Organization = source.Organization ?? string.Empty,
                Signer = source.Signer ?? string.Empty,
                Resolution = source.Resolution ?? string.Empty,
                Responsibles = string.Join(Environment.NewLine, source.Charges.Select(x => x.Responsible.Name)),
                Commentary = source.Commentary ?? string.Empty,
                ExecutionMark = source.ExecutionMark ?? string.Empty,
                ExecutionDate = source.ExecutionDate.HasValue ? source.ExecutionDate.Value.ToShortDateString() : string.Empty,
                RegNumber = source.RegNumber ?? string.Empty,
                CommissionNumber = source.CommissionNumber ?? string.Empty,
                ImportanceFactor = source.ImportanceFactor,
                ImportanceFactorString = ConvertImportanceFactor(source.ImportanceFactor),
                DelayFactor = source.DelayFactor.HasValue 
                ? source.DelayFactor.Value.ToString(CultureInfo.CurrentCulture) 
                : string.Empty,
                DelayFactorString = source.DelayFactor.HasValue 
                ? ConvertDelayFactor(source.DelayFactor.Value.ToString(CultureInfo.CurrentCulture)) 
                : string.Empty,
                DelayInfo = source.DelayInfo ?? string.Empty,
                Reason = source.Reason ?? string.Empty
            };
        }

        private static string ConvertImportanceFactor(int factor)
        {
            switch (factor)
            {
                case 1:
                    return Global.ImportanceFactor_1;
                case 2:
                    return Global.ImportanceFactor_2;
                case 3:
                    return Global.ImportanceFactor_3;
                default:
                    return factor.ToString();
            }
        }

        private static string ConvertDelayFactor(string factor)
        {
            if (factor == DelayFactorsLookup[0])
                return Global.DelayFactor_1;

            if (factor == DelayFactorsLookup[1])
                return Global.DelayFactor_2;

            if (factor == DelayFactorsLookup[2])
                return Global.DelayFactor_3;

            return Global.DelayFactor_4;
        }
    }
}