﻿using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CustomMapper : IMapper
    {
        private readonly IUnitOfWork _unitOfWork;
        
        private IMapToNew<Attachment, AttachmentModel> _attachmentModelMapper;
        private IMapToNew<AttachmentModel, Attachment> _attachmentMapper;
        private IMapToNew<Commission, CommissionViewModel> _commissionViewModelMapper;
        private IMapToNew<CommissionViewModel, CommissionAssistViewModel> _commissionAssistViewModelMapper;
        private IMapToNew<Commission, CommissionEditModel> _commissionEditModelMapper;
        private IMapToNew<CommissionEditModel, Commission> _commissionMapper;
        private IMapToNew<User, UserViewModel> _userViewModelMapper;
        private IMapToNew<User, UserEditModel> _userEditModelMapper;
        private IMapToNew<IUserModel, User> _userMapper;
        private IMapToNew<Responsible, ResponsibleViewModel> _responsibleViewModelMapper;
        private IMapToNew<Responsible, ResponsibleEditModel> _responsibleEditModelMapper;
        private IMapToNew<ResponsibleEditModel, Responsible> _responsibleMapper;

        public CustomMapper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMapToNew<Attachment, AttachmentModel> AttachmentModelMapper
        {
            get { return _attachmentModelMapper ?? (_attachmentModelMapper = new AttachmentModelMapper()); }
        }

        public IMapToNew<AttachmentModel, Attachment> AttachmentMapper
        {
            get { return _attachmentMapper ?? (_attachmentMapper = new AttachmentMapper()); }
        }

        public IMapToNew<Commission, CommissionViewModel> CommissionViewModelMapper
        {
            get { return _commissionViewModelMapper ?? (_commissionViewModelMapper = new CommissionViewModelMapper()); }
        }

        public IMapToNew<CommissionViewModel, CommissionAssistViewModel> CommissionAssistViewModelMapper
        {
            get { return _commissionAssistViewModelMapper ?? (_commissionAssistViewModelMapper = new CommissionAssistViewModelMapper()); }
        }

        public IMapToNew<Commission, CommissionEditModel> CommissionEditModelMapper
        {
            get { return _commissionEditModelMapper ?? (_commissionEditModelMapper = new CommissionEditModelMapper(_unitOfWork, this)); }
        }

        public IMapToNew<CommissionEditModel, Commission> CommissionMapper
        {
            get { return _commissionMapper ?? (_commissionMapper = new CommissionMapper(_unitOfWork, this)); }
        }

        public IMapToNew<User, UserViewModel> UserViewModelMapper
        {
            get { return _userViewModelMapper ?? (_userViewModelMapper = new UserViewModelMapper()); }
        }

        public IMapToNew<User, UserEditModel> UserEditModelMapper
        {
            get { return _userEditModelMapper ?? (_userEditModelMapper = new UserEditModelMapper(_unitOfWork)); }
        }

        public IMapToNew<IUserModel, User> UserMapper
        {
            get { return _userMapper ?? (_userMapper = new UserMapper(_unitOfWork)); }
        }

        public IMapToNew<Responsible, ResponsibleViewModel> ResponsibleViewModelMapper
        {
            get { return _responsibleViewModelMapper ?? (_responsibleViewModelMapper = new ResponsibleViewModelMapper()); }
        }

        public IMapToNew<Responsible, ResponsibleEditModel> ResponsibleEditModelMapper
        {
            get { return _responsibleEditModelMapper ?? (_responsibleEditModelMapper = new ResponsibleEditModelMapper(_unitOfWork)); }
        }

        public IMapToNew<ResponsibleEditModel, Responsible> ResponsibleMapper
        {
            get { return _responsibleMapper ?? (_responsibleMapper = new ResponsibleMapper(_unitOfWork)); }
        }
    }
}