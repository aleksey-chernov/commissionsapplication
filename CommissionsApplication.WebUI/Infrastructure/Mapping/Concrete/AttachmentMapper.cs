﻿using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class AttachmentMapper : IMapToNew<AttachmentModel, Attachment>
    {
        public Attachment Map(AttachmentModel source)
        {
            return new Attachment
            {
                AttachmentId = source.AttachmentId,
                CommissionId = source.CommissionId,
                AttachmentType = source.AttachmentType,
                ContentType = source.ContentType,
                Name = source.Name
            };
        }
    }
}