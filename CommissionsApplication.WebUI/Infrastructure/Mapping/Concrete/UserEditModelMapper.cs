﻿using System.Linq;
using System.Web.Mvc;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class UserEditModelMapper : IMapToNew<User, UserEditModel>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserEditModelMapper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public UserEditModel Map(User source)
        {
            var responsiblesList = _unitOfWork.ResponsibleRepository.GetAll()
                .Select(r => new SelectListItem
                {
                    Text = r.Name,
                    Value = r.ResponsibleId.ToString()
                });

            return new UserEditModel
            {
                UserId = source.UserId,
                RowVersion = source.RowVersion,
                Name = source.Name,
                Login = source.Login,
                Email = source.Email,
                UserRole = (UserRoleModel.Role) source.UserRole,
                SelectedResponsibleIds = source.Responsibles.Select(r => r.ResponsibleId).ToList(),
                ResponsiblesList = responsiblesList
            };
        }
    }
}