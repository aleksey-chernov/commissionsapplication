﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Models.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class AutoMapperWebUIConfiguration
    {
        public static void Configure()
        {
            ConfigureCommissionMapping();
            ConfigureAttachmentMapping();
            ConfigureUserMapping();
            ConfigureResponsibleMapping();
        }

        private static void ConfigureCommissionMapping()
        {
            //Mapper.CreateMap<Commission, CommissionViewModel>()
            //    .ForMember(dest => dest.IncomeDocument, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.CommissionStatusIndex, opt =>
            //        opt.MapFrom(src => src.CommissionStatus))
            //    .ForMember(dest => dest.CommissionStatus, opt =>
            //        opt.MapFrom(src =>
            //            ((DropDownNameAttribute[])
            //                typeof (CommissionStatusModel.Status).GetField(src.CommissionStatus.ToString())
            //                    .GetCustomAttributes(typeof(DropDownNameAttribute), false))[0].Name
            //            ))
            //    .ForMember(dest => dest.OutcomeDocument, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Summary, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Organization, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Signer, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Resolution, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Commentary, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.ExecutionMark, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.RegNumber, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.CommissionNumber, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Reason, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.DelayFactor, opt =>
            //        opt.MapFrom(src => src.DelayFactor.ToString(CultureInfo.CurrentCulture)))
            //    .ForMember(dest => dest.DelayInfo, opt =>
            //        opt.NullSubstitute(string.Empty))
            //    .ForMember(dest => dest.Responsibles, opt =>
            //        opt.MapFrom(src => string.Join(Environment.NewLine, src.Charges.Select(x => x.Responsible.Name))))
            //    .ForMember(dest => dest.Commentary, opt =>
            //        opt.MapFrom(
            //            src => src.Commentary.Length > 100 ? src.Commentary.Substring(0, 100) + " ..." : src.Commentary))
            //    .ForMember(dest => dest.IncomeDate, opt =>
            //        opt.MapFrom(src => src.IncomeDate.ToShortDateString()))
            //    .ForMember(dest => dest.ExecutionDate, opt =>
            //        opt.MapFrom(src =>
            //            src.ExecutionDate.HasValue ? src.ExecutionDate.Value.ToShortDateString() : string.Empty
            //            ))
            //    .ForMember(dest => dest.ControlDates, opt =>
            //        opt.MapFrom(src => string.Join(Environment.NewLine, src.ControlDates
            //            .OrderBy(x => x.Date)
            //            .Select(x => x.Date.ToShortDateString()))));

            //Mapper.CreateMap<Commission, CommissionEditModel>()
            //    .ForMember(dest => dest.ChargeModels, opt =>
            //        opt.MapFrom(src => src.Charges.Select(x => new ChargeModel
            //        {
            //            ChargeId = x.ChargeId,
            //            ResponsibleId = x.ResponsibleId,
            //            ResponsibleName = x.Responsible.Name,
            //            ExecutionDate = x.ExecutionDate,
            //            IsSelected = true
            //        })))
            //    .ForMember(dest => dest.SelectedControlDates, opt =>
            //        opt.MapFrom(src => src.ControlDates.Select(x => x.Date.ToShortDateString())))
            //    .ForMember(dest => dest.AttachmentsIncome, opt =>
            //        opt.MapFrom(
            //            src => Mapper.Map(src.Attachments.Where(a => a.AttachmentType == Attachment.Type.Income),
            //                typeof (IEnumerable<Attachment>),
            //                typeof (IEnumerable<AttachmentModel>))))
            //    .ForMember(dest => dest.AttachmentsOutcome, opt =>
            //        opt.MapFrom(
            //            src => Mapper.Map(src.Attachments.Where(a => a.AttachmentType == Attachment.Type.Outcome),
            //                typeof (IEnumerable<Attachment>),
            //                typeof (IEnumerable<AttachmentModel>))));

            //Mapper.CreateMap<CommissionEditModel, Commission>()
            //    .ForMember(dest => dest.ControlDates, opt =>
            //        opt.MapFrom(src => src.SelectedControlDates.Select(x => new ControlDate
            //        {
            //            Date = DateTime.Parse(x)
            //        })))
            //    .ForMember(dest => dest.Attachments, opt =>
            //        opt.MapFrom(src => Mapper.Map(src.SelectedAttachmentModels,
            //            typeof (IEnumerable<AttachmentModel>),
            //            typeof (IEnumerable<Attachment>))));

            Mapper.CreateMap<CommissionFilterModel, CommissionFilter>()
                .ForMember(dest => dest.ImportanceFactor, opt =>
                    opt.ResolveUsing(src =>
                    {
                        int value;
                        return int.TryParse(src.ImportanceFactor, out value) ? (int?) value : null;
                    }))
                .ForMember(dest => dest.DelayFactor, opt =>
                    opt.ResolveUsing(src =>
                    {
                        if (string.IsNullOrWhiteSpace(src.DelayFactor))
                            return null;

                        decimal value;
                        return decimal.TryParse(src.DelayFactor.Replace(",", "."), NumberStyles.Any,
                            CultureInfo.InvariantCulture, out value)
                            ? (decimal?) value
                            : null;
                    }));
        }

        private static void ConfigureAttachmentMapping()
        {
            //Mapper.CreateMap<Attachment, AttachmentModel>();
            //Mapper.CreateMap<AttachmentModel, Attachment>();
        }

        private static void ConfigureUserMapping()
        {
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.UserRole, opt =>
                    opt.MapFrom(src =>
                        ((DropDownNameAttribute[])
                            typeof (UserRoleModel.Role).GetField(src.UserRole.ToString())
                                .GetCustomAttributes(typeof(DropDownNameAttribute), false))[0].Name
                        ));

            Mapper.CreateMap<User, UserEditModel>()
                .ForMember(dest => dest.SelectedResponsibleIds, opt =>
                    opt.MapFrom(src => src.Responsibles.Select(x => x.ResponsibleId)))
                .ForMember(dest => dest.Password, opt => opt.Ignore());

            Mapper.CreateMap<IUserModel, User>()
                .ForMember(dest => dest.Password, opt =>
                    opt.MapFrom(
                        src =>
                            !string.IsNullOrEmpty(src.Password) ? PasswordHash.GetMD5Hash(src.Password) : string.Empty));
        }

        private static void ConfigureResponsibleMapping()
        {
            Mapper.CreateMap<Responsible, ResponsibleViewModel>();

            Mapper.CreateMap<Responsible, ResponsibleEditModel>()
                .ForMember(dest => dest.SelectedUserIds, opt =>
                    opt.MapFrom(src => src.Users.Select(x => x.UserId)));

            Mapper.CreateMap<ResponsibleEditModel, Responsible>();
        }
    }
}