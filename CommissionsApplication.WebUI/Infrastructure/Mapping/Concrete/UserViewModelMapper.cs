﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class UserViewModelMapper : IMapToNew<User, UserViewModel>
    {
        private static readonly Dictionary<UserRoleModel.Role, string> RoleLookup =
            Enum.GetValues(typeof(UserRoleModel.Role))
                .Cast<UserRoleModel.Role>()
                .ToDictionary(r => r,
                    r => typeof(UserRoleModel.Role).GetField(r.ToString())
                        .GetCustomAttributes(typeof(DropDownNameAttribute), false)
                        .OfType<DropDownNameAttribute>()
                        .First()
                        .Name);

        public UserViewModel Map(User source)
        {
            return new UserViewModel
            {
                UserId = source.UserId,
                Name = source.Name,
                Login = source.Login,
                Email = source.Email,
                UserRole = RoleLookup[(UserRoleModel.Role) source.UserRole]
            };
        }
    }
}