﻿using System;
using System.Linq;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommissionMapper : IMapToNew<CommissionEditModel, Commission>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommissionMapper(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Commission Map(CommissionEditModel source)
        {
            var target = new Commission
            {
                CommissionId = source.CommissionId,
                RowVersion = source.RowVersion,
                OutcomeDocument = source.OutcomeDocument,
                IncomeDocument = source.IncomeDocument,
                IncomeDate = source.IncomeDate,
                Summary = source.Summary,
                Organization = source.Organization,
                Signer = source.Signer,
                Resolution = source.Resolution,
                Commentary = source.Commentary,
                ExecutionMark = source.ExecutionMark,
                ExecutionDate = source.ExecutionDate,
                RegNumber = source.RegNumber,
                CommissionNumber = source.CommissionNumber,
                ImportanceFactor = source.ImportanceFactor,
                DelayFactor = source.DelayFactor,
                DelayInfo = source.DelayInfo,
                Reason = source.Reason,
                CommissionStatus = (Commission.Status) source.CommissionStatus
            };

            foreach (var date in source.ControlDates)
            {
                target.ControlDates.Add(new ControlDate
                {
                    ControlDateId = date.ControlDateId,
                    CommissionId = target.CommissionId,
                    Date = DateTime.Parse(date.Date)
                });
            }

            foreach (
                var attachment in source.AttachmentsIncome.Concat(source.AttachmentsOutcome).Where(a => !a.IsDeleted))
            {
                target.Attachments.Add(_mapper.AttachmentMapper.Map(attachment));
            }

            var chargeModels = source.ChargeModels;
            var responsiblesDict = _unitOfWork.ResponsibleRepository.GetByKeys(chargeModels.Select(ch => ch.ResponsibleId))
                .ToDictionary(r => r.ResponsibleId, r => r);
            foreach (var ch in chargeModels)
            {
                Responsible responsible;
                if(!responsiblesDict.TryGetValue(ch.ResponsibleId, out responsible))
                    continue;

                target.Charges.Add(new Charge
                {
                    ChargeId = ch.ChargeId,
                    CommissionId = target.CommissionId,
                    Commission = target,
                    Responsible = responsible,
                    ExecutionDate = ch.ExecutionDate
                });
            }

            return target;
        }
    }
}