﻿using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class ResponsibleViewModelMapper : IMapToNew<Responsible, ResponsibleViewModel>
    {
        public ResponsibleViewModel Map(Responsible source)
        {
            return new ResponsibleViewModel
            {
                ResponsibleId = source.ResponsibleId,
                Name = source.Name,
                Email = source.Email
            };
        }
    }
}