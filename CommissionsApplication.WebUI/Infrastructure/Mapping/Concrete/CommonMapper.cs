﻿using System;
using AutoMapper;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;

namespace CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete
{
    public class CommonMapper : IMapper
    {
        static CommonMapper()
        {
            AutoMapperWebUIConfiguration.Configure();
        }

        public object Map(object source, Type sourceType, Type destinationType)
        {
            return Mapper.Map(source, sourceType, destinationType);
        }

        public void DynamicMap(object source, object dest, Type sourceType, Type destinationType)
        {
            Mapper.DynamicMap(source, dest, sourceType, destinationType);
        }
    }
}