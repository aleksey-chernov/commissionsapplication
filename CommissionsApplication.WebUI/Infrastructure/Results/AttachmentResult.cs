﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.WebUI.Infrastructure.Results
{
    public class AttachmentResult : FileResult
    {
        private readonly int _attachmentId;
        private readonly Action<int, Stream> _fetchAttachment;

        public AttachmentResult(Attachment attachment, Action<int, Stream> fetchAttachment)
            : base(attachment.ContentType)
        {
            _attachmentId = attachment.AttachmentId;
            _fetchAttachment = fetchAttachment;

            FileDownloadName = attachment.Name;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            _fetchAttachment(_attachmentId, response.OutputStream);
        }
    }
}