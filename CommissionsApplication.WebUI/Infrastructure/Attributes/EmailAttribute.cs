﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CommissionsApplication.WebUI.Infrastructure.Attributes
{
    public class EmailAttribute : RegularExpressionAttribute
    {
        static EmailAttribute()
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof (EmailAttribute),
                typeof (RegularExpressionAttributeAdapter));
        }

        public EmailAttribute()
            : base(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                   + "@"
                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$")
        {
        }
    }
}