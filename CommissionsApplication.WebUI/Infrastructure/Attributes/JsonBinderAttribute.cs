﻿using System.Web.Mvc;
using CommissionsApplication.WebUI.Binders;

namespace CommissionsApplication.WebUI.Infrastructure.Attributes
{
    public class JsonBinderAttribute : CustomModelBinderAttribute
    {
        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder();
        }
    }
}