﻿using System;

namespace CommissionsApplication.WebUI.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class DropDownNameAttribute : ResourceAttribute
    {
        public DropDownNameAttribute(string name)
        {
            Name = name;
        }

        public DropDownNameAttribute(Type resourceType, string resourceName)
        {
            Name = GetResourceLookup(resourceType, resourceName);
        }

        public string Name { get; private set; }
    }
}