﻿using System.Collections.Generic;
using System.Web.Profile;
using System.Web.Security;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.WebUI.Infrastructure.Auth.Concrete
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }

        public static UserProfile GetUserProfile()
        {
            var user = Membership.GetUser();

            return user != null
                ? Create(user.UserName) as UserProfile
                : null;
        }

        [SettingsAllowAnonymous(false)]
        public string Name
        {
            get { return base["Name"] as string; }
        }
    }
}