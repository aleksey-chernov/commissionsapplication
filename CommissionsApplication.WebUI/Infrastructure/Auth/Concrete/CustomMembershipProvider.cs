﻿using System;
using System.Collections.Specialized;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Auth.Abstract;

namespace CommissionsApplication.WebUI.Infrastructure.Auth.Concrete
{
    public class CustomMembershipProvider : MembershipProvider
    {
        #region Fields

        private string _applicationName;
        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;
        private int _maxInvalidPasswordAttempts;
        private int _passwordAttemptWindow;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;

        #endregion

        #region Properties

        public IUnitOfWork UnitOfWork
        {
            get { return DependencyResolver.Current.GetService<IUnitOfWork>(); }
        }

        public IMembershipUserConverter MembershipUserConverter
        {
            get { return DependencyResolver.Current.GetService<IMembershipUserConverter>(); }
        }

        #endregion

        #region MembershipProvider implementation

        #region Properties

        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _minRequiredNonAlphanumericCharacters; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
        }

        #endregion

        #region Methods

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            _applicationName = config["applicationName"] ?? HostingEnvironment.ApplicationVirtualPath;
            _enablePasswordRetrieval = GetBooleanValue(config, "enablePasswordRetrieval");
            _enablePasswordReset = GetBooleanValue(config, "enablePasswordReset");
            _requiresQuestionAndAnswer = GetBooleanValue(config, "requiresQuestionAndAnswer");
            _requiresUniqueEmail = GetBooleanValue(config, "requiresUniqueEmail");
            _maxInvalidPasswordAttempts = GetIntValue(config, "maxInvalidPasswordAttempts", 5);
            _passwordAttemptWindow = GetIntValue(config, "passwordAttemptWindow", 10);
            _minRequiredPasswordLength = GetIntValue(config, "minRequiredPasswordLength", 7);
            _minRequiredNonAlphanumericCharacters = GetIntValue(config, "minRequiredNonalphanumericCharacters", 1);
            _passwordStrengthRegularExpression = config["passwordStrengthRegularExpression"];
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
            string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email,
            string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
            out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            var user = UnitOfWork.UserRepository.GetByLogin(username);

            return user != null
                ? MembershipUserConverter.ConvertToMembershipUser(user)
                : null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            var user = UnitOfWork.UserRepository.GetByKey((int) providerUserKey);

            return user != null
                ? MembershipUserConverter.ConvertToMembershipUser(user)
                : null;
        }

        public override string GetUserNameByEmail(string email)
        {
            var user = UnitOfWork.UserRepository.GetByEmail(email);

            return user != null ? user.Name : null;
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            // Для поддержки базы данных с неправильно записанными хэшами, вычисляем хэш 2 разными способами:
            return UnitOfWork.UserRepository.GetByLoginAndPassword(username, PasswordHash.GetMD5Hash(password)) != null
                || UnitOfWork.UserRepository.GetByLoginAndPassword(username, PasswordHash.GetUnicodeMD5Hash(password)) != null;
        }

        #endregion

        #endregion

        #region Private methods
        
        private static bool GetBooleanValue(NameValueCollection config, string valueName)
        {
            bool result;
            var value = config[valueName];

            return bool.TryParse(value, out result) && result;
        }

        private static int GetIntValue(NameValueCollection config, string valueName, int defaultValue)
        {
            int result;
            var value = config[valueName];

            return int.TryParse(value, out result) ? result : defaultValue;
        }

        #endregion
    }
}