﻿using System;
using System.Collections.Specialized;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;
using CommissionsApplication.DAL.Repositories.Abstract;

namespace CommissionsApplication.WebUI.Infrastructure.Auth.Concrete
{
    public class CustomRoleProvider : RoleProvider
    {
        #region Fields

        private string _applicationName;

        #endregion

        #region Properties

        public IUnitOfWork UnitOfWork
        {
            get { return DependencyResolver.Current.GetService<IUnitOfWork>(); }
        }

        #endregion

        #region RoleProvider implementation

        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            _applicationName = config["applicationName"] ?? HostingEnvironment.ApplicationVirtualPath;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var user = UnitOfWork.UserRepository.GetByLogin(username);

            return user != null && user.UserRole.ToString() == roleName;
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = UnitOfWork.UserRepository.GetByLogin(username);

            return user != null ? new[] {user.UserRole.ToString()} : new[] {string.Empty};
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}