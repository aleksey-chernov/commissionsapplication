﻿using System;
using System.Web.Security;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.WebUI.Infrastructure.Auth.Abstract;

namespace CommissionsApplication.WebUI.Infrastructure.Auth.Concrete
{
    public class CustomMembershipUserConverter : IMembershipUserConverter
    {
        public MembershipUser ConvertToMembershipUser(object user)
        {
            var entityUser = user as User;

            if(entityUser == null)
                throw new ArgumentException("User must be of type " + typeof(User).FullName);

            return new MembershipUser("CustomMembershipProvider", entityUser.Login, entityUser.UserId, entityUser.Email,
                string.Empty,
                string.Empty, true, false, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, DateTime.Now, DateTime.Now);
        }
    }
}