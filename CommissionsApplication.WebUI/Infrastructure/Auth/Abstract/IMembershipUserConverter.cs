﻿using System.Web.Security;

namespace CommissionsApplication.WebUI.Infrastructure.Auth.Abstract
{
    public interface IMembershipUserConverter
    {
        MembershipUser ConvertToMembershipUser(object user);
    }
}