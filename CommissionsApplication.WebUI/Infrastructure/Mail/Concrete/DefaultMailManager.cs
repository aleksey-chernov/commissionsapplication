﻿using System.Configuration;
using System.Linq;
using System.Net.Mail;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Export.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Export.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Mail.Abstract;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Mail.Concrete
{
    public class DefaultMailManager : IMailManager
    {
        private readonly string SmtpHost = ConfigurationManager.AppSettings["email:host"];
        private readonly int SmtpPort = int.Parse(ConfigurationManager.AppSettings["email:port"]);
        private readonly string EmailFrom = ConfigurationManager.AppSettings["email:from"];

        private readonly IExportService<CommissionViewModel> _exportService;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DefaultMailManager(IUnitOfWork unitOfWork, IMapper mapper,
            IExportService<CommissionViewModel> exportService)
        {
            _exportService = exportService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void DeliverDelayReport()
        {
            var responsibles = _unitOfWork.ResponsibleRepository.GetAll(include: "Users");

            using (var smtp = new SmtpClient(SmtpHost, SmtpPort))
            {
                foreach (var responsible in responsibles)
                {
                    var commissions =
                        _unitOfWork.CommissionRepository.GetDelayedForResponsible(responsible.ResponsibleId)
                            .Select(c => _mapper.CommissionViewModelMapper.Map(c))
                            .ToList();

                    if (commissions.Count == 0) continue;

                    foreach (var user in responsible.Users)
                    {
                        var exportOptions = new ExportOptions
                        {
                            IgnoreColumns = typeof (CommissionViewModel).GetProperties().Select(prop => prop.Name)
                                .Except(typeof (CommissionAssistViewModel).GetProperties().Select(prop => prop.Name))
                                .ToArray()
                        };

                        using (var stream = _exportService.ToExcel(commissions, exportOptions))
                        {
                            using (var message =
                                new MailMessage(new MailAddress(EmailFrom, "Контроль поручений"),
                                    new MailAddress(user.Email))
                                {
                                    Subject = string.Format("Контрольные поручения ({0})", responsible.Name)
                                })


                            using (var attach = new Attachment(stream, "Таблица поручений.xlsx"))
                            {
                                message.Attachments.Add(attach);

                                smtp.Send(message);
                            }
                        }
                    }
                }
            }
        }
    }
}