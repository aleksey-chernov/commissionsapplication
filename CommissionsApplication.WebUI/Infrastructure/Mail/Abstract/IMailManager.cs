﻿namespace CommissionsApplication.WebUI.Infrastructure.Mail.Abstract
{
    public interface IMailManager
    {
        void DeliverDelayReport();
    }
}