﻿namespace CommissionsApplication.WebUI.Infrastructure.Export.Concrete
{
    public class ExportOptions
    {
        public string[] IgnoreColumns { get; set; }
    }
}