﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using CommissionsApplication.WebUI.Infrastructure.Attributes;
using CommissionsApplication.WebUI.Infrastructure.Export.Abstract;
using CommissionsApplication.WebUI.Models.Concrete;
using SharpExport;
using SharpExport.Attributes;

namespace CommissionsApplication.WebUI.Infrastructure.Export.Concrete
{
    public class CommissionsExportService : IExportService<CommissionViewModel>
    {
        public Stream ToExcel(IEnumerable<CommissionViewModel> commissions, ExportOptions options = null)
        {
            var wb = options == null
                ? commissions.ToXLWorkbook()
                : commissions.ToXLWorkbook(options.IgnoreColumns);

            var expTableAttr =
                typeof (CommissionViewModel).GetCustomAttributes(typeof (ExportTableAttribute), false)
                    .OfType<ExportTableAttribute>()
                    .First();
            var ws = wb.Worksheet(expTableAttr.Name);

            var range = ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address);

            var statusColName = typeof (CommissionViewModel).GetProperty("CommissionStatus")
                .GetCustomAttributes(typeof (ExportColumnAttribute), false)
                .OfType<ExportColumnAttribute>()
                .First().Name;
            int statusColIndex = (from value in range.Rows(1, 1).Cells().Select((c, i) => new {i = i + 1, cell = c})
                where (string) value.cell.Value == statusColName
                select value.i).FirstOrDefault();
            if (statusColIndex == 0)
                throw new Exception("Can not find status column");

            var colorsDict = CreateStatusColorsDictionary();
            foreach (var row in range.Rows(2, range.RowCount()))
            {
                var value = row.Cell(statusColIndex).Value.ToString();
                var color = colorsDict[value];
                row.Style.Fill.BackgroundColor = color;
            }

            range.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            range.Style.Font.FontName = "Arial";
            range.Style.Font.FontSize = 9;

            var ms = new MemoryStream();
            wb.SaveAs(ms);
            ms.Position = 0;

            return ms;
        }

        public Stream ToPDF(IEnumerable<CommissionViewModel> commissions, ExportOptions options = null)
        {
            throw new NotImplementedException();
        }

        private Dictionary<string, XLColor> CreateStatusColorsDictionary()
        {
            var colorsDict = new Dictionary<string, XLColor>();

            foreach (var status in Enum.GetValues(typeof (CommissionStatusModel.Status))
                .Cast<CommissionStatusModel.Status>())
            {
                var key = typeof (CommissionStatusModel.Status).GetField(status.ToString())
                    .GetCustomAttributes(typeof (DropDownNameAttribute), false)
                    .OfType<DropDownNameAttribute>()
                    .First()
                    .Name;

                XLColor value;
                switch (status)
                {
                    case CommissionStatusModel.Status.NotExecuted:
                        value = XLColor.LightCoral;
                        break;
                    case CommissionStatusModel.Status.Executed:
                        value = XLColor.LightGreen;
                        break;
                    case CommissionStatusModel.Status.ThisWeek:
                        value = XLColor.LightYellow;
                        break;
                    case CommissionStatusModel.Status.NextWeek:
                        value = XLColor.LightSalmon;
                        break;
                    case CommissionStatusModel.Status.Question:
                        value = XLColor.LightGray;
                        break;
                    case CommissionStatusModel.Status.Transfer:
                        value = XLColor.LightSteelBlue;
                        break;
                    default:
                        value = XLColor.White;
                        break;
                }

                colorsDict.Add(key, value);
            }

            return colorsDict;
        }
    }
}