﻿using System.Collections.Generic;
using System.IO;
using CommissionsApplication.WebUI.Infrastructure.Export.Concrete;

namespace CommissionsApplication.WebUI.Infrastructure.Export.Abstract
{
    public interface IExportService<in T> where T : class
    {
        Stream ToExcel(IEnumerable<T> commissions, ExportOptions options = null);

        Stream ToPDF(IEnumerable<T> commissions, ExportOptions options = null);
    }
}