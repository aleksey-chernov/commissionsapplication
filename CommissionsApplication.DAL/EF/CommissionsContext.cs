﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Diagnostics;
using CommissionsApplication.DAL.Helpers.Models;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.EF
{
    public class CommissionsContext : DbContext
    {
        public CommissionsContext()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;

#if DEBUG
            Database.Log = s => Debug.WriteLine(s);
#endif
        }

        public DbSet<Attachment> Attachments { get; set; }

        public DbSet<Commission> Commissions { get; set; }

        public DbSet<CommissionView> CommissionViews { get; set; }

        public DbSet<ControlDate> ControlDates { get; set; }

        public DbSet<Responsible> Responsibles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<MailLog> MailLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Attachment>()
                .Ignore(x => x.Data);

            modelBuilder.Entity<Commission>().Ignore(x => x.CommissionRowNumber);
            modelBuilder.Entity<Commission>().Ignore(x => x.IsDelayed);
            modelBuilder.Entity<Commission>().Property(x => x.CommissionStatus).IsRequired();
            modelBuilder.Entity<Commission>().Property(x => x.IncomeDate).IsRequired();
            modelBuilder.Entity<Commission>().Property(x => x.ImportanceFactor).IsRequired();
            modelBuilder.Entity<Commission>().Property(x => x.RowVersion).IsRowVersion();

            modelBuilder.Entity<CommissionView>().ToTable("CommissionViews");
            modelBuilder.Entity<CommissionView>().HasKey(x => x.CommissionId);

            modelBuilder.Entity<User>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.Login)
                .IsRequired()
                .HasMaxLength(99)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UserLogin", 1) {IsUnique = true}));
            modelBuilder.Entity<User>().Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(99)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UserEmail", 1) {IsUnique = true}));
            modelBuilder.Entity<User>().Property(x => x.RowVersion).IsRowVersion();

            modelBuilder.Entity<Responsible>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Responsible>().Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(99)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_ResponsibleEmail", 1) {IsUnique = true}));
            modelBuilder.Entity<Responsible>().Property(x => x.RowVersion).IsRowVersion();
        }
    }
}