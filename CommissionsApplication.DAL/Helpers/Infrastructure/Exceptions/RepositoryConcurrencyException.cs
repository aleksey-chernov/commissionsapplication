﻿using System;

namespace CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions
{
    public class RepositoryConcurrencyException : RepositoryException
    {
        public RepositoryConcurrencyException()
        {
        }
        
        public RepositoryConcurrencyException(string message) : base(message)
        {
        }

        public RepositoryConcurrencyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}