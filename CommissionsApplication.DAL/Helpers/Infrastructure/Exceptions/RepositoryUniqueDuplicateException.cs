﻿using System;

namespace CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions
{
    public class RepositoryUniqueDuplicateException : RepositoryException
    {
        public RepositoryUniqueDuplicateException()
        {
        }
        
        public RepositoryUniqueDuplicateException(string message) : base(message)
        {
        }

        public RepositoryUniqueDuplicateException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public RepositoryUniqueDuplicateException(string message, Exception innerException, )
            : base(message, innerException)
        {

        }
    }
}