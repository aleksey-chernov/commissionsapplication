﻿using System;
using System.Collections.Generic;

namespace CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions
{
    public class RepositoryDuplicateException : RepositoryException
    {
        public string EntityName { get; private set; }

        public IEnumerable<string> PropertiesNames { get; private set; }

        public RepositoryDuplicateException(string entityName, IEnumerable<string> propertiesNames)
        {
            EntityName = entityName;
            PropertiesNames = propertiesNames;
        }

        public RepositoryDuplicateException(string entityName, IEnumerable<string> propertiesNames, Exception innerException)
            : base(null, innerException)
        {
            EntityName = entityName;
            PropertiesNames = propertiesNames;
        }
    }
}