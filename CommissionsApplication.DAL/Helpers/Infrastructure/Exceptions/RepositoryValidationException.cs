﻿using System;
using System.Collections.Generic;

namespace CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions
{
    public class RepositoryValidationException : RepositoryException
    {
        public List<EntityValidationError> ValidationErrors { get; private set; }

        public RepositoryValidationException()
        {
            ValidationErrors = new List<EntityValidationError>();
        }
        
        public RepositoryValidationException(string message) : base(message)
        {
            ValidationErrors = new List<EntityValidationError>();
        }

        public RepositoryValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
            ValidationErrors = new List<EntityValidationError>();
        }
    }

    public class EntityValidationError
    {
        public string EntityName { get; private set; }

        public string PropertyName {get; private set; }

        public EntityValidationError(string propertyName, string entityName)
        {
            PropertyName = propertyName;
            EntityName = entityName;
        }
    }
}