﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using CommissionsApplication.DAL.EF;

namespace CommissionsApplication.DAL.Helpers.Infrastructure
{
    internal static class MetadataHelpers
    {
        public static IEnumerable<UniqueIndexDescription> UniqueIndexDescriptions()
        {
            using (var context = new CommissionsContext())
            {
                var objectContext = ((IObjectContextAdapter) context).ObjectContext;
                var container =
                    objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName,
                        DataSpace.CSpace);
                foreach (var set in container.EntitySets)
                {
                    foreach (var metaProp in set.MetadataProperties.Where(m => m.IsAnnotation))
                    {
                        if (metaProp.Value == null || metaProp.Value.GetType().Name != "EntityTypeConfiguration" ||
                            !metaProp.IsAnnotation) continue;

                        var metaType = metaProp.Value.GetType().BaseType;

                        if (metaType == null) continue;

                        var configsProp = metaType
                            .GetProperty("PrimitivePropertyConfigurations",
                                BindingFlags.NonPublic | BindingFlags.Instance);
                        var configs = configsProp.GetValue(metaProp.Value, null);
                        foreach (var config in (IDictionary) configs)
                        {
                            var configValue = config.GetType().GetProperty("Value").GetValue(config, null);
                            var configValueType = configValue.GetType();
                            while (configValueType != null)
                            {
                                if (configValueType.Name == "PrimitivePropertyConfiguration")
                                    break;

                                configValueType = configValueType.BaseType;
                            }

                            if (configValueType == null) continue;

                            var annotationsProp = configValueType.GetProperty("Annotations",
                                BindingFlags.Public | BindingFlags.Instance);
                            var annotations = annotationsProp.GetValue(configValue, null);
                            foreach (var annotation in (IDictionary) annotations)
                            {
                                var annotationKey =
                                    annotation.GetType().GetProperty("Key").GetValue(annotation, null);

                                if (annotationKey.ToString() != "Index") continue;

                                var annotationValue = annotation.GetType()
                                    .GetProperty("Value")
                                    .GetValue(annotation, null);

                                var indexes = (IEnumerable<IndexAttribute>) annotationValue.GetType()
                                    .GetProperty("Indexes")
                                    .GetValue(annotationValue, null);

                                foreach (var index in indexes)
                                {
                                    var typeProp = metaType
                                        .GetProperty("ClrType", BindingFlags.NonPublic | BindingFlags.Instance);
                                    var type = (Type) typeProp.GetValue(metaProp.Value, null);

                                    var propsInfo = config.GetType().GetProperty("Key").GetValue(config, null);
                                    var props = ((IEnumerable<PropertyInfo>) propsInfo);

                                    yield return new UniqueIndexDescription(index.Name, type, props);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    internal class UniqueIndexDescription
    {
        public string Name { get; private set; }

        public Type Type { get; private set; }

        public IEnumerable<PropertyInfo> Properties { get; private set; }

        public UniqueIndexDescription(string name, Type type, IEnumerable<PropertyInfo> properties)
        {
            Name = name;
            Type = type;
            Properties = properties;
        }
    }
}