﻿namespace CommissionsApplication.DAL.Helpers.Infrastructure
{
    public class PageCriteria
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}