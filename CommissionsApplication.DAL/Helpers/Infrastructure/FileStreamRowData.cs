﻿namespace CommissionsApplication.DAL.Helpers.Infrastructure
{
    public class FileStreamRowData
    {
        public string Path { get; set; }

        public byte[] Transaction { get; set; }
    }
}