﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CommissionsApplication.DAL.Helpers.Infrastructure
{
    public class PasswordHash
    {
        /// <summary>
        /// Устаревшая версия метода для калькуляции MD5 хэш строки. В результате конвертации в Unicode невозможно восстановить
        /// исходный массив байтов, полученный в результате хэширования. Не использовать в дальнейшем.
        /// Подробнее см. здесь http://haacked.com/archive/2012/01/30/hazards-of-converting-binary-data-to-a-string.aspx/
        /// </summary>
        /// <param name="value">Строка, для которой необходимо посчитать значение хэша.</param>
        /// <returns>Строка со значением хэша.</returns>
        public static string GetUnicodeMD5Hash(string value)
        {
            var hashedBytes = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(value));

            return Encoding.UTF8.GetString(hashedBytes);
        }

        /// <summary>
        /// Исправленная версия метода для калькуляции MD5 хэш строки.
        /// </summary>
        /// <param name="value">Строка, для которой необходимо посчитать значение хэша.</param>
        /// <returns>Строка со значением хэша.</returns>
        public static string GetMD5Hash(string value)
        {
            var hash = string.Empty;
            var hashedBytes = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(value));

            var result = hashedBytes.Aggregate(hash, (current, b) => current + $"{b:x2}");
            return result;
        }
    }
}