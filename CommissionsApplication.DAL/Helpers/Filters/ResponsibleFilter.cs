﻿namespace CommissionsApplication.DAL.Helpers.Filters
{
    public class ResponsibleFilter
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}