﻿using System.Collections.Generic;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Helpers.Filters
{
    public class UserFilter
    {
        public string Name { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public IEnumerable<User.Role> UserRoles { get; set; }
    }
}