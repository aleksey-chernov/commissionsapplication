﻿using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Helpers.Filters
{
    public class AttachmentFilter
    {
        public int? CommissionId { get; set; }

        public Attachment.Type? AttachmentType { get; set; }
    }
}