﻿using System;

namespace CommissionsApplication.DAL.Helpers.Models
{
    public class CommissionView
    {
        public int CommissionId { get; set; }

        public Int64 CommissionRowNumber { get; set; }
    }
}