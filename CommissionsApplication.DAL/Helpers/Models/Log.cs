﻿using System;

namespace CommissionsApplication.DAL.Helpers.Models
{
    public class Log
    {
        public int LogId { get; set; }

        public string UserName { get; set; }

        public string Entity { get; set; }

        public string Action { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public DateTime ActionDate { get; set; }
    }
}