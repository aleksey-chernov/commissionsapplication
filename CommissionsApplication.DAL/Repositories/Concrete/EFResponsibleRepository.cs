﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using RefactorThis.GraphDiff;
using LinqKit;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFResponsibleRepository : IResponsibleRepository
    {
        private readonly CommissionsContext _context;
        private readonly EFBaseRepository<Responsible, int> _baseRepository;

        public EFResponsibleRepository(CommissionsContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
            _baseRepository = new EFBaseRepository<Responsible, int>(context);
        }

        public Responsible GetByKey(int key)
        {
            return _context.Responsibles.AsNoTracking()
                .Include(x => x.Charges)
                .Include(x => x.Users)
                .SingleOrDefault(r => r.ResponsibleId == key);
        }

        public IEnumerable<Responsible> GetByKeys(IEnumerable<int> keys, string include = null)
        {
            var query = _context.Responsibles.AsNoTracking()
                .Where(c => keys.Contains(c.ResponsibleId));

            if (include != null)
            {
                query = _baseRepository.IncludeProperties(query, include);
            }

            return query.ToList();
        }

        public IEnumerable<Responsible> GetByUserLogin(string login)
        {
            return _context.Responsibles.AsNoTracking()
                .Include(x => x.Charges)
                .Include(x => x.Users)
                .Where(r => r.Users.Any(u => u.Login == login))
                .ToList();
        }

        public IEnumerable<Responsible> GetAll(string orderBy = null, ResponsibleFilter filter = null,
            string include = null)
        {
            var query = _context.Responsibles.AsNoTracking().AsQueryable();

            if (include != null)
            {
                query = _baseRepository.IncludeProperties(query, include);
            }

            if (filter != null)
            {
                query = GetFiltered(query, filter);
            }

            query = query.OrderBy(orderBy ?? "ResponsibleId");

            return query.ToList();
        }

        public Responsible Insert(Responsible responsible)
        {
            responsible.LastModified = DateTime.Now;
            return UpdateGraph(responsible);
        }

        public void Update(Responsible responsible)
        {
            responsible.LastModified = DateTime.Now;
            UpdateGraph(responsible);
        }

        public void DeleteByKey(int key)
        {
            _baseRepository.DeleteByKey(key);
        }

        public bool ResponsibleEmailUniqueForKey(string email, int key)
        {
            return _context.Responsibles
                .Any(r => r.Email == email && r.ResponsibleId != key);
        }

        public bool Exists(int key)
        {
            return _context.Responsibles.Any(r => r.ResponsibleId == key);
        }

        private Responsible UpdateGraph(Responsible responsible)
        {
            try
            {
                return _context.UpdateGraph(responsible, map =>
                    map.AssociatedCollection(p => p.Users));
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new RepositoryConcurrencyException("Concurrency error on Responsible update happened.", ex);
            }
        }

        private IQueryable<Responsible> GetFiltered(IQueryable<Responsible> query, ResponsibleFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            var predicate = PredicateBuilder.True<Responsible>();

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                predicate = predicate.And(r => r.Name.Contains(filter.Name));
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                predicate = predicate.And(r => r.Email.Contains(filter.Email));
            }

            return query.AsExpandable().Where(predicate);
        }
    }
}