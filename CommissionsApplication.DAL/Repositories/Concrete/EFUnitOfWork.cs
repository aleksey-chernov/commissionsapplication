﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Transactions;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Helpers.Models;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly CommissionsContext _context = new CommissionsContext();

        private readonly string _userName;

        private EFCommissionRepository _commissionRepository;

        private EFAttachmentRepository _attachmentRepository;

        private EFResponsibleRepository _responsibleRepository;

        private EFUserRepository _userRepository;

        public EFUnitOfWork(string userName)
        {
            _userName = userName;
        }

        public ICommissionRepository CommissionRepository
        {
            get { return _commissionRepository ?? (_commissionRepository = new EFCommissionRepository(_context)); }
        }

        public IAttachmentRepository AttachmentRepository
        {
            get { return _attachmentRepository ?? (_attachmentRepository = new EFAttachmentRepository(_context)); }
        }

        public IResponsibleRepository ResponsibleRepository
        {
            get { return _responsibleRepository ?? (_responsibleRepository = new EFResponsibleRepository(_context)); }
        }

        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new EFUserRepository(_context)); }
        }

        public void Commit()
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var logs = GetLogChanges().ToList();

                    var addedAttachments = _context.ChangeTracker.Entries<Attachment>()
                        .Where(e => e.State == EntityState.Added)
                        .Select(e => e.Entity)
                        .ToList();

                    _context.SaveChanges();

                    foreach (var attachment in addedAttachments)
                    {
                        ((EFAttachmentRepository) AttachmentRepository)
                            .SetAttachmentData(attachment.AttachmentId, attachment.Data);
                    }

                    _context.Logs.AddRange(logs);
                    _context.SaveChanges();

                    scope.Complete();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var sb = new StringBuilder();

                foreach (var entry in ex.Entries)
                {
                    sb.AppendLine(string.Format("Concurrency error on {0} update happened.", entry.Entity.GetType().Name));
                }

                throw new RepositoryConcurrencyException(sb.ToString(), ex);
            }
            catch (DbUpdateException ex)
            {
                if (!(ex.InnerException is UpdateException) || !(ex.InnerException.InnerException is SqlException))
                    throw;

                var sqlEx = (SqlException) ex.InnerException.InnerException;

                switch (sqlEx.Number)
                {
                    case 2601:
                        var uniques = MetadataHelpers.UniqueIndexDescriptions();
                        foreach (var unique in uniques.Where(unique => sqlEx.Message.Contains(unique.Name)))
                        {
                            throw new RepositoryDuplicateException(unique.Type.Name,
                                unique.Properties.Select(prop => prop.Name),
                                ex);
                        }
                        break;
                    case 547:
                        throw new RepositoryConcurrencyException(
                            "FK violation exception. See the inner exception for details.", ex);
                }

                throw;
            }
            catch (DbEntityValidationException ex)
            {
                Debug.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Failed on saving changes. See the inner exception for details.", ex);
            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        private IEnumerable<Log> GetLogChanges()
        {
            var objectContext = ((IObjectContextAdapter) _context).ObjectContext;
            var contextEntries = _context.ChangeTracker.Entries()
                .Where(e =>
                    e.State == EntityState.Added || e.State == EntityState.Deleted ||
                    e.State == EntityState.Modified);
            var datetime = DateTime.Now;

            foreach (var contextEntry in contextEntries)
            {
                var log = new Log
                {
                    UserName = _userName,
                    Action = contextEntry.State.ToString(),
                    ActionDate = datetime,
                    Entity = contextEntry.Entity.GetType().Name
                };

                var oldValueSb = new StringBuilder();
                var newValueSb = new StringBuilder();

                switch (contextEntry.State)
                {
                    case EntityState.Added:
                        foreach (var prop in contextEntry.CurrentValues.PropertyNames)
                        {
                            newValueSb.Append(string.Format("{0}:{1};", prop, contextEntry.CurrentValues[prop]));
                        }
                        break;
                    case EntityState.Deleted:
                        foreach (var prop in contextEntry.OriginalValues.PropertyNames)
                        {
                            oldValueSb.Append(string.Format("{0}:{1};", prop, contextEntry.OriginalValues[prop]));
                        }
                        break;
                    case EntityState.Modified:
                        ObjectStateEntry objectEntry =
                            objectContext.ObjectStateManager.GetObjectStateEntry(contextEntry.Entity);
                        foreach (var prop in objectEntry.GetModifiedProperties())
                        {
                            oldValueSb.Append(string.Format("{0}:{1};", prop, contextEntry.OriginalValues[prop]));
                            newValueSb.Append(string.Format("{0}:{1};", prop, contextEntry.CurrentValues[prop]));
                        }
                        break;
                }

                log.OldValue = oldValueSb.ToString();
                log.NewValue = newValueSb.ToString();

                yield return log;
            }
        }
    }
}