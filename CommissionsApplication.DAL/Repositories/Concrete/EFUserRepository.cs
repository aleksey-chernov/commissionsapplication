﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using LinqKit;
using RefactorThis.GraphDiff;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFUserRepository : IUserRepository
    {
        private readonly CommissionsContext _context;
        private readonly EFBaseRepository<User, int> _baseRepository;

        public EFUserRepository(CommissionsContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
            _baseRepository = new EFBaseRepository<User, int>(context);
        }

        public User GetByKey(int key)
        {
            return _context.Users.AsNoTracking().
                Include(x => x.Responsibles)
                .SingleOrDefault(u => u.UserId == key);
        }

        public IEnumerable<User> GetByKeys(IEnumerable<int> keys, string include = null)
        {
            var query = _context.Users.AsNoTracking()
                .Where(u => keys.Contains(u.UserId));

            if (include != null)
            {
                query = _baseRepository.IncludeProperties(query, include);
            }

            return query.ToList();
        }

        public User GetByLogin(string login)
        {
            return _context.Users.AsNoTracking()
                .Include(x => x.Responsibles)
                .SingleOrDefault(u => u.Login == login);
        }

        public User GetByLoginAndPassword(string login, string password)
        {
            return _context.Users.AsNoTracking()
                .Include(x => x.Responsibles)
                .SingleOrDefault(u => u.Login == login && u.Password == password);
        }

        public User GetByEmail(string email)
        {
            return _context.Users.AsNoTracking()
                .Include(x => x.Responsibles)
                .SingleOrDefault(u => u.Email == email);
        }

        public string GetPasswordByKey(int key)
        {
            var user = _context.Users.AsNoTracking()
                .SingleOrDefault(u => u.UserId == key);
            return user != null ? user.Password : string.Empty;
        }

        public IEnumerable<User> GetByResponsible(int id)
        {
            return _context.Users.AsNoTracking()
                .Include(x => x.Responsibles)
                .Where(u => u.Responsibles.Any(r => r.ResponsibleId == id));
        }

        public IEnumerable<User> GetAll(string orderBy = null, UserFilter filter = null)
        {
            var query = _context.Users.AsNoTracking()
                .Include(x => x.Responsibles);

            if (filter != null)
            {
                query = GetFiltered(query, filter);
            }

            query = query.OrderBy(orderBy ?? "UserId");

            return query.ToList();
        }

        public User Insert(User user)
        {
            user.LastModified = DateTime.Now;
            return UpdateGraph(user);
        }

        public void Update(User user)
        {
            user.LastModified = DateTime.Now;
            UpdateGraph(user);
        }

        public void DeleteByKey(int key)
        {
            _baseRepository.DeleteByKey(key);
        }

        public bool UserEmailUniqueForKey(string email, int key)
        {
            return _context.Users
                .Any(u => u.Email == email && u.UserId != key);
        }

        public bool UserLoginUniqueForKey(string login, int key)
        {
            return _context.Users
                .Any(u => u.Login == login && u.UserId != key);
        }

        public bool Exists(int key)
        {
            return _context.Users.Any(u => u.UserId == key);
        }

        private User UpdateGraph(User user)
        {
            try
            {
                GraphDiffConfiguration.ReloadAssociatedEntitiesWhenAttached = true;

                return _context.UpdateGraph(user, map =>
                    map.AssociatedCollection(p => p.Responsibles));
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new RepositoryConcurrencyException("Concurrency error on User update happened.", ex);
            }
        }

        private IQueryable<User> GetFiltered(IQueryable<User> query, UserFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            var predicate = PredicateBuilder.True<User>();

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                predicate = predicate.And(u => u.Name.Contains(filter.Name));
            }
            if (!string.IsNullOrWhiteSpace(filter.Login))
            {
                predicate = predicate.And(u => u.Login.Contains(filter.Login));
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                predicate = predicate.And(u => u.Email.Contains(filter.Email));
            }
            if (filter.UserRoles != null && filter.UserRoles.Count() != 0)
            {
                predicate = predicate.And(u => filter.UserRoles.Contains(u.UserRole));
            }

            return query.AsExpandable().Where(predicate);
        }
    }
}