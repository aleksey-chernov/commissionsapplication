﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Transactions;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using LinqKit;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFAttachmentRepository : IAttachmentRepository
    {
        private readonly CommissionsContext _context;
        private readonly EFBaseRepository<Attachment, int> _baseRepository;

        public EFAttachmentRepository(CommissionsContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
            _baseRepository = new EFBaseRepository<Attachment, int>(context);
        }

        public Attachment GetByKey(int key)
        {
            return _context.Attachments.AsNoTracking()
                .SingleOrDefault(a => a.AttachmentId == key);
        }

        public void GetAttachmentData(int key, Stream output)
        {
            using (var scope = new TransactionScope())
            {
                var rowData = _context.Database.SqlQuery<FileStreamRowData>(
                    "SELECT Data.PathName() AS 'Path', GET_FILESTREAM_TRANSACTION_CONTEXT() AS 'Transaction' FROM dbo.Attachments WHERE AttachmentId = @id",
                    new SqlParameter("id", key)).SingleOrDefault();

                if (rowData == null)
                    return;

                using (var stream = new SqlFileStream(rowData.Path, rowData.Transaction, FileAccess.Read))
                {
                    stream.CopyTo(output);
                }

                scope.Complete();
            }
        }

        public IEnumerable<Attachment> GetAll(string orderBy = null, AttachmentFilter filter = null)
        {
            var query = _context.Attachments.AsNoTracking()
                .AsQueryable();

            if (filter != null)
            {
                query = GetFiltered(query, filter);
            }

            query = query.OrderBy(orderBy ?? "AttachmentId");

            return query.ToList();
        }

        public IEnumerable<Attachment> GetByCommissionAndType(int id, Attachment.Type type)
        {
            var query = _context.Attachments.AsNoTracking()
                .Where(a => a.CommissionId == id && a.AttachmentType == type);

            return query;
        }

        public Attachment Insert(Attachment attachment)
        {
            return _baseRepository.Insert(attachment);
        } 

        public void DeleteByKey(int key)
        {
            _baseRepository.DeleteByKey(key);
        }

        public bool RelatesToUser(int key, string login)
        {
            return _context.Commissions
                .Include(x => x.Attachments)
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(ch => ch.Responsible))
                .Include(x => x.Charges.Select(ch => ch.Responsible.Users))
                .Any(c => c.Attachments.Any(a => a.AttachmentId == key) && c.Charges.Any(ch => ch.Responsible.Users.Any(u => u.Login == login)));
        }

        internal void SetAttachmentData(int key, Stream data)
        {
            var rowData = _context.Database.SqlQuery<FileStreamRowData>(
                "SELECT Data.PathName() AS 'Path', GET_FILESTREAM_TRANSACTION_CONTEXT() AS 'Transaction' FROM dbo.Attachments WHERE AttachmentId = @id",
                new SqlParameter("id", key)).SingleOrDefault();

            if (rowData == null)
                return;

            using (var stream = new SqlFileStream(rowData.Path, rowData.Transaction, FileAccess.Write))
            {
                data.CopyTo(stream);
            }
        }

        private IQueryable<Attachment> GetFiltered(IQueryable<Attachment> query, AttachmentFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            var predicate = PredicateBuilder.True<Attachment>();

            if (filter.CommissionId.HasValue)
            {
                predicate = predicate.And(a => a.CommissionId == filter.CommissionId.Value);
            }
            if (filter.AttachmentType.HasValue)
            {
                predicate = predicate.And(a => a.AttachmentType == filter.AttachmentType.Value);
            }

            return query.AsExpandable().Where(predicate);
        }
    }
}