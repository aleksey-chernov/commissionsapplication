﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.DAL.Helpers.Infrastructure.Exceptions;
using CommissionsApplication.DAL.Repositories.Abstract;
using CommissionsApplication.Domain.Entities;
using LinqKit;
using RefactorThis.GraphDiff;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFCommissionRepository : ICommissionRepository
    {
        private readonly EFBaseRepository<Commission, int> _baseRepository;
        private readonly CommissionsContext _context;

        public EFCommissionRepository(CommissionsContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
            _baseRepository = new EFBaseRepository<Commission, int>(context);
        }

        public Commission GetByKey(int key)
        {
            return _context.Commissions.AsNoTracking()
                .Include(x => x.Attachments)
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(c => c.Responsible))
                .Include(x => x.ControlDates)
                .SingleOrDefault(c => c.CommissionId == key);
        }

        public IList<Commission> GetAll(string orderBy = null, CommissionFilter filter = null)
        {
            var query = _context.Commissions.AsNoTracking()
                .Include(x => x.Attachments)
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(c => c.Responsible))
                .Include(x => x.ControlDates);

            if (filter != null)
            {
                query = GetFiltered(query, filter);
            }

            query = GetOrdered(query, orderBy);

            return AddRowNumbers(query.ToList());
        }

        public IEnumerable<Commission> GetAllPaginated(out int count, PageCriteria pageCriteria,
            string orderBy = null, CommissionFilter filter = null)
        {
            var query = _context.Commissions.AsNoTracking()
                .Include(x => x.Attachments)
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(c => c.Responsible))
                .Include(x => x.ControlDates);

            if (filter != null)
            {
                query = GetFiltered(query, filter);
            }

            query = GetOrdered(query, orderBy);

            count = query.Count();
            query = _baseRepository.GetPaged(query, pageCriteria);

            return AddRowNumbers(query.ToList());
        }

        public IList<Commission> GetDelayedForResponsible(int id)
        {
            var query = _context.Commissions.AsNoTracking()
                .Include(x => x.Attachments)
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(ch => ch.Responsible))
                .Include(x => x.ControlDates)
                .Where(c => c.Charges.Any(ch => ch.ResponsibleId == id && !ch.ExecutionDate.HasValue)
                            && Commission.DelayedStatusList.Contains(c.CommissionStatus)
                            && !c.ExecutionDate.HasValue);

            return AddRowNumbers(query.ToList());
        }

        public Commission Insert(Commission commission)
        {
            commission.LastModified = DateTime.Now;
            return UpdateGraph(commission);
        }

        public void Update(Commission commission)
        {
            commission.LastModified = DateTime.Now;
            UpdateGraph(commission);
        }

        public void DeleteByKey(int key)
        {
            _baseRepository.DeleteByKey(key);
        }

        public bool Exists(int key)
        {
            return _context.Commissions.Any(c => c.CommissionId == key);
        }

        public bool RelatesToUser(int key, string login)
        {
            return _context.Commissions
                .Include(x => x.Charges)
                .Include(x => x.Charges.Select(ch => ch.Responsible))
                .Include(x => x.Charges.Select(ch => ch.Responsible.Users))
                .Any(c => c.CommissionId == key && c.Charges.Any(ch => ch.Responsible.Users.Any(u => u.Login == login)));
        }

        private Commission UpdateGraph(Commission commission)
        {
            try
            {
                return _context.UpdateGraph(commission, map =>
                    map.OwnedCollection(c => c.ControlDates)
                        .OwnedCollection(c => c.Attachments)
                        .OwnedCollection(c => c.Charges, with => with.AssociatedEntity(ch => ch.Responsible)));
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new RepositoryConcurrencyException("Concurrency error on Commission update happened.", ex);
            }
        }

        private IQueryable<Commission> GetOrdered(IQueryable<Commission> query, string orderBy)
        {
            if (orderBy == "CommissionRowNumber")
            {
                query = query.OrderBy("CommissionId");
            }
            else if (orderBy == "CommissionRowNumber desc")
            {
                query = query.OrderBy("CommissionId desc");
            }
            else
            {
                query = query.OrderBy(orderBy ?? "CommissionId");
            }

            return query;
        }

        private IQueryable<Commission> GetFiltered(IQueryable<Commission> query, CommissionFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            var predicate = PredicateBuilder.True<Commission>();

            if (filter.CommissionId.HasValue)
            {
                // костыль для поиска не по идентификатору, а по номеру строки
                var commissionId = _context.CommissionViews
                    .FirstOrDefault(cv => cv.CommissionRowNumber == filter.CommissionId)?.CommissionId;

               predicate = predicate.And(c => commissionId.HasValue && c.CommissionId == commissionId);
            }
            if (filter.CommissionStatuses != null && filter.CommissionStatuses.Count() != 0)
            {
                predicate = predicate.And(c => filter.CommissionStatuses.Contains(c.CommissionStatus));
            }
            if (filter.ControlDate?.StartDate != null && filter.ControlDate.EndDate.HasValue)
            {
                predicate =
                    predicate.And(
                        c =>
                            c.ControlDates.Any(
                                d => d.Date >= filter.ControlDate.StartDate && d.Date <= filter.ControlDate.EndDate));
            }
            if (!string.IsNullOrEmpty(filter.OutcomeDocument))
            {
                predicate = predicate.And(c => c.OutcomeDocument.Contains(filter.OutcomeDocument));
            }
            if (!string.IsNullOrEmpty(filter.IncomeDocument))
            {
                predicate = predicate.And(c => c.IncomeDocument.Contains(filter.IncomeDocument));
            }
            if (!string.IsNullOrEmpty(filter.Summary))
            {
                predicate = predicate.And(c => c.Summary.Contains(filter.Summary));
            }
            if (!string.IsNullOrEmpty(filter.Organization))
            {
                predicate = predicate.And(c => c.Organization.Contains(filter.Organization));
            }
            if (!string.IsNullOrEmpty(filter.Signer))
            {
                predicate = predicate.And(c => c.Signer.Contains(filter.Signer));
            }
            if (!string.IsNullOrEmpty(filter.Resolution))
            {
                predicate = predicate.And(c => c.Resolution.Contains(filter.Resolution));
            }
            if (!string.IsNullOrEmpty(filter.Commentary))
            {
                predicate = predicate.And(c => c.Commentary.Contains(filter.Commentary));
            }
            if (!string.IsNullOrEmpty(filter.ExecutionMark))
            {
                predicate = predicate.And(c => c.ExecutionMark.Contains(filter.ExecutionMark));
            }
            if (filter.ExecutionDate.HasValue)
            {
                predicate = predicate.And(c =>
                    c.ExecutionDate.Value.Year == filter.ExecutionDate.Value.Year
                    && c.ExecutionDate.Value.Month == filter.ExecutionDate.Value.Month
                    && c.ExecutionDate.Value.Day == filter.ExecutionDate.Value.Day);
            }
            if (!string.IsNullOrEmpty(filter.RegNumber))
            {
                predicate = predicate.And(c => c.RegNumber.Contains(filter.RegNumber));
            }
            if (!string.IsNullOrEmpty(filter.CommissionNumber))
            {
                predicate = predicate.And(c => c.CommissionNumber.Contains(filter.CommissionNumber));
            }
            if (filter.ImportanceFactor.HasValue)
            {
                predicate = predicate.And(c => c.ImportanceFactor == filter.ImportanceFactor.Value);
            }
            if (filter.DelayFactor.HasValue)
            {
                predicate = predicate.And(c => c.DelayFactor == filter.DelayFactor);
            }
            if (!string.IsNullOrEmpty(filter.Reason))
            {
                predicate = predicate.And(c => c.Reason.Contains(filter.Reason));
            }
            if (filter.Responsibles != null && filter.Responsibles.Count() != 0)
            {
                predicate = predicate.And(c => c.Charges.Any(ch => filter.Responsibles.Contains(ch.ResponsibleId)));
            }
            if (filter.IncomeDate != null)
            {
                if (filter.IncomeDate.StartDate != null)
                {
                    predicate = predicate.And(c => c.IncomeDate >= filter.IncomeDate.StartDate);
                }
                if (filter.IncomeDate.EndDate != null)
                {
                    predicate = predicate.And(c => c.IncomeDate <= filter.IncomeDate.EndDate);
                }
            }

            return query.AsExpandable().Where(predicate);
        }

        private List<Commission> AddRowNumbers(List<Commission> commissions)
        {
            var commissionIds = commissions.Select(c => c.CommissionId);

            var rownumbers = _context.CommissionViews
                .Where(cv => commissionIds.Contains(cv.CommissionId))
                .ToDictionary(cv => cv.CommissionId);

            foreach (var commission in commissions)
            {
                commission.CommissionRowNumber = (int) rownumbers[commission.CommissionId].CommissionRowNumber;
            }

            foreach (var commission in commissions)
            {
                commission.CommissionRowNumber = (int) rownumbers[commission.CommissionId].CommissionRowNumber;
            }

            return commissions;
        }
    }
}