﻿using System;
using System.Data.Entity;
using System.Linq;
using CommissionsApplication.DAL.Helpers.Infrastructure;

namespace CommissionsApplication.DAL.Repositories.Concrete
{
    public class EFBaseRepository<TEntity, TKey> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public EFBaseRepository(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public TEntity GetByKey(TKey key)
        {
            var entity = _dbSet.Find(key);

            if (entity != null)
            {
                _context.Entry(entity).State = EntityState.Detached;    
            }

            return entity;
        }

        public TEntity Insert(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Added;
            return _dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
                return;

            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }

        public void DeleteByKey(TKey key)
        {
            var entity = _dbSet.Find(key);
            Delete(entity);
        }

        public IQueryable<TEntity> GetPaged(IQueryable<TEntity> query, PageCriteria pageCriteria)
        {
            return query.Skip((pageCriteria.PageIndex - 1) * pageCriteria.PageSize).Take(pageCriteria.PageSize);
        }

        public IQueryable<TEntity> IncludeProperties(IQueryable<TEntity> query, string include)
        {
            foreach (var includeProperty in include.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return query;
        }
    }
}