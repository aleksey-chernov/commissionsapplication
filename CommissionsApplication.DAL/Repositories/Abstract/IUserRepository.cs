﻿using System.Collections.Generic;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Repositories.Abstract
{
    public interface IUserRepository
    {
        User GetByKey(int key);

        IEnumerable<User> GetByKeys(IEnumerable<int> keys, string include = null);

        User GetByLogin(string login);

        User GetByLoginAndPassword(string login, string password);

        User GetByEmail(string email);

        string GetPasswordByKey(int key);

        IEnumerable<User> GetByResponsible(int id);
            
        IEnumerable<User> GetAll(string orderBy = null, UserFilter filter = null);

        User Insert(User user);

        void Update(User user);

        void DeleteByKey(int key);

        bool UserEmailUniqueForKey(string email, int key);

        bool UserLoginUniqueForKey(string email, int key);

        bool Exists(int key);
    }
}