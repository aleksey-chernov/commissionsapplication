﻿using System;

namespace CommissionsApplication.DAL.Repositories.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        ICommissionRepository CommissionRepository { get; }
        
        IAttachmentRepository AttachmentRepository { get; }
        
        IResponsibleRepository ResponsibleRepository { get; }

        IUserRepository UserRepository { get; }

        void Commit();
    }
}