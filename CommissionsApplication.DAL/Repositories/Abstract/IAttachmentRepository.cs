﻿using System.Collections.Generic;
using System.IO;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Repositories.Abstract
{
    public interface IAttachmentRepository
    {
        Attachment GetByKey(int key);

        void GetAttachmentData(int key, Stream output);

        IEnumerable<Attachment> GetAll(string orderBy = null, AttachmentFilter filter = null);

        IEnumerable<Attachment> GetByCommissionAndType(int id, Attachment.Type type);

        Attachment Insert(Attachment attachment);

        void DeleteByKey(int key);

        bool RelatesToUser(int key, string login);
    }
}