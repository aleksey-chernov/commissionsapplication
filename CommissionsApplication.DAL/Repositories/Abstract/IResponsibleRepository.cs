﻿using System.Collections.Generic;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Repositories.Abstract
{
    public interface IResponsibleRepository
    {
        Responsible GetByKey(int key);

        IEnumerable<Responsible> GetByKeys(IEnumerable<int> keys, string include = null);
        
        IEnumerable<Responsible> GetByUserLogin(string login);
        
        IEnumerable<Responsible> GetAll(string orderBy = null, ResponsibleFilter filter = null, string include = null);

        Responsible Insert(Responsible responsible);

        void Update(Responsible responsible);
        
        void DeleteByKey(int key);
        
        bool ResponsibleEmailUniqueForKey(string email, int key);

        bool Exists(int key);
    }
}