﻿using System.Collections.Generic;
using CommissionsApplication.DAL.Helpers.Filters;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.Domain.Entities;

namespace CommissionsApplication.DAL.Repositories.Abstract
{
    public interface ICommissionRepository
    {
        Commission GetByKey(int key);

        IList<Commission> GetAll(string orderBy = null, CommissionFilter filter = null);

        IEnumerable<Commission> GetAllPaginated(out int count, PageCriteria pageCriteria = null,
            string orderBy = null, CommissionFilter filter = null);

        IList<Commission> GetDelayedForResponsible(int id);

        Commission Insert(Commission commission);

        void Update(Commission commission);

        void DeleteByKey(int key);

        bool Exists(int key);

        bool RelatesToUser(int key, string login);
    }
}