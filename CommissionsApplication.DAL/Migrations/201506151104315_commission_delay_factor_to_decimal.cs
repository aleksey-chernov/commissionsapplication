namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_delay_factor_to_decimal : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Commissions", "DelayFactor");
            AddColumn("dbo.Commissions", "DelayFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commissions", "DelayFactor");
            AddColumn("dbo.Commissions", "DelayFactor", c => c.String());
        }
    }
}