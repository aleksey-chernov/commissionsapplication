namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false, identity: true),
                        CommissionId = c.Int(nullable: false),
                        Name = c.String(),
                        Commission_CommissionId = c.Int(),
                        Commission_CommissionId1 = c.Int(),
                        Commission_CommissionId2 = c.Int(),
                        Commission_CommissionId3 = c.Int(),
                        Commission_CommissionId4 = c.Int(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId1)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId2)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId3)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId4)
                .Index(t => t.Commission_CommissionId)
                .Index(t => t.Commission_CommissionId1)
                .Index(t => t.Commission_CommissionId2)
                .Index(t => t.Commission_CommissionId3)
                .Index(t => t.Commission_CommissionId4);
            
            CreateTable(
                "dbo.AttachmentDatas",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false),
                        Data = c.Binary(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId)
                .Index(t => t.AttachmentId);
            
            CreateTable(
                "dbo.Commissions",
                c => new
                    {
                        CommissionId = c.Int(nullable: false, identity: true),
                        CommissionStatus = c.Int(nullable: false),
                        OutcomeDocument = c.String(),
                        IncomeDocument = c.String(),
                        IncomeDate = c.DateTime(nullable: false),
                        Summary = c.String(),
                        Organization = c.String(),
                        Signer = c.String(),
                        Resolution = c.String(),
                        CommissionDate = c.DateTime(nullable: false),
                        Commentary = c.String(),
                        ExecutionDate = c.DateTime(nullable: false),
                        RegNumber = c.String(),
                        CommissionNumber = c.String(),
                        ImportanceFactor = c.Int(nullable: false),
                        DelayFactor = c.String(),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.CommissionId);
            
            CreateTable(
                "dbo.ControlDates",
                c => new
                    {
                        ControlDateId = c.Int(nullable: false, identity: true),
                        CommissionId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ControlDateId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .Index(t => t.CommissionId);
            
            CreateTable(
                "dbo.Ids",
                c => new
                    {
                        ResponsibleId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ResponsibleId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Login = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ResponsibleCommissions",
                c => new
                    {
                        Responsible_ResponsibleId = c.Int(nullable: false),
                        Commission_CommissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Responsible_ResponsibleId, t.Commission_CommissionId })
                .ForeignKey("dbo.Ids", t => t.Responsible_ResponsibleId, cascadeDelete: true)
                .ForeignKey("dbo.Commissions", t => t.Commission_CommissionId, cascadeDelete: true)
                .Index(t => t.Responsible_ResponsibleId)
                .Index(t => t.Commission_CommissionId);
            
            CreateTable(
                "dbo.UserResponsibles",
                c => new
                    {
                        User_UserId = c.Int(nullable: false),
                        Responsible_ResponsibleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserId, t.Responsible_ResponsibleId })
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Ids", t => t.Responsible_ResponsibleId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.Responsible_ResponsibleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attachments", "Commission_CommissionId4", "dbo.Commissions");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserResponsibles", "Responsible_ResponsibleId", "dbo.Ids");
            DropForeignKey("dbo.UserResponsibles", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.ResponsibleCommissions", "Commission_CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.ResponsibleCommissions", "Responsible_ResponsibleId", "dbo.Ids");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId3", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId2", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId1", "dbo.Commissions");
            DropForeignKey("dbo.ControlDates", "CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments");
            DropIndex("dbo.UserResponsibles", new[] { "Responsible_ResponsibleId" });
            DropIndex("dbo.UserResponsibles", new[] { "User_UserId" });
            DropIndex("dbo.ResponsibleCommissions", new[] { "Commission_CommissionId" });
            DropIndex("dbo.ResponsibleCommissions", new[] { "Responsible_ResponsibleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.ControlDates", new[] { "CommissionId" });
            DropIndex("dbo.AttachmentDatas", new[] { "AttachmentId" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId4" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId3" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId2" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId1" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId" });
            DropTable("dbo.UserResponsibles");
            DropTable("dbo.ResponsibleCommissions");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Users");
            DropTable("dbo.Ids");
            DropTable("dbo.ControlDates");
            DropTable("dbo.Commissions");
            DropTable("dbo.AttachmentDatas");
            DropTable("dbo.Attachments");
        }
    }
}
