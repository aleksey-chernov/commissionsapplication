namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_attachment1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Attachments", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Attachments", "CreatedDate");
        }
    }
}
