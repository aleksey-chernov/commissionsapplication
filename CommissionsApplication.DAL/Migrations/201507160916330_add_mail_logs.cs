namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_mail_logs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MailLogs",
                c => new
                    {
                        MailLogId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Thread = c.String(),
                        Level = c.String(),
                        Logger = c.String(),
                        Message = c.String(),
                        Exception = c.String(),
                    })
                .PrimaryKey(t => t.MailLogId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MailLogs");
        }
    }
}
