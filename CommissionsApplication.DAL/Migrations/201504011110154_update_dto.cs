namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_dto : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Attachment", newName: "Attachments");
            RenameTable(name: "dbo.Commission", newName: "Commissions");
            RenameTable(name: "dbo.ControlDate", newName: "ControlDates");
            RenameTable(name: "dbo.Responsible", newName: "Responsibles");
            RenameTable(name: "dbo.User", newName: "Users");
            RenameTable(name: "dbo.ResponsibleCommission", newName: "ResponsibleDTOCommissionDTOes");
            RenameTable(name: "dbo.UserResponsible", newName: "UserDTOResponsibleDTOes");
            RenameColumn(table: "dbo.ResponsibleDTOCommissionDTOes", name: "Responsible_ResponsibleId", newName: "ResponsibleDTO_ResponsibleId");
            RenameColumn(table: "dbo.ResponsibleDTOCommissionDTOes", name: "Commission_CommissionId", newName: "CommissionDTO_CommissionId");
            RenameColumn(table: "dbo.UserDTOResponsibleDTOes", name: "User_UserId", newName: "UserDTO_UserId");
            RenameColumn(table: "dbo.UserDTOResponsibleDTOes", name: "Responsible_ResponsibleId", newName: "ResponsibleDTO_ResponsibleId");
            RenameIndex(table: "dbo.ResponsibleDTOCommissionDTOes", name: "IX_Responsible_ResponsibleId", newName: "IX_ResponsibleDTO_ResponsibleId");
            RenameIndex(table: "dbo.ResponsibleDTOCommissionDTOes", name: "IX_Commission_CommissionId", newName: "IX_CommissionDTO_CommissionId");
            RenameIndex(table: "dbo.UserDTOResponsibleDTOes", name: "IX_User_UserId", newName: "IX_UserDTO_UserId");
            RenameIndex(table: "dbo.UserDTOResponsibleDTOes", name: "IX_Responsible_ResponsibleId", newName: "IX_ResponsibleDTO_ResponsibleId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserDTOResponsibleDTOes", name: "IX_ResponsibleDTO_ResponsibleId", newName: "IX_Responsible_ResponsibleId");
            RenameIndex(table: "dbo.UserDTOResponsibleDTOes", name: "IX_UserDTO_UserId", newName: "IX_User_UserId");
            RenameIndex(table: "dbo.ResponsibleDTOCommissionDTOes", name: "IX_CommissionDTO_CommissionId", newName: "IX_Commission_CommissionId");
            RenameIndex(table: "dbo.ResponsibleDTOCommissionDTOes", name: "IX_ResponsibleDTO_ResponsibleId", newName: "IX_Responsible_ResponsibleId");
            RenameColumn(table: "dbo.UserDTOResponsibleDTOes", name: "ResponsibleDTO_ResponsibleId", newName: "Responsible_ResponsibleId");
            RenameColumn(table: "dbo.UserDTOResponsibleDTOes", name: "UserDTO_UserId", newName: "User_UserId");
            RenameColumn(table: "dbo.ResponsibleDTOCommissionDTOes", name: "CommissionDTO_CommissionId", newName: "Commission_CommissionId");
            RenameColumn(table: "dbo.ResponsibleDTOCommissionDTOes", name: "ResponsibleDTO_ResponsibleId", newName: "Responsible_ResponsibleId");
            RenameTable(name: "dbo.UserDTOResponsibleDTOes", newName: "UserResponsible");
            RenameTable(name: "dbo.ResponsibleDTOCommissionDTOes", newName: "ResponsibleCommission");
            RenameTable(name: "dbo.Users", newName: "User");
            RenameTable(name: "dbo.Ids", newName: "Responsible");
            RenameTable(name: "dbo.ControlDates", newName: "ControlDate");
            RenameTable(name: "dbo.Commissions", newName: "Commission");
            RenameTable(name: "dbo.Attachments", newName: "Attachment");
        }
    }
}
