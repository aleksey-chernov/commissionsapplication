namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_execution_date_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Commissions", "ExecutionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Commissions", "ExecutionDate", c => c.DateTime(nullable: false));
        }
    }
}
