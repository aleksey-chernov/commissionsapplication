namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Assists",
                c => new
                    {
                        AssistId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ResponsibleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AssistId)
                .ForeignKey("dbo.Responsibles", t => t.ResponsibleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ResponsibleId);
            
            CreateTable(
                "dbo.Responsibles",
                c => new
                    {
                        ResponsibleId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ResponsibleId);
            
            CreateTable(
                "dbo.CommissionResponsibilities",
                c => new
                    {
                        CommissionResponsibilityId = c.Int(nullable: false, identity: true),
                        CommissionId = c.Int(nullable: false),
                        ResponsibleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommissionResponsibilityId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .ForeignKey("dbo.Responsibles", t => t.ResponsibleId, cascadeDelete: true)
                .Index(t => t.CommissionId)
                .Index(t => t.ResponsibleId);
            
            CreateTable(
                "dbo.Commissions",
                c => new
                    {
                        CommissionId = c.Int(nullable: false, identity: true),
                        CommissionStatus = c.Int(nullable: false),
                        OutcomeDocument = c.String(),
                        IncomeDocument = c.String(),
                        IncomeDate = c.DateTime(nullable: false),
                        Summary = c.String(),
                        Organization = c.String(),
                        Signer = c.String(),
                        Resolution = c.String(),
                        CommissionDate = c.DateTime(nullable: false),
                        Commentary = c.String(),
                        ExecutionDate = c.DateTime(nullable: false),
                        RegNumber = c.String(),
                        CommissionNumber = c.String(),
                        ImportanceFactor = c.Int(nullable: false),
                        DelayFactor = c.String(),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.CommissionId);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false, identity: true),
                        CommissionId = c.Int(nullable: false),
                        Name = c.String(),
                        AttachmentType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .Index(t => t.CommissionId);
            
            CreateTable(
                "dbo.ControlDates",
                c => new
                    {
                        ControlDateId = c.Int(nullable: false, identity: true),
                        CommissionId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ControlDateId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .Index(t => t.CommissionId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Login = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        UserRole = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Assists", "UserId", "dbo.Users");
            DropForeignKey("dbo.CommissionResponsibilities", "ResponsibleId", "dbo.Responsibles");
            DropForeignKey("dbo.ControlDates", "CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.CommissionResponsibilities", "CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.Assists", "ResponsibleId", "dbo.Responsibles");
            DropIndex("dbo.ControlDates", new[] { "CommissionId" });
            DropIndex("dbo.Attachments", new[] { "CommissionId" });
            DropIndex("dbo.CommissionResponsibilities", new[] { "ResponsibleId" });
            DropIndex("dbo.CommissionResponsibilities", new[] { "CommissionId" });
            DropIndex("dbo.Assists", new[] { "ResponsibleId" });
            DropIndex("dbo.Assists", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.ControlDates");
            DropTable("dbo.Attachments");
            DropTable("dbo.Commissions");
            DropTable("dbo.CommissionResponsibilities");
            DropTable("dbo.Responsibles");
            DropTable("dbo.Assists");
        }
    }
}
