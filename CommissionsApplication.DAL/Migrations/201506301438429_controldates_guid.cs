namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class controldates_guid : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ControlDates");

            CreateTable(
                "dbo.ControlDates",
                c => new
                {
                    ControlDateId = c.Guid(nullable: false, identity: true),
                    CommissionId = c.Int(nullable: false),
                    Date = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.ControlDateId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .Index(t => t.CommissionId);
        }
        
        public override void Down()
        {
            DropTable("dbo.ControlDates");

            CreateTable(
                "dbo.ControlDates",
                c => new
                {
                    ControlDateId = c.Int(nullable: false, identity: true),
                    CommissionId = c.Int(nullable: false),
                    Date = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.ControlDateId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .Index(t => t.CommissionId);
        }
    }
}
