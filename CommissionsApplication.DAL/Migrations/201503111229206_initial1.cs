namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attachments", "Commission_CommissionId", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId1", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId2", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId3", "dbo.Commissions");
            DropForeignKey("dbo.Attachments", "Commission_CommissionId4", "dbo.Commissions");
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId1" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId2" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId3" });
            DropIndex("dbo.Attachments", new[] { "Commission_CommissionId4" });
            DropColumn("dbo.Attachments", "CommissionId");
            RenameColumn(table: "dbo.Attachments", name: "Commission_CommissionId4", newName: "CommissionId");
            AddColumn("dbo.Attachments", "AttachmentType", c => c.Int(nullable: false));
            AlterColumn("dbo.Attachments", "CommissionId", c => c.Int(nullable: false));
            CreateIndex("dbo.Attachments", "CommissionId");
            AddForeignKey("dbo.Attachments", "CommissionId", "dbo.Commissions", "CommissionId", cascadeDelete: true);
            DropColumn("dbo.Attachments", "Commission_CommissionId");
            DropColumn("dbo.Attachments", "Commission_CommissionId1");
            DropColumn("dbo.Attachments", "Commission_CommissionId2");
            DropColumn("dbo.Attachments", "Commission_CommissionId3");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attachments", "Commission_CommissionId3", c => c.Int());
            AddColumn("dbo.Attachments", "Commission_CommissionId2", c => c.Int());
            AddColumn("dbo.Attachments", "Commission_CommissionId1", c => c.Int());
            AddColumn("dbo.Attachments", "Commission_CommissionId", c => c.Int());
            DropForeignKey("dbo.Attachments", "CommissionId", "dbo.Commissions");
            DropIndex("dbo.Attachments", new[] { "CommissionId" });
            AlterColumn("dbo.Attachments", "CommissionId", c => c.Int());
            DropColumn("dbo.Attachments", "AttachmentType");
            RenameColumn(table: "dbo.Attachments", name: "CommissionId", newName: "Commission_CommissionId4");
            AddColumn("dbo.Attachments", "CommissionId", c => c.Int(nullable: false));
            CreateIndex("dbo.Attachments", "Commission_CommissionId4");
            CreateIndex("dbo.Attachments", "Commission_CommissionId3");
            CreateIndex("dbo.Attachments", "Commission_CommissionId2");
            CreateIndex("dbo.Attachments", "Commission_CommissionId1");
            CreateIndex("dbo.Attachments", "Commission_CommissionId");
            AddForeignKey("dbo.Attachments", "Commission_CommissionId4", "dbo.Commissions", "CommissionId");
            AddForeignKey("dbo.Attachments", "Commission_CommissionId3", "dbo.Commissions", "CommissionId");
            AddForeignKey("dbo.Attachments", "Commission_CommissionId2", "dbo.Commissions", "CommissionId");
            AddForeignKey("dbo.Attachments", "Commission_CommissionId1", "dbo.Commissions", "CommissionId");
            AddForeignKey("dbo.Attachments", "Commission_CommissionId", "dbo.Commissions", "CommissionId");
        }
    }
}
