namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_responsible_charges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResponsibleCommissions", "Responsible_ResponsibleId", "dbo.Responsibles");
            DropForeignKey("dbo.ResponsibleCommissions", "Commission_CommissionId", "dbo.Commissions");
            DropIndex("dbo.ResponsibleCommissions", new[] { "Responsible_ResponsibleId" });
            DropIndex("dbo.ResponsibleCommissions", new[] { "Commission_CommissionId" });
            CreateTable(
                "dbo.Charges",
                c => new
                    {
                        ChargeId = c.Int(nullable: false, identity: true),
                        ResponsibleId = c.Int(nullable: false),
                        CommissionId = c.Int(nullable: false),
                        ExecutionDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ChargeId)
                .ForeignKey("dbo.Commissions", t => t.CommissionId, cascadeDelete: true)
                .ForeignKey("dbo.Responsibles", t => t.ResponsibleId, cascadeDelete: true)
                .Index(t => t.ResponsibleId)
                .Index(t => t.CommissionId);
            
            DropTable("dbo.ResponsibleCommissions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ResponsibleCommissions",
                c => new
                    {
                        Responsible_ResponsibleId = c.Int(nullable: false),
                        Commission_CommissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Responsible_ResponsibleId, t.Commission_CommissionId });
            
            DropForeignKey("dbo.Charges", "ResponsibleId", "dbo.Responsibles");
            DropForeignKey("dbo.Charges", "CommissionId", "dbo.Commissions");
            DropIndex("dbo.Charges", new[] { "CommissionId" });
            DropIndex("dbo.Charges", new[] { "ResponsibleId" });
            DropTable("dbo.Charges");
            CreateIndex("dbo.ResponsibleCommissions", "Commission_CommissionId");
            CreateIndex("dbo.ResponsibleCommissions", "Responsible_ResponsibleId");
            AddForeignKey("dbo.ResponsibleCommissions", "Commission_CommissionId", "dbo.Commissions", "CommissionId", cascadeDelete: true);
            AddForeignKey("dbo.ResponsibleCommissions", "Responsible_ResponsibleId", "dbo.Responsibles", "ResponsibleId", cascadeDelete: true);
        }
    }
}
