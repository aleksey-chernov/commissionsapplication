namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_logs_datetime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "ActionDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs", "ActionDate");
        }
    }
}
