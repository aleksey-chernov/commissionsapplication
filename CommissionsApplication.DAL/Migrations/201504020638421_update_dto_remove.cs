namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_dto_remove : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ResponsibleDTOCommissionDTOes", newName: "ResponsibleCommissions");
            RenameTable(name: "dbo.UserDTOResponsibleDTOes", newName: "UserResponsibles");
            RenameColumn(table: "dbo.ResponsibleCommissions", name: "ResponsibleDTO_ResponsibleId", newName: "Responsible_ResponsibleId");
            RenameColumn(table: "dbo.ResponsibleCommissions", name: "CommissionDTO_CommissionId", newName: "Commission_CommissionId");
            RenameColumn(table: "dbo.UserResponsibles", name: "UserDTO_UserId", newName: "User_UserId");
            RenameColumn(table: "dbo.UserResponsibles", name: "ResponsibleDTO_ResponsibleId", newName: "Responsible_ResponsibleId");
            RenameIndex(table: "dbo.ResponsibleCommissions", name: "IX_ResponsibleDTO_ResponsibleId", newName: "IX_Responsible_ResponsibleId");
            RenameIndex(table: "dbo.ResponsibleCommissions", name: "IX_CommissionDTO_CommissionId", newName: "IX_Commission_CommissionId");
            RenameIndex(table: "dbo.UserResponsibles", name: "IX_UserDTO_UserId", newName: "IX_User_UserId");
            RenameIndex(table: "dbo.UserResponsibles", name: "IX_ResponsibleDTO_ResponsibleId", newName: "IX_Responsible_ResponsibleId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserResponsibles", name: "IX_Responsible_ResponsibleId", newName: "IX_ResponsibleDTO_ResponsibleId");
            RenameIndex(table: "dbo.UserResponsibles", name: "IX_User_UserId", newName: "IX_UserDTO_UserId");
            RenameIndex(table: "dbo.ResponsibleCommissions", name: "IX_Commission_CommissionId", newName: "IX_CommissionDTO_CommissionId");
            RenameIndex(table: "dbo.ResponsibleCommissions", name: "IX_Responsible_ResponsibleId", newName: "IX_ResponsibleDTO_ResponsibleId");
            RenameColumn(table: "dbo.UserResponsibles", name: "Responsible_ResponsibleId", newName: "ResponsibleDTO_ResponsibleId");
            RenameColumn(table: "dbo.UserResponsibles", name: "User_UserId", newName: "UserDTO_UserId");
            RenameColumn(table: "dbo.ResponsibleCommissions", name: "Commission_CommissionId", newName: "CommissionDTO_CommissionId");
            RenameColumn(table: "dbo.ResponsibleCommissions", name: "Responsible_ResponsibleId", newName: "ResponsibleDTO_ResponsibleId");
            RenameTable(name: "dbo.UserResponsibles", newName: "UserDTOResponsibleDTOes");
            RenameTable(name: "dbo.ResponsibleCommissions", newName: "ResponsibleDTOCommissionDTOes");
        }
    }
}
