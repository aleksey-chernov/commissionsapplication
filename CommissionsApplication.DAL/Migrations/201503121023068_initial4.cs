namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial4 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Attachments", newName: "Attachment");
            RenameTable(name: "dbo.AttachmentDatas", newName: "AttachmentData");
            RenameTable(name: "dbo.Commissions", newName: "Commission");
            RenameTable(name: "dbo.ControlDates", newName: "ControlDate");
            RenameTable(name: "dbo.Ids", newName: "Responsible");
            RenameTable(name: "dbo.Users", newName: "User");
            RenameTable(name: "dbo.ResponsibleCommissions", newName: "ResponsibleCommission");
            RenameTable(name: "dbo.UserResponsibles", newName: "UserResponsible");
            DropColumn("dbo.User", "UserRoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "UserRoleId", c => c.Int(nullable: false));
            RenameTable(name: "dbo.UserResponsible", newName: "UserResponsibles");
            RenameTable(name: "dbo.ResponsibleCommission", newName: "ResponsibleCommissions");
            RenameTable(name: "dbo.User", newName: "Users");
            RenameTable(name: "dbo.Responsible", newName: "Responsibles");
            RenameTable(name: "dbo.ControlDate", newName: "ControlDates");
            RenameTable(name: "dbo.Commission", newName: "Commissions");
            RenameTable(name: "dbo.AttachmentData", newName: "AttachmentDatas");
            RenameTable(name: "dbo.Attachment", newName: "Attachments");
        }
    }
}
