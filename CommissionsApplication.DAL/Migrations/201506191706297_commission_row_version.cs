namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_row_version : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commissions", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commissions", "RowVersion");
        }
    }
}
