namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_row_number_view : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommissionViews",
                c => new
                    {
                        CommissionId = c.Int(nullable: false, identity: true),
                        CommissionRowNumber = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CommissionId);

            DropTable("dbo.CommissionViews");

            Sql("CREATE VIEW dbo.CommissionViews " +
                "AS SELECT CommissionId, ROW_NUMBER() OVER(ORDER BY CommissionId) AS CommissionRowNumber " +
                "FROM dbo.Commissions");
        }
        
        public override void Down()
        {
            Sql("DROP VIEW dbo.CommissionViews");
        }
    }
}