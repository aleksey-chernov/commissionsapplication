namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_unique_required_constraints : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Responsibles", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Responsibles", "Email", c => c.String(nullable: false, maxLength: 99));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 99));
            AlterColumn("dbo.Users", "Email", c => c.String(nullable: false, maxLength: 99));
            CreateIndex("dbo.Responsibles", "Email", unique: true, name: "IX_ResponsibleEmail");
            CreateIndex("dbo.Users", new[] { "Login", "Email" }, unique: true, name: "IX_UserLoginEmail");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", "IX_UserLoginEmail");
            DropIndex("dbo.Responsibles", "IX_ResponsibleEmail");
            AlterColumn("dbo.Users", "Email", c => c.String());
            AlterColumn("dbo.Users", "Login", c => c.String());
            AlterColumn("dbo.Users", "Name", c => c.String());
            AlterColumn("dbo.Responsibles", "Email", c => c.String());
            AlterColumn("dbo.Responsibles", "Name", c => c.String());
        }
    }
}
