namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_execution_mark_add : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commissions", "ExecutionMark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commissions", "ExecutionMark");
        }
    }
}
