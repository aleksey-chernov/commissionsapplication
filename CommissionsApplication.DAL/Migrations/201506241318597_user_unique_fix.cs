namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_unique_fix : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", "IX_UserLoginEmail");
            CreateIndex("dbo.Users", "Login", unique: true, name: "IX_UserLogin");
            CreateIndex("dbo.Users", "Email", unique: true, name: "IX_UserEmail");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", "IX_UserEmail");
            DropIndex("dbo.Users", "IX_UserLogin");
            CreateIndex("dbo.Users", new[] { "Login", "Email" }, unique: true, name: "IX_UserLoginEmail");
        }
    }
}
