namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_commission_lastmodified_token : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commissions", "LastModified", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commissions", "LastModified");
        }
    }
}
