namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_delay_factor_remove_required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Commissions", "DelayFactor", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Commissions", "DelayFactor", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
