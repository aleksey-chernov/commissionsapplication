namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_add_delay_info : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commissions", "DelayInfo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Commissions", "DelayInfo");
        }
    }
}
