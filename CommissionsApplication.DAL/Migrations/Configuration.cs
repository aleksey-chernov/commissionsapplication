using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Transactions;
using CommissionsApplication.DAL.EF;
using CommissionsApplication.DAL.Helpers.Infrastructure;
using CommissionsApplication.Domain.Entities;
using CommissionsApplication.Domain.Infrastructure;

namespace CommissionsApplication.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CommissionsContext>
    {
        private readonly bool _pendingMigrations;
        
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            //http://stackoverflow.com/questions/13510341/entity-framework-5-migrations-setting-up-an-initial-migration-and-single-seed-o
            _pendingMigrations = new DbMigrator(this).GetPendingMigrations().Any();
        }

        protected override void Seed(CommissionsContext context)
        {
#if !DEBUG
            if (!_pendingMigrations) return;
#endif

            var random = new Random((int) DateTime.Now.Ticks);
            var generator = new RandomEntityGenerator(random);

            //var users = Enumerable.Range(1, 5).Select(x => generator.RandomUser()).ToList();
            var users = new List<User>
            {
                new User
                {
                    Name = "�������������",
                    Login = "admin",
                    Password = PasswordHash.GetMD5Hash("123456"),
                    Email = "admin@spbaep.ru",
                    UserRole = User.Role.Administrator
                },
                new User
                {
                    Name = "��������",
                    Login = "assist",
                    Password = PasswordHash.GetMD5Hash("123456"),
                    Email = "assist@spbaep.ru",
                    UserRole = User.Role.Assistant
                }
            };

            var responsibles = Enumerable.Range(1, 3).Select(x =>
            {
                var responsible = generator.RandomResponsible();
                int index = random.Next(users.Count);
                int count = random.Next(0, users.Count - index);
                users.GetRange(index, count).ToList()
                    .ForEach(user => responsible.Users.Add(user));

                return responsible;
            }).ToList();

            var commissions = Enumerable.Range(1, 1500).Select(x =>
            {
                var commission = generator.RandomCommission();
                int index = random.Next(responsibles.Count);
                int count = random.Next(0, responsibles.Count - index);
                responsibles.GetRange(index, count).ToList()
                    .ForEach(responsible => commission.Charges.Add(new Charge {
                          Responsible  = responsible,
                          Commission = commission,
                          ExecutionDate = random.Next() % 2 == 0 && commission.ExecutionDate.HasValue
                          ? generator.RandomDate(commission.ExecutionDate.Value.AddMonths(-1), commission.ExecutionDate.Value)
                          : (DateTime?) null
                        }));

                Enumerable.Range(1, 3).Select(c => new ControlDate
                {
                    Date = generator.RandomDate(DateTime.Now.AddMonths(-12), DateTime.Now)
                }).ToList().ForEach(controlDate => commission.ControlDates.Add(controlDate));
                
                return commission;
            }).ToList();

            foreach (var user in context.Users)
                context.Users.Remove(user);
            foreach (var responsible in context.Responsibles)
                context.Responsibles.Remove(responsible);
            foreach (var commission in context.Commissions)
                context.Commissions.Remove(commission);

            context.SaveChanges();

            context.Users.AddRange(users);
            context.Responsibles.AddRange(responsibles);
            context.Commissions.AddRange(commissions);

            context.SaveChanges();
        }
    }
}