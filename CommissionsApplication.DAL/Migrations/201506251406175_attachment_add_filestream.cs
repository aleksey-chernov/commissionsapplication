namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attachment_add_filestream : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments");
            DropIndex("dbo.AttachmentDatas", new[] { "AttachmentId" });
            AddColumn("dbo.Attachments", "ContentType", c => c.String());
            DropTable("dbo.AttachmentDatas");

            Sql("ALTER TABLE [dbo].[Attachments] ADD [Guid] uniqueidentifier rowguidcol NOT NULL");
            Sql("ALTER TABLE [dbo].[Attachments] ADD CONSTRAINT [UQ_Attachments_Guid] UNIQUE NONCLUSTERED ([Guid])");
            Sql("ALTER TABLE [dbo].[Attachments] ADD CONSTRAINT [DF_Attachments_Guid] DEFAULT (newid()) FOR [Guid]");

            Sql("ALTER TABLE [dbo].[Attachments] ADD [Data] varbinary(max) FILESTREAM NOT NULL");
            Sql("ALTER TABLE [dbo].[Attachments] ADD CONSTRAINT [DF_Attachments_Data] DEFAULT(0x) FOR [Data]");
            
            //EXEC sp_configure filestream_access_level, 2
            //RECONFIGURE

            //ALTER DATABASE [Commissions]
            //SET ALLOW_SNAPSHOT_ISOLATION OFF

            //ALTER DATABASE [Commissions]
            //SET READ_COMMITTED_SNAPSHOT OFF

            //alter database [Commissions]
            //add filegroup [FileStream] contains filestream;
            //go

            //alter database [Commissions]
            //add file
            //  ( NAME = 'Commissions_files', FILENAME = 'C:\Commissions' )
            //to filegroup [Filestream];
            //go
        }
        
        public override void Down()
        {
            Sql("ALTER TABLE [dbo].[Attachments] DROP CONSTRAINT [DF_Attachments_Data]");
            Sql("ALTER TABLE [dbo].[Attachments] DROP COLUMN [Data]");

            Sql("ALTER TABLE [dbo].[Attachments] DROP CONSTRAINT [UQ_Attachments_Guid]");
            Sql("ALTER TABLE [dbo].[Attachments] DROP CONSTRAINT [DF_Attachments_Guid]");
            Sql("ALTER TABLE [dbo].[Attachments] DROP COLUMN [GUID]");

            CreateTable(
                "dbo.AttachmentDatas",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false),
                        Data = c.Binary(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.AttachmentId);
            
            DropColumn("dbo.Attachments", "ContentType");
            CreateIndex("dbo.AttachmentDatas", "AttachmentId");
            AddForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments", "AttachmentId", cascadeDelete: true);
        }
    }
}
