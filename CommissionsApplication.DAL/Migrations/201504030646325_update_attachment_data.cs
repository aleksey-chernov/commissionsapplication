namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_attachment_data : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AttachmentDatas",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false),
                        Data = c.Binary(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId, cascadeDelete: true)
                .Index(t => t.AttachmentId);
            
            DropColumn("dbo.Attachments", "ContentType");
            DropColumn("dbo.Attachments", "CreatedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attachments", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Attachments", "ContentType", c => c.String());
            DropForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments");
            DropIndex("dbo.AttachmentDatas", new[] { "AttachmentId" });
            DropTable("dbo.AttachmentDatas");
        }
    }
}
