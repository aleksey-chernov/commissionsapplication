namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_attachment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttachmentData", "AttachmentId", "dbo.Attachment");
            DropIndex("dbo.AttachmentData", new[] { "AttachmentId" });
            AddColumn("dbo.Attachment", "ContentType", c => c.String());
            DropTable("dbo.AttachmentData");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AttachmentData",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false),
                        Data = c.Binary(),
                    })
                .PrimaryKey(t => t.AttachmentId);
            
            DropColumn("dbo.Attachment", "ContentType");
            CreateIndex("dbo.AttachmentData", "AttachmentId");
            AddForeignKey("dbo.AttachmentData", "AttachmentId", "dbo.Attachment", "AttachmentId", cascadeDelete: true);
        }
    }
}
