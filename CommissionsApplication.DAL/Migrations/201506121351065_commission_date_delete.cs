namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commission_date_delete : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Commissions", "CommissionDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Commissions", "CommissionDate", c => c.DateTime(nullable: false));
        }
    }
}