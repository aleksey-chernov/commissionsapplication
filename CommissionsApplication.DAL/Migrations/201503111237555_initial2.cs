namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropPrimaryKey("dbo.UserRoles");
            AddColumn("dbo.Users", "UserRoleId", c => c.Int(nullable: false));
            AddColumn("dbo.UserRoles", "UserRoleId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.UserRoles", "UserRoleId");
            CreateIndex("dbo.Users", "UserRoleId");
            AddForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments", "AttachmentId", cascadeDelete: true);
            AddForeignKey("dbo.Users", "UserRoleId", "dbo.UserRoles", "UserRoleId", cascadeDelete: true);
            DropColumn("dbo.UserRoles", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserRoles", "UserId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Users", "UserRoleId", "dbo.UserRoles");
            DropForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments");
            DropIndex("dbo.Users", new[] { "UserRoleId" });
            DropPrimaryKey("dbo.UserRoles");
            DropColumn("dbo.UserRoles", "UserRoleId");
            DropColumn("dbo.Users", "UserRoleId");
            AddPrimaryKey("dbo.UserRoles", "UserId");
            CreateIndex("dbo.UserRoles", "UserId");
            AddForeignKey("dbo.UserRoles", "UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.AttachmentDatas", "AttachmentId", "dbo.Attachments", "AttachmentId");
        }
    }
}
