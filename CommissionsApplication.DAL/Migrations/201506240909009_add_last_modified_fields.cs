namespace CommissionsApplication.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_last_modified_fields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commissions", "LastModified", c => c.DateTime());
            AddColumn("dbo.Responsibles", "LastModified", c => c.DateTime());
            AddColumn("dbo.Users", "LastModified", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "LastModified");
            DropColumn("dbo.Responsibles", "LastModified");
            DropColumn("dbo.Commissions", "LastModified");
        }
    }
}
