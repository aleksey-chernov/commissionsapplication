﻿using System;
using System.ServiceProcess;
using System.Text;
using CommandLine;
using log4net;

namespace CommissionsApplication.MailService
{
    class Program
    {
        internal static readonly ILog Log = LogManager.GetLogger("MailServiceLogger");

        static void Main(string[] args)
        {
            var options = new Options();

            var parser = new Parser(conf =>
            {
                conf.MutuallyExclusive = true;
                conf.CaseSensitive = false;
                conf.IgnoreUnknownArguments = true;
                conf.HelpWriter = Console.Error;
            });

            if (parser.ParseArguments(args, options))
            {
                if (options.Install)
                {
                    Console.WriteLine(ServiceInstaller.Install()
                        ? @"Service installed."
                        : @"Failed to install service.");
                    return;
                }

                if (options.Uninstall)
                {
                    Console.WriteLine(ServiceInstaller.Uninstall()
                        ? @"Service uninstalled."
                        : @"Failed to uninstall service.");
                    return;
                }

                var service = new MailService();
                var servicesToRun = new ServiceBase[] { service };

                if (Environment.UserInteractive)
                {
                    Console.OutputEncoding = Encoding.UTF8;

                    Console.CancelKeyPress += (x, y) => service.Stop();
                    service.Start();
                    Console.WriteLine(@"Service is running, press a key to stop.");
                    Console.ReadKey();
                    service.Stop();
                    Console.WriteLine(@"Service stopped.");
                }
                else
                {
                    ServiceBase.Run(servicesToRun);
                }
            }
            else
            {
                Environment.ExitCode = 1;
            }
        }
    }
}