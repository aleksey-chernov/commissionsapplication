﻿using CommandLine;
using CommandLine.Text;

namespace CommissionsApplication.MailService
{
    class Options
    {
        [Option('i', "install", MutuallyExclusiveSet = "install", HelpText = "Install application as service.")]
        public bool Install { get; set; }

        [Option('u', "uninstall", MutuallyExclusiveSet = "install", HelpText = "Uninstall application service.")]
        public bool Uninstall { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            var help = new HelpText
            {
                AdditionalNewLineAfterOption = true,
                AddDashesToOption = true
            };
            help.AddOptions(this);

            return help;
        }
    }
}