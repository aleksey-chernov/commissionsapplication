﻿using System.Configuration;
using System.ServiceProcess;
using Quartz;
using Quartz.Impl;

namespace CommissionsApplication.MailService
{
    partial class MailService : ServiceBase
    {
        private readonly IScheduler _scheduler;

        public MailService()
        {
            InitializeComponent();

            _scheduler = new StdSchedulerFactory().GetScheduler();

            var identity = 0;
            foreach (var cron in ConfigurationManager.AppSettings["CronSchedule"].Split(';'))
            {
                var job = JobBuilder.Create<DelayReportJob>()
                    .WithIdentity("delayReport" + identity)
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("delayTrigger" + identity)
                    .WithCronSchedule(cron)
                    .ForJob(job)
                    .Build();

                _scheduler.ScheduleJob(job, trigger);

                identity++;
            }
        }

        public void Start()
        {
            _scheduler.Start();
        }

        public new void Stop()
        {
            _scheduler.Shutdown();
        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        protected override void OnStop()
        {
            Stop();
        }
    }
}