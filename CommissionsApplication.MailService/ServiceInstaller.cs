﻿using System.Configuration.Install;
using System.Reflection;

namespace CommissionsApplication.MailService
{
    class ServiceInstaller
    {
        private static readonly string ExePath = Assembly.GetExecutingAssembly().Location;

        public static bool Install()
        {
            try { ManagedInstallerClass.InstallHelper(new[] { ExePath }); }
            catch { return false; }
            return true;
        }

        public static bool Uninstall()
        {
            try { ManagedInstallerClass.InstallHelper(new[] { "/u", ExePath }); }
            catch { return false; }
            return true;
        }
    }
}