﻿using System;
using CommissionsApplication.DAL.Repositories.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Export.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Mail.Concrete;
using CommissionsApplication.WebUI.Infrastructure.Mapping.Concrete;
using Quartz;

namespace CommissionsApplication.MailService
{
    class DelayReportJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var unitOfWork = new EFUnitOfWork(null);
                var mailManager = new DefaultMailManager(unitOfWork, new CustomMapper(unitOfWork), new CommissionsExportService());
                mailManager.DeliverDelayReport();
            }
            catch (Exception ex)
            {
                Program.Log.Error("DelayReportJob error", ex);
            }
        }
    }
}